﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ProjectDazzle;
using System.Drawing;

namespace DazzleTest
{
    [TestClass]
    public class CSSTests
    {
        [TestInitialize]
        public void Init_Static_Fields()
        {
            ChromosomeBase.InitStatic(new CSSBorder());
        }

        [TestMethod]
        public void Test_Fitness_After_Crossover()
        {
            // Arrange

            ChromosomeBase yellow = new CSSButton();
            ChromosomeBase purple = new CSSButton();

            double initialFitness = yellow - purple;

            List<ChromosomeBase> cpList = new List<ChromosomeBase>() { yellow, purple };

            var mockEngine = new Mock<GeneticEngine>(0);
            mockEngine.SetupGet(m => m.Population).Returns(cpList);

            // Act

            mockEngine.Object.CalculatePopulationFitness(yellow);

            purple.Crossover(yellow);

            mockEngine.Object.CalculatePopulationFitness(yellow);

            // Assert

            double newFitness = purple.Fitness;
            Assert.IsTrue(newFitness <= initialFitness);
        }
        
        [TestMethod]
        public void Test_PopulationFitness_After_Crossover()
        {
            // Arrange

            GeneticEngine ge = new GeneticEngine(50, EDesignElement.Button, false);

            ge.CalculatePopulationFitness(new CSSButton());

            double initFitness = ge.PopulationFitness;

            // Act

            ge.MakeNextGeneration();

            // Assert

            double newFitness = ge.PopulationFitness;

            Assert.AreNotEqual(initFitness, newFitness);
            Assert.IsTrue(newFitness < initFitness);
        }

        
        [TestMethod]
        public void Test_GenerationIncrement()
        {
            // Arrange

            GeneticEngine ge = new GeneticEngine(10, EDesignElement.Button, false);

            ge.CalculatePopulationFitness(new CSSButton());

            int currentGeneration = ge.Generation;

            // Act

            ge.MakeNextGeneration();

            // Assert

            int nextGeneration = ge.Generation;

            Assert.AreNotEqual(currentGeneration, nextGeneration);
            Assert.IsTrue(currentGeneration < nextGeneration);
        }

        
        [TestMethod]
        public void Test_Mutation_WholeObject()
        {
            // Arrange

            var ch1 = new CSSButton();
            var ch2 = new CSSButton(ch1);

            // Act

            ch1.SetMutationChance(1);
            ch1.Mutate();

            //Assert

            Assert.AreNotEqual(ch1.ToString(), ch2.ToString());
        }

        
        [TestMethod]
        public void Test_Mutation_BgColor()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var redBin = ch1.BGColor;

            // Act

            ch1.Mutate();

            // Assert

            var redBinMut = ch1.BGColor;
            Assert.AreNotEqual(redBin, redBinMut);
        }

        
        [TestMethod]
        public void Test_Mutation_BorderColor()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var greenBin = ch1.BorderColors[0];

            // Act

            ch1.Mutate();

            // Assert

            var greenBinMut = ch1.BorderColors[0];
            Assert.AreNotEqual(greenBin, greenBinMut);
        }

        
        [TestMethod]
        public void Test_Mutation_HoverColor()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.HoverColor;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.HoverColor;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_ShadowColor()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.ShadowColor;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.ShadowColor;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_Shadow_HPos()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.ShadowHPos;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.ShadowHPos;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_Shadow_VPos()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.ShadowVPos;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.ShadowVPos;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_ShadowBlur()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.ShadowBlur;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.ShadowBlur;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_BorderRadius()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.BorderRadii[0];

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.BorderRadii[0];
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_FontFamilyID()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.FontFamilyID;

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.FontFamilyID;
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_Border()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var blueBin = ch1.BorderStyles[0];

            // Act

            ch1.Mutate();

            // Assert

            var blueBinMut = ch1.BorderStyles[0];
            Assert.AreNotEqual(blueBin, blueBinMut);
        }

        [TestMethod]
        public void Test_Mutation_Padding()
        {
            // Arrange

            var ch1 = new CSSButton();
            ch1.SetMutationChance(1);

            var p1 = ch1.Paddings[0];
            var p2 = ch1.Paddings[1];
            var p3 = ch1.Paddings[2];
            var p4 = ch1.Paddings[3];

            // Act

            ch1.Mutate();

            // Assert

            var p1Mut = ch1.Paddings[0];
            var p2Mut = ch1.Paddings[1];
            var p3Mut = ch1.Paddings[2];
            var p4Mut = ch1.Paddings[3];

            Assert.AreNotEqual(p1, p1Mut);
            Assert.AreNotEqual(p2, p2Mut);
            Assert.AreNotEqual(p3, p3Mut);
            Assert.AreNotEqual(p4, p4Mut);
        }

        
        [TestMethod]
        public void Test_MutationChanceSetter_Max()
        {
            // Arrange

            var ch = new CSSButton();

            // Act

            ch.SetMutationChance(double.MaxValue);

            // Assert

            Assert.AreEqual(ch.MutationChance, 1);
        }

        
        [TestMethod]
        public void Test_MutationChanceSetter_Min()
        {
            // Arrange

            var ch = new CSSButton();

            // Act

            ch.SetMutationChance(double.MinValue);

            // Assert

            Assert.AreEqual(ch.MutationChance, 0);
        }

        
        [TestMethod]
        public void Test_BinaryStringHelper()
        {
            // Arrange

            var gl = new GeneList();
            var tString = "111";
            int targetLength = 6;

            // Act

            Gene g = gl.FixBinaryStringLength(targetLength, tString, 6);

            // Assert

            Assert.AreEqual(targetLength, g.Binary.Length);
        }

        [TestMethod]
        public void Test_DeltaOperator()
        {
            // Arrange

            var mockList1 = new Mock<GeneList>();
            var mockList2 = new Mock<GeneList>();

            mockList1.Setup(c => c.GetValue(0)).Returns(10);
            mockList1.Setup(c => c.GetValue(1)).Returns(0);
            mockList1.Setup(c => c.GetValue(2)).Returns(0);
            mockList1.Setup(c => c.GetValue(3)).Returns(10);
            mockList1.Setup(c => c.GetValue(4)).Returns(0);
            mockList1.Setup(c => c.GetValue(5)).Returns(0);
            mockList1.Setup(c => c.GetValue(6)).Returns(0);
            mockList1.Setup(c => c.GetValue(7)).Returns(10);
            mockList1.Setup(c => c.GetValue(8)).Returns(0);
            mockList1.Setup(c => c.GetValue(9)).Returns(0);
            mockList1.Setup(c => c.GetValue(10)).Returns(0);
            mockList1.Setup(c => c.GetValue(11)).Returns(0);
            mockList1.Setup(c => c.GetValue(12)).Returns(0);
            mockList1.Setup(c => c.GetValue(13)).Returns(0);
            mockList1.Setup(c => c.GetValue(14)).Returns(0);
            mockList1.Setup(c => c.GetValue(15)).Returns(0);
            mockList1.Setup(c => c.GetValue(16)).Returns(0);
            mockList1.Setup(c => c.GetValue(17)).Returns(0);
            mockList1.Setup(c => c.GetValue(18)).Returns(0);
            mockList1.Setup(c => c.GetValue(19)).Returns(0);
            mockList1.Setup(c => c.GetValue(20)).Returns(0);
            mockList1.Setup(c => c.GetValue(21)).Returns(0);
            mockList1.Setup(c => c.GetValue(22)).Returns(0);

            mockList1.SetupGet(c => c.Count).Returns(23);

            mockList2.Setup(c => c.GetValue(0)).Returns(0);
            mockList2.Setup(c => c.GetValue(1)).Returns(0);
            mockList2.Setup(c => c.GetValue(2)).Returns(0);
            mockList2.Setup(c => c.GetValue(3)).Returns(0);
            mockList2.Setup(c => c.GetValue(4)).Returns(0);
            mockList2.Setup(c => c.GetValue(5)).Returns(0);
            mockList2.Setup(c => c.GetValue(6)).Returns(0);
            mockList2.Setup(c => c.GetValue(7)).Returns(0);
            mockList2.Setup(c => c.GetValue(8)).Returns(0);
            mockList2.Setup(c => c.GetValue(9)).Returns(0);
            mockList2.Setup(c => c.GetValue(10)).Returns(10);
            mockList2.Setup(c => c.GetValue(11)).Returns(10);
            mockList2.Setup(c => c.GetValue(12)).Returns(10);
            mockList2.Setup(c => c.GetValue(13)).Returns(10);
            mockList2.Setup(c => c.GetValue(14)).Returns(10);
            mockList2.Setup(c => c.GetValue(15)).Returns(10);
            mockList2.Setup(c => c.GetValue(16)).Returns(10);
            mockList2.Setup(c => c.GetValue(17)).Returns(10);
            mockList2.Setup(c => c.GetValue(18)).Returns(10);
            mockList2.Setup(c => c.GetValue(19)).Returns(10);
            mockList2.Setup(c => c.GetValue(20)).Returns(10);
            mockList2.Setup(c => c.GetValue(21)).Returns(10);
            mockList2.Setup(c => c.GetValue(22)).Returns(10);

            mockList2.SetupGet(c => c.Count).Returns(23);

            var ch1 = new CSSButton(mockList1.Object, new GeneList());
            var ch2 = new CSSButton(mockList2.Object, new GeneList());

            // Act

            double delta = ch1 - ch2;

            // Assert

            Assert.AreEqual(160, delta);
        }

        [TestMethod]
        public void Test_RouletteSelection_FittestProb()
        {
            // Arrange

            ChromosomeBase ch1 = new CSSButton();
            ChromosomeBase ch2 = new CSSButton();
            ChromosomeBase ch3 = new CSSButton();
            ChromosomeBase ch4 = new CSSButton();
            ChromosomeBase ch5 = new CSSButton();
            ChromosomeBase ch6 = new CSSButton();

            List<ChromosomeBase> cpList = new List<ChromosomeBase>() { ch1, ch2, ch3, ch4, ch5, ch6 };

            var mockEngine = new Mock<GeneticEngine>(0);
            mockEngine.SetupGet(m => m.Population).Returns(cpList);

            var fittest = ch1;

            List<double> probList = new List<double>() { 5, 3, 3, 2, 1, 0 };

            double prob = 0.6;

            // Act

            mockEngine.Object.CalculatePopulationFitness(fittest);

            var selectedCh = mockEngine.Object.RouletteHelper(prob, 10, probList);

            // Assert

            Assert.AreEqual(ch1, selectedCh);
        }

        
        [TestMethod]
        public void Test_RouletteSelection_MediumProb()
        {
            // Arrange

            ChromosomeBase ch1 = new CSSButton();
            ChromosomeBase ch2 = new CSSButton();
            ChromosomeBase ch3 = new CSSButton();
            ChromosomeBase ch4 = new CSSButton();
            ChromosomeBase ch5 = new CSSButton();
            ChromosomeBase ch6 = new CSSButton();

            List<ChromosomeBase> cpList = new List<ChromosomeBase>() { ch1, ch2, ch3, ch4, ch5, ch6 };

            var mockEngine = new Mock<GeneticEngine>(0);
            mockEngine.SetupGet(m => m.Population).Returns(cpList);

            var fittest = ch1;

            List<double> probList = new List<double>() { 5, 3, 3, 2, 1, 0 };

            double prob = 0.4;

            // Act

            mockEngine.Object.CalculatePopulationFitness(fittest);

            var selectedCh = mockEngine.Object.RouletteHelper(prob, 10, probList);

            // Assert

            Assert.AreEqual(ch2, selectedCh);
        }

        
        [TestMethod]
        public void Test_RouletteSelection_LowestProb()
        {
            // Arrange

            ChromosomeBase ch1 = new CSSButton();
            ChromosomeBase ch2 = new CSSButton();
            ChromosomeBase ch3 = new CSSButton();
            ChromosomeBase ch4 = new CSSButton();
            ChromosomeBase ch5 = new CSSButton();
            ChromosomeBase ch6 = new CSSButton();

            List<ChromosomeBase> cpList = new List<ChromosomeBase>() { ch1, ch2, ch3, ch4, ch5, ch6 };

            var mockEngine = new Mock<GeneticEngine>(0);
            mockEngine.SetupGet(m => m.Population).Returns(cpList);

            var fittest = ch1;

            List<double> probList = new List<double>() { 7, 6, 3, 2, 1, 0 };

            double prob = 0.01;

            // Act

            mockEngine.Object.CalculatePopulationFitness(fittest);

            var selectedCh = mockEngine.Object.RouletteHelper(prob, 10, probList);

            // Assert

            Assert.AreEqual(ch6, selectedCh);
        }

        
        [TestMethod]
        public void Test_Array_Swap()
        {
            // Arrange

            ChromosomeBase ch = new CSSButton();
            int[] arr = new int[] { 5, 6, 7 };

            // Act

            ch.Swap(0, 2, ref arr);

            // Assert

            Assert.AreEqual(7, arr[0]);
            Assert.AreEqual(6, arr[1]);
            Assert.AreEqual(5, arr[2]);
        }

        [TestMethod]
        public void Test_MultiPointCrossover()
        {
            // Arrange

            CSSTable ch1 = new CSSTable();
            ChromosomeBase ch2 = new CSSTable();
            ChromosomeBase b = new CSSTable(ch1);

            // Act

            ch1.MultiPointCrossover(ch2);

            // Assert

            Assert.AreNotEqual(b.ToString(), ch1.ToString());
        }

        [TestMethod]
        public void Test_ElementViewer_AddSelected_AddNew()
        {
            // Arrange

            GeneticEngine ge = new GeneticEngine(5, EDesignElement.Button, false);
            ElementViewer ev = new ElementViewer(ge);

            // Act

            ev.AddSelected(0);

            // Assert

            Assert.AreEqual(ev.SelectedDesigns[0], ge.Population[0]);
        }

        [TestMethod]
        public void Test_ElementViewer_AddSelected_AddExisting()
        {
            // Arrange

            GeneticEngine ge = new GeneticEngine(5, EDesignElement.Button, false);
            ElementViewer ev = new ElementViewer(ge);

            // Act

            ev.AddSelected(0); // Add new
            ev.AddSelected(0); // Should remove the already added element

            // Assert

            Assert.AreEqual(0, ev.SelectedDesigns.Count);
        }

        [TestMethod]
        public void Test_ColorShader_Shade()
        {
            // Arrange

            Color cr = Color.Red;
            Color cg = Color.Green;
            Color cb = Color.Blue;

            // Act

            Color cr2 = ColorShader.Shade(cr);
            Color cg2 = ColorShader.Shade(cg);
            Color cb2 = ColorShader.Shade(cb);

            // Assert

            Assert.IsTrue(cr2.R < cr.R);
            Assert.IsTrue(cg2.G < cg.G);
            Assert.IsTrue(cb2.B < cb.B);
        }

        [TestMethod]
        public void Test_ColorShader_Tint()
        {
            // Arrange

            Color cr = Color.Red;
            Color cg = Color.Green;
            Color cb = Color.Blue;

            // Act

            Color cr2 = ColorShader.Tint(cr);
            Color cg2 = ColorShader.Tint(cg);
            Color cb2 = ColorShader.Tint(cb);

            // Assert

            Assert.IsTrue(cr2.G > cr.G);
            Assert.IsTrue(cr2.B > cr.B);

            Assert.IsTrue(cg2.R > cg.R);
            Assert.IsTrue(cg2.B > cg.B);

            Assert.IsTrue(cb2.R > cb.R);
            Assert.IsTrue(cb2.G > cb.G);
        }

        [TestMethod]
        public void Test_Copy_Constructor_Border()
        {
            // Arrange

            CSSBorder orig = new CSSBorder();
            CSSBorder copy = new CSSBorder(orig);

            // Act

            // Assert

            Assert.AreEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Border_Constructor_Border_Uniqueness()
        {
            // Arrange

            CSSBorder orig = new CSSBorder();
            CSSBorder copy = new CSSBorder(orig);

            // Act

            copy.SetMutationChance(1);
            copy.Mutate();

            // Assert

            Assert.AreNotEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Button()
        {
            // Arrange

            CSSButton orig = new CSSButton();
            CSSButton copy = new CSSButton(orig);

            // Act

            // Assert

            Assert.AreEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Button_Uniqueness()
        {
            // Arrange

            CSSButton orig = new CSSButton();
            CSSButton copy = new CSSButton(orig);

            // Act

            copy.SetMutationChance(1);
            copy.Mutate();

            // Assert

            Assert.AreNotEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Input()
        {
            // Arrange

            CSSInput orig = new CSSInput();
            CSSInput copy = new CSSInput(orig);

            // Act

            // Assert

            Assert.AreEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Input_Uniqueness()
        {
            // Arrange

            CSSInput orig = new CSSInput();
            CSSInput copy = new CSSInput(orig);

            // Act

            copy.SetMutationChance(1);
            copy.Mutate();

            // Assert

            Assert.AreNotEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Table()
        {
            // Arrange

            CSSTable orig = new CSSTable();
            CSSTable copy = new CSSTable(orig);

            // Act

            // Assert

            Assert.AreEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Table_Uniqueness()
        {
            // Arrange

            CSSTable orig = new CSSTable();
            CSSTable copy = new CSSTable(orig);

            // Act

            copy.SetMutationChance(1);
            copy.Mutate();

            // Assert

            Assert.AreNotEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Text()
        {
            // Arrange

            CSSText orig = new CSSText();
            CSSText copy = new CSSText(orig);

            // Act

            // Assert

            Assert.AreEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Copy_Constructor_Text_Uniqueness()
        {
            // Arrange

            CSSText orig = new CSSText();
            CSSText copy = new CSSText(orig);

            // Act

            copy.SetMutationChance(1);
            copy.Mutate();

            // Assert

            Assert.AreNotEqual(orig.ToString(), copy.ToString());
        }

        [TestMethod]
        public void Test_Seed_Constructor_Border()
        {
            // Arrange

            CSSBorder b = new CSSBorder(1337);
            CSSBorder h = new CSSBorder(1337);

            // Act

            // Assert

            Assert.AreEqual(b.ToString(), h.ToString());
        }

        [TestMethod]
        public void Test_Seed_Constructor_Button()
        {
            // Arrange

            CSSButton b = new CSSButton(1337);
            CSSButton h = new CSSButton(1337);

            // Act

            // Assert

            Assert.AreEqual(b.ToString(), h.ToString());
        }

        [TestMethod]
        public void Test_Seed_Constructor_Input()
        {
            // Arrange

            CSSInput b = new CSSInput(1337);
            CSSInput h = new CSSInput(1337);

            // Act

            // Assert

            Assert.AreEqual(b.ToString(), h.ToString());
        }

        [TestMethod]
        public void Test_Seed_Constructor_Table()
        {
            // Arrange

            CSSTable b = new CSSTable(1337);
            CSSTable h = new CSSTable(1337);

            // Act

            // Assert

            Assert.AreEqual(b.ToString(), h.ToString());
        }

        [TestMethod]
        public void Test_ParseBundle_Constructor_NumberOfElements()
        {
            // Arrange

            CSSButton cb = new CSSButton();

            ParseBundle b = new ParseBundle
            {
                BGColor = Color.Red,
                HoverColor = Color.Gray,
                ShadowColor = Color.PaleGoldenrod,
                BorderColors = new[] { Color.Green, Color.Wheat },
                NoUniqueBorderColors = 2,
                BorderStyles = new[] { 0, 2, 1 },
                NoUniqueBorderStyles = 3,
                BorderWidths = new[] { 3 },
                NoUniqueBorderWidths = 1,
                BorderRadii = new[] { 50, 120, 10, 1 },
                NoUniqueBorderRadii = 4,
                ShadowHPos = 0,
                ShadowVPos = 10,
                ShadowBlur = 4,
                FontFamilyID = 3,
                FontSize = 20,
                Paddings = new[] { 10 },
                NoUniquePaddings = 1,
                UseShadeHover = false,
                TransparentBG = 1
            };

            // Act

            CSSButton cp = new CSSButton(b);

            // Assert

            Assert.AreEqual(cb.Length, cp.Length);
        }

        [TestMethod]
        public void Test_ParseBundle_Constructor_Values()
        {
            // Arrange

            CSSButton cb = new CSSButton(42);

            ParseBundle b = new ParseBundle
            {
                BGColor = cb.BGColor,
                HoverColor = cb.HoverColorRaw,
                ShadowColor = cb.ShadowColor,
                BorderColors = cb.BorderColors,
                NoUniqueBorderColors = cb.NoUniqueBorderColors,
                BorderStyles = cb.BorderStyles,
                NoUniqueBorderStyles = cb.NoUniqueBorderStyles,
                BorderWidths = cb.BorderWidths,
                NoUniqueBorderWidths = cb.NoUniqueBorderWidths,
                BorderRadii = cb.BorderRadiiRaw,
                NoUniqueBorderRadii = cb.NoUniqueBorderRadii,
                ShadowHPos = cb.ShadowHPos,
                ShadowVPos = cb.ShadowVPos,
                ShadowBlur = cb.ShadowBlur,
                FontFamilyID = cb.FontFamilyID,
                FontSize = cb.FontSizeRaw,
                Paddings = cb.Paddings,
                NoUniquePaddings = cb.NoUniquePaddings,
                UseShadeHover = cb.UseShadeHover,
                TransparentBG = cb.TransparentBGRaw,
                UseGradient = cb.UseGradient,
                Gradient = cb.Gradient
            };

            // Act

            CSSButton cp = new CSSButton(b);

            // Assert

            Assert.AreEqual(cb.ToString(), cp.ToString());
        }

        [TestMethod]
        public void Test_Lock_Crossover()
        {
            // Arrange

            ChromosomeBase.CommonLockList[0] = true;
            ChromosomeBase.CommonLockList[5] = true;
            ChromosomeBase.CommonLockList[15] = true;
            ChromosomeBase.CommonLockList[27] = true;
            ChromosomeBase.CommonLockList[41] = true;
            ChromosomeBase.CommonLockList[50] = true;

            var a = new CSSButton(13);
            var b = new CSSButton(37);

            ChromosomeBase.Target = a;

            int q = a.BGColor.A;
            int w = a.HoverColorRaw.R;
            int e = a.BorderColors[0].B;
            int r = a.BorderColors[3].B;
            int t = a.BorderRadiiRaw[2];
            int y = a.Paddings[1];

            // Act

            a.PropLockCrossover(b);

            // Assert

            Assert.AreEqual(q, a.BGColor.A);
            Assert.AreEqual(w, a.HoverColorRaw.R);
            Assert.AreEqual(e, a.BorderColors[0].B);
            Assert.AreEqual(r, a.BorderColors[3].B);
            Assert.AreEqual(t, a.BorderRadiiRaw[2]);
            Assert.AreEqual(y, a.Paddings[1]);
        }
    }
}
