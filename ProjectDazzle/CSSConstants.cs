﻿using System;
using System.Drawing;
using System.Globalization;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSConstants.cs - Static helper class used to map integer values to their respective CSS strings.
    /// Since many properties of CSS are represented using specific syntax, predefined strings had to be made.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public static class CSSConstants
    {
        public static string Meta
        {
            get
            {
                return "@charset \"utf-8\";\n/* CSS Document */\n";
            }
        }
        
        /// <summary>
        /// Creates a CSS class string with name and properties
        /// </summary>
        /// <param name="name">Name of class</param>
        /// <param name="args">List of properties</param>
        /// <returns></returns>
        public static string ClassTag(string name, params string[] args)
        {
            string result = $"\n{name}{{\n";

            for (int i = 0; i < args.Length; i++)
                result += $"\t{args[i]}{((i + 1) >= args.Length ? "}\n" : "")}";

            return result;
        }

        public static string TextDecoration(string type)
        {
            return $"text-decoration:{type};\n";
        }

        public static string TextTransform(string type)
        {
            return $"text-transform:{type};\n";
        }

        public static string Position(string pos)
        {
            return $"position:{pos};\n";
        }

        /// <summary>
        /// Creates the shadow string with correct syntax
        /// </summary>
        /// <param name="type">text-, box-</param>
        /// <param name="args"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string Shadow(string type, string args, Color color)
        {
            return $"{type}shadow:{args} {RGBA(color)};\n";
        }

        /// <summary>
        /// Creates the width string with correct syntax
        /// </summary>
        /// <param name="type">max-, min-</param>
        /// <param name="value"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public static string Width(string type, double value, string unit)
        {
            return $"{type}width:{value} {unit};\n";
        }

        /// <summary>
        /// Creates the height string with correct syntax
        /// </summary>
        /// <param name="type">max-, min-</param>
        /// <param name="value"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public static string Height(string type, double value, string unit)
        {
            return $"{type}height:{value.ToString(new CultureInfo("en-US"))}{unit};\n";
        }

        public static string Align(string pos)
        {
            return $"text-align:{pos};\n";
        }

        /// <summary>
        /// Creates the padding string with standard syntax
        /// </summary>
        /// <param name="pos">-left, -right, -top, -bottom</param>
        /// <param name="value"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public static string Padding(string pos, double value, string unit)
        {
            return $"padding{pos}:{value}{unit};\n";
        }

        /// <summary>
        /// Creates the padding string with shorthand syntax
        /// </summary>
        /// <param name="unit">px, %</param>
        /// <param name="count">Number of paddings</param>
        /// <param name="values">List of values</param>
        /// <returns></returns>
        public static string Padding(string unit, int count, int[] values)
        {
            string result = "padding:";

            for (int i = 0; i <= count; i++)
                result += $"{values[i]}{unit}{((i + 1) > count ? ";\n" : " ")}";

            return result;
        }

        /// <summary>
        /// Creates the font family string with correct syntax
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string FontFamily(params string[] args)
        {
            string result = "font-family:";

            for (int i = 0; i < args.Length; i++)
                result += $"{args[i]}{((i + 1) >= args.Length ? ";\n" : ",")}";

            return result;
        }

        public static string FontSize(int value, string unit)
        {
            return $"font-size:{value}{unit};\n";
        }

        public static string BorderCollapse(bool collapse)
        {
            return collapse ? "border-collapse:collapse;\n" : "";
        }

        public static string Border(int border)
        {
            return $"border:{border};\n";
        }

        /// <summary>
        /// Creates border string with shorthand syntax
        /// </summary>
        /// <param name="border"></param>
        /// <param name="unit"></param>
        /// <param name="type"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string Border(int border, string unit, string type, Color color)
        {
            return $"border:{border}{unit} {type} {RGBA(color)};\n";
        }

        /// <summary>
        /// Creates various border strings using correct syntax
        /// </summary>
        /// <param name="property">Radius, style, width etc</param>
        /// <param name="unit">px, %</param>
        /// <param name="count">Number of values</param>
        /// <param name="values">List of values</param>
        /// <returns></returns>
        public static string Border(string property, string unit, int count, int[] values)
        {
            string result = $"border-{property}:";

            for (int i = 0; i <= count; i++)
                result += $"{values[i]}{unit}{((i + 1) > count ? ";\n" : " ")}";

            return result;
        }

        /// <summary>
        /// Creates uniform border using shorthand syntax
        /// </summary>
        /// <param name="pos">top, bottom, left, right</param>
        /// <param name="border"></param>
        /// <param name="unit"></param>
        /// <param name="solid"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string Border(string pos, int border, string unit, string type, Color color)
        {
            return $"border-{pos}:{border}{unit} {type} {RGBA(color)};\n";
        }

        // Special mapping function for table borders
        public static string BorderMultiConfig(int conf, int border, string unit, string type, Color color)
        {
            switch (conf)
            {
                case 0:
                    return Border("top", border, unit, type, color);
                case 1:
                    return Border("right", border, unit, type, color);
                case 2:
                    return Border("bottom", border, unit, type, color);
                case 3:
                    return Border("left", border, unit, type, color);
                case 4:
                    return Border("top", border, unit, type, color) + Border("bottom", border, unit, type, color);
                case 5:
                    return Border("left", border, unit, type, color) + Border("right", border, unit, type, color);
                case 6:
                    return Border("top", border, unit, type, color) + Border("right", border, unit, type, color);
                case 7:
                    return Border("top", border, unit, type, color) + Border("left", border, unit, type, color);
                case 8:
                    return Border("bottom", border, unit, type, color) + Border("right", border, unit, type, color);
                case 9:
                    return Border("bottom", border, unit, type, color) + Border("left", border, unit, type, color);
                case 10:
                    return Border(border, unit, type, color);
                default:
                    return Border(0);
            }
        }

        // Mapping for standard border style syntax
        public static string BorderStyles(int count, int[] styles)
        {
            string result = "border-style:";

            for (int i = 0; i <= count; i++)
                result += $"{HTMLHelper.GetBorderStyleString(styles[i])}{((i + 1) > count ? ";\n" : " ")}";

            return result;
        }

        // Mapping for standard border color syntax
        public static string BorderColors(int count, Color[] colors)
        {
            string result = "border-color:";

            for (int i = 0; i <= count; i++)
                result += $"{RGBA(colors[i])}{((i + 1) > count ? ";\n" : " ")}";

            return result;
        }

        // Mapping for standard border radius syntax
        public static string BorderRadius(double value, string unit)
        {
            return $"border-radius:{value}{unit};\n";
        }

        // Mapping for word- and letter-spacing
        public static string Spacing(string type, int value, string unit)
        {
            return $"{type}spacing:{value}{unit};\n";
        }

        /// <summary>
        /// Creates color string using correct syntax.
        /// </summary>
        /// <param name="type">background-, foreground-</param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string Color(string type, Color color)
        {
            return $"{type}color:{RGBA(color)};\n";
        }

        // Background mapping
        public static string Background(string value)
        {
            return $"background:{value};\n";
        }

        // "Overloader" function for various background styles
        public static string Background(ChromosomeBase c)
        {
            if (c.TransparentBG)
                return Background("0");
            else if (c.UseGradient)
                return Gradient(c.BGColor, c.Gradient);
            else
                return Background(RGBA(c.BGColor));
        }

        // Creates gradient color string with correct syntax
        // Only linear, top-to-bottom gradient for now
        public static string Gradient(Color grad1, Color grad2)
        {
            return $"{Background($"linear-gradient({RGBA(grad1)}, {RGBA(grad2)})")}";
        }

        // Creates rgba color string based on color
        public static string RGBA(Color color)
        {
            return (color.IsKnownColor ? color.Name : $"rgba({color.R}, {color.G}, {color.B}, {Math.Round(color.A / 255.0, 1).ToString(new CultureInfo("en-US"))})");
        }
    }
}
