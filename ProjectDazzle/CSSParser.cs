﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    // Data structure for parsed CSS data
    public struct ParseBundle
    {
        public Color BGColor;
        public Color HoverColor;
        public Color ShadowColor;
        public Color[] BorderColors;
        public int NoUniqueBorderColors;
        public int[] BorderStyles;
        public int NoUniqueBorderStyles;
        public int[] BorderWidths;
        public int NoUniqueBorderWidths;
        public int[] BorderRadii;
        public int NoUniqueBorderRadii;
        public int ShadowHPos;
        public int ShadowVPos;
        public int ShadowBlur;
        public int FontFamilyID;
        public int FontSize;
        public int[] Paddings;
        public int NoUniquePaddings;
        public bool UseShadeHover;
        public int TransparentBG;
        public bool UseGradient;
        public Color Gradient;
    }

    /// <summary>
    /// CSSParser.css - Simple parser to extract basic features from CSS files.
    /// Parses all .css-files in directory.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public static class CSSParser
    {
        private static List<ChromosomeBase> _parsedDesigns;

#if DEBUG
        private const string _inputFolder = "..\\..\\..\\HTML\\input\\";
#else
        private const string _inputFolder = "\\HTML\\input\\";
#endif

        public static List<ChromosomeBase> ParseCSSFiles()
        {
            var files = Directory.GetFiles($"{AppDomain.CurrentDomain.BaseDirectory}\\{_inputFolder}");

            _parsedDesigns = new List<ChromosomeBase>();

            for (int i = 0; i < files.Length; i++)
            {
                string subs = files[i].Substring(files[i].Length - 3, 3);

                // Find all css files
                if (subs.ToLower() == "css")
                {
                    string file = File.ReadAllText(files[i]);
                    List<string> classes = file.Split('}').ToList();

                    for (int k = 0; k < classes.Count; k++)
                    {
                        var c = TryParse(classes[k]);

                        if (c != null)
                            _parsedDesigns.Add(c);
                    }
                }
            }

            ChromosomeBase TryParse(string input)
            {
                // Split properties into array of values
                List<string> props = input.Split(';', '{', '}').ToList();

                ParseBundle bundle = new ParseBundle();

                for (int i = 0; i < props.Count; i++)
                {
                    // Find padding values
                    if (props[i].Contains("padding:"))
                    {
                        bundle.Paddings = ToIntArray(ExtractNumbers(props[i]));
                        bundle.NoUniquePaddings = bundle.Paddings.Length - 1;
                    }
                    // Find background values
                    else if (props[i].Contains("background:"))
                    {
                        // Find linear gradient
                        if (props[i].Contains("linear-gradient"))
                        {
                            var colors = ExtractGradientColors(props[i]);

                            bundle.BGColor = colors[0];
                            bundle.Gradient = colors[1];
                            bundle.UseGradient = true;
                        }
                        else
                        {
                            // Solid color
                            var bgc = ExtractColors(props[i]).First();

                            if (bundle.BGColor == Color.Empty)
                            {
                                if (bgc.Name == "0")
                                    bundle.TransparentBG = 1;
                                else
                                    bundle.BGColor = bgc;
                            }
                        }
                    }
                    // Find border colors on shorthand form
                    else if (props[i].Contains("border-color:"))
                    {
                        bundle.BorderColors = ExtractColors(props[i]);
                        bundle.NoUniqueBorderColors = bundle.BorderColors.Length - 1;
                    }
                    // Find text color value
                    else if (props[i].Contains("\tcolor:"))
                    {
                        bundle.ShadowColor = ExtractColors(props[i]).First();
                    }
                    // Find font-size value
                    else if (props[i].Contains("font-size:"))
                    {
                        bundle.FontSize = ToIntArray(ExtractNumbers(props[i])).First();
                    }
                    // Find border styles on shorthand form
                    else if (props[i].Contains("border-style:"))
                    {
                        bundle.BorderStyles = ExtractBorderStyles(props[i]);
                        bundle.NoUniqueBorderStyles = bundle.BorderStyles.Length - 1;
                    }
                    // Find border widths on shorthand form
                    else if (props[i].Contains("border-width:"))
                    {
                        bundle.BorderWidths = ToIntArray(ExtractNumbers(props[i]));
                        bundle.NoUniqueBorderWidths = bundle.BorderWidths.Length - 1;
                    }
                    // Find border radii on shorthand form
                    else if (props[i].Contains("border-radius:"))
                    {
                        bundle.BorderRadii = ToIntArray(ExtractNumbers(props[i], 128));
                        bundle.NoUniqueBorderRadii = bundle.BorderRadii.Length - 1;
                    }
                }

                // Helper function to extract style strings
                int[] ExtractBorderStyles(string s)
                {
                    var bs = s.Split(':', ' ').ToList();
                    List<int> ss = new List<int>();

                    for (int j = 1; j < bs.Count; j++)
                        if (bs[j].Length > 0)
                            ss.Add(HTMLHelper.GetBorderStyleId(bs[j]));

                    return ss.ToArray();
                }

                // c is helper variable for offset properties like radius, discussed in report
                double[] ExtractNumbers(string s, int c = 0)
                {
                    List<string> efs = s.Split(':', ' ').ToList();
                    List<double> values = new List<double>();

                    for (int j = 0; j < efs.Count; j++)
                    {
                        // http://stackoverflow.com/questions/4734116/find-and-extract-a-number-from-a-string
                        // and http://stackoverflow.com/questions/11061550/extract-decimal-from-start-of-string 03/04-17
                        // Running this code on a Norwegian culture machine breaks . as decimal separator, needs cultureInfo
                        // https://msdn.microsoft.com/en-us/library/3s27fasw(v=vs.110).aspx
                        // Using regex to extract integers from css-string
                        var sfdfsdf = Regex.Match(efs[j], @"\d+(\.\d{1,2})?");
                        if (Double.TryParse(sfdfsdf.Value, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out double v))
                            values.Add(v + c);
                    }

                    return values.ToArray();
                }

                // Helper to convert double values to int values in array
                int[] ToIntArray(double[] arr)
                {
                    int[] result = new int[arr.Count()];

                    for (int i = 0; i < result.Count(); i++)
                        result[i] = (int)arr[i];

                    return result;
                }

                Color[] ExtractGradientColors(string s)
                {
                    // This string needs some preprocessing to work with ExtractColors, due
                    // to its different syntax compared to other multiple color set-ups

                    int si = s.IndexOf('l'); // Start index (Linear-gradient)
                    int ei = s.LastIndexOf('t'); // End index (linear-gradient)

                    string processed = s.Remove(si, ei - si + 2);
                    processed = processed.Replace("),", ":");

                    return ExtractColors(processed);
                }

                // Finds colors on rgba(r, g, b, a), #HEX and name form
                Color[] ExtractColors(string s)
                {
                    List<string> efs;
                    if (s.Contains("rgba"))
                        efs = s.Split(':', ')').ToList();
                    else
                        efs = s.Split(':', '(').ToList();
                    List<Color> colors = new List<Color>();

                    for (int j = 1; j < efs.Count; j++)
                    {
                        if (efs[j].Length > 0)
                        {
                            if (efs[j].Trim().Substring(0, 1) == "#")
                            {
                                List<string> hex = efs[j].Split('#').ToList();
                                for (int k = 1; k < hex.Count; k++)
                                    colors.Add(ColorTranslator.FromHtml($"#{hex[k]}"));
                            }
                            else if (efs[j].Length > 3)
                            {
                                if (efs[j].Trim().Substring(0, 4) == "rgba")
                                {
                                    var rgba = ExtractNumbers(efs[j]);
                                    colors.Add(Color.FromArgb((int)(rgba[3] * 255), (int)rgba[0], (int)rgba[1], (int)rgba[2]));
                                }
                                else
                                {
                                    var tc = Color.FromName(efs[j].Trim());
                                    if (tc.IsKnownColor)
                                        colors.Add(tc);
                                }
                            }
                            else
                                colors.Add(Color.FromName(efs[j]));
                        }
                    }

                    return colors.ToArray();
                }

                return bundle.Equals(default(ParseBundle)) ? null : new CSSButton(bundle);
            }

            return _parsedDesigns;
        }
    }
}
