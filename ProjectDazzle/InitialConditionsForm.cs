﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDazzle
{
    /// <summary>
    /// InitialConditionsForm.cs - User interface class to let the user manually set the range of the random number
    /// generation for button elements. Data is stored in the InitialBunde data structure, and passed to the special
    /// chromosome constructor.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class InitialConditionsForm : Form
    {
        ElementViewer _parent;

        public InitialConditionsForm(ElementViewer parent)
        {
            InitializeComponent();

            _parent = parent;

            var fontFamList = new List<ComboHelper>();
            var borderStyleList = new List<ComboHelper>();

            // Populate drop-down lists
            for (int i = 0; i < 32; i++)
            {
                fontFamList.Add(new ComboHelper() { ID = i, Name = $"{HTMLHelper.GetFontFamily(i)} ({i})" });
            }

            for (int i = 0; i < 10; i++)
            {
                borderStyleList.Add(new ComboHelper() { ID = i, Name = $"{HTMLHelper.GetBorderStyleString(i)} ({ i })" });
            }

            cmbFontFamMin.DataSource = new List<ComboHelper>(fontFamList);
            cmbFontFamMin.DisplayMember = "Name";
            cmbFontFamMin.ValueMember = "ID";
            cmbFontFamMin.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbFontFamMax.DataSource = new List<ComboHelper>(fontFamList);
            cmbFontFamMax.DisplayMember = "Name";
            cmbFontFamMax.ValueMember = "ID";
            cmbFontFamMax.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbFontFamMax.SelectedIndex = cmbFontFamMax.Items.Count - 1;

            cmbBStyle1Min.DataSource = new List<ComboHelper>(borderStyleList);
            cmbBStyle1Min.DisplayMember = "Name";
            cmbBStyle1Min.ValueMember = "ID";
            cmbBStyle1Min.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBStyle1Max.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle1Max.DisplayMember = "Name";
            cmbBStyle1Max.ValueMember = "ID";
            cmbBStyle1Max.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbBStyle1Max.SelectedIndex = cmbBStyle1Max.Items.Count - 1;

            cmbBStyle2Min.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle2Min.DisplayMember = "Name";
            cmbBStyle2Min.ValueMember = "ID";
            cmbBStyle2Min.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBStyle2Max.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle2Max.DisplayMember = "Name";
            cmbBStyle2Max.ValueMember = "ID";
            cmbBStyle2Max.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbBStyle2Max.SelectedIndex = cmbBStyle2Max.Items.Count - 1;

            cmbBStyle3Min.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle3Min.DisplayMember = "Name";
            cmbBStyle3Min.ValueMember = "ID";
            cmbBStyle3Min.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBStyle3Max.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle3Max.DisplayMember = "Name";
            cmbBStyle3Max.ValueMember = "ID";
            cmbBStyle3Max.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbBStyle3Max.SelectedIndex = cmbBStyle3Max.Items.Count - 1;

            cmbBStyle4Min.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle4Min.DisplayMember = "Name";
            cmbBStyle4Min.ValueMember = "ID";
            cmbBStyle4Min.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBStyle4Max.DataSource = new List<ComboHelper>(borderStyleList); ;
            cmbBStyle4Max.DisplayMember = "Name";
            cmbBStyle4Max.ValueMember = "ID";
            cmbBStyle4Max.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbBStyle4Max.SelectedIndex = cmbBStyle4Max.Items.Count - 1;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var minList = new List<int>();
            var maxList = new List<int>();
            var lengths = new List<int>();
   
            // Add the minimum values to list
            minList.Add((int)nudBGAlphaMin.Value);
            minList.Add((int)nudBGRedMin.Value);
            minList.Add((int)nudBGGreenMin.Value);
            minList.Add((int)nudBGBlueMin.Value);

            minList.Add((int)nudHoverAlphaMin.Value);
            minList.Add((int)nudHoverRedMin.Value);
            minList.Add((int)nudHoverGreenMin.Value);
            minList.Add((int)nudHoverBlueMin.Value);

            minList.Add((int)nudShadowAlphaMin.Value);
            minList.Add((int)nudShadowRedMin.Value);
            minList.Add((int)nudShadowGreenMin.Value);
            minList.Add((int)nudShadowBlueMin.Value);

            minList.Add((int)nudBC1AMin.Value);
            minList.Add((int)nudBC1RMin.Value);
            minList.Add((int)nudBC1GMin.Value);
            minList.Add((int)nudBC1BMin.Value);

            minList.Add((int)nudBC2AMin.Value);
            minList.Add((int)nudBC2RMin.Value);
            minList.Add((int)nudBC2GMin.Value);
            minList.Add((int)nudBC2BMin.Value);

            minList.Add((int)nudBC3AMin.Value);
            minList.Add((int)nudBC3RMin.Value);
            minList.Add((int)nudBC3GMin.Value);
            minList.Add((int)nudBC3BMin.Value);

            minList.Add((int)nudBC4AMin.Value);
            minList.Add((int)nudBC4RMin.Value);
            minList.Add((int)nudBC4GMin.Value);
            minList.Add((int)nudBC4BMin.Value);

            minList.Add((int)nudUniqBColors.Value - 1);

            minList.Add((cmbBStyle1Min.SelectedItem as ComboHelper).ID);
            minList.Add((cmbBStyle2Min.SelectedItem as ComboHelper).ID);
            minList.Add((cmbBStyle3Min.SelectedItem as ComboHelper).ID);
            minList.Add((cmbBStyle4Min.SelectedItem as ComboHelper).ID);

            minList.Add((int)nudUniqBStyles.Value - 1);

            minList.Add((int)nudBW1Min.Value);
            minList.Add((int)nudBW2Min.Value);
            minList.Add((int)nudBW3Min.Value);
            minList.Add((int)nudBW4Min.Value);

            minList.Add((int)nudUniqBWidths.Value - 1);

            minList.Add((int)nudBr1Min.Value + 128);
            minList.Add((int)nudBr2Min.Value + 128);
            minList.Add((int)nudBr3Min.Value + 128);
            minList.Add((int)nudBr4Min.Value + 128); // Offsets

            minList.Add((int)nudUniqBRad.Value - 1);

            minList.Add((int)nudShadHMin.Value);
            minList.Add((int)nudShadVMin.Value);
            minList.Add((int)nudShadBlurMin.Value);

            minList.Add((cmbFontFamMin.SelectedItem as ComboHelper).ID);
            minList.Add((int)nudFontSzMin.Value);

            minList.Add((int)nudPad1Min.Value);
            minList.Add((int)nudPad2Min.Value);
            minList.Add((int)nudPad3Min.Value);
            minList.Add((int)nudPad4Min.Value);

            minList.Add((int)nudUniqPadd.Value - 1);

            minList.Add(0);

            minList.Add(chkTransparent.Checked ? 1 : 0);

            minList.Add(chkGradient.Checked ? 1 : 0);

            minList.Add((int)nudGradRedMin.Value);
            minList.Add((int)nudGradGreenMin.Value);
            minList.Add((int)nudGradBlueMin.Value);
            minList.Add((int)nudGradAlphaMin.Value);

            /////////////////////////MAX/////////////////////

            maxList.Add((int)nudBGAlphaMax.Value);
            maxList.Add((int)nudBGRedMax.Value);
            maxList.Add((int)nudBGGreenMax.Value);
            maxList.Add((int)nudBGBlueMax.Value);

            maxList.Add((int)nudHoverAlphaMax.Value);
            maxList.Add((int)nudHoverRedMax.Value);
            maxList.Add((int)nudHoverGreenMax.Value);
            maxList.Add((int)nudHoverBlueMax.Value);

            maxList.Add((int)nudShadowAlphaMax.Value);
            maxList.Add((int)nudShadowRedMax.Value);
            maxList.Add((int)nudShadowGreenMax.Value);
            maxList.Add((int)nudShadowBlueMax.Value);

            maxList.Add((int)nudBC1AMax.Value);
            maxList.Add((int)nudBC1RMax.Value);
            maxList.Add((int)nudBC1GMax.Value);
            maxList.Add((int)nudBC1BMax.Value);

            maxList.Add((int)nudBC2AMax.Value);
            maxList.Add((int)nudBC2RMax.Value);
            maxList.Add((int)nudBC2GMax.Value);
            maxList.Add((int)nudBC2BMax.Value);

            maxList.Add((int)nudBC3AMax.Value);
            maxList.Add((int)nudBC3RMax.Value);
            maxList.Add((int)nudBC3GMax.Value);
            maxList.Add((int)nudBC3BMax.Value);

            maxList.Add((int)nudBC4AMax.Value);
            maxList.Add((int)nudBC4RMax.Value);
            maxList.Add((int)nudBC4GMax.Value);
            maxList.Add((int)nudBC4BMax.Value);
            
            maxList.Add((int)nudUniqBColors.Value - 1);
            
            maxList.Add((cmbBStyle1Max.SelectedItem as ComboHelper).ID);
            maxList.Add((cmbBStyle2Max.SelectedItem as ComboHelper).ID);
            maxList.Add((cmbBStyle3Max.SelectedItem as ComboHelper).ID);
            maxList.Add((cmbBStyle4Max.SelectedItem as ComboHelper).ID);
            
            maxList.Add((int)nudUniqBStyles.Value - 1);
            
            maxList.Add((int)nudBW1Max.Value);
            maxList.Add((int)nudBW2Max.Value);
            maxList.Add((int)nudBW3Max.Value);
            maxList.Add((int)nudBW4Max.Value);
            
            maxList.Add((int)nudUniqBWidths.Value - 1);
            
            maxList.Add((int)nudBr1Max.Value + 128);
            maxList.Add((int)nudBr2Max.Value + 128);
            maxList.Add((int)nudBr3Max.Value + 128);
            maxList.Add((int)nudBr4Max.Value + 128);
            
            maxList.Add((int)nudUniqBRad.Value - 1);
            
            maxList.Add((int)nudShadHMax.Value);
            maxList.Add((int)nudShadVMax.Value);
            maxList.Add((int)nudShadBlurMax.Value);
            
            maxList.Add((cmbFontFamMax.SelectedItem as ComboHelper).ID);
            maxList.Add((int)nudFontSzMax.Value);
            
            maxList.Add((int)nudPad1Max.Value);
            maxList.Add((int)nudPad2Max.Value);
            maxList.Add((int)nudPad3Max.Value);
            maxList.Add((int)nudPad4Max.Value);
            
            maxList.Add((int)nudUniqPadd.Value - 1);

            maxList.Add(0);

            maxList.Add(chkTransparent.Checked ? 1 : 0);

            maxList.Add(chkGradient.Checked ? 1 : 0);

            maxList.Add((int)nudGradRedMax.Value);
            maxList.Add((int)nudGradGreenMax.Value);
            maxList.Add((int)nudGradBlueMax.Value);
            maxList.Add((int)nudGradAlphaMax.Value);

            ////////////LENGTHS///////////////

            for (int i = 0; i < 28; i++)
                lengths.Add(ChromosomeBase.TL255);

            lengths.Add(ChromosomeBase.TL3);

            for (int i = 0; i < 4; i++)
                lengths.Add(ChromosomeBase.TL15);

            lengths.Add(ChromosomeBase.TL3);

            for (int i = 0; i < 4; i++)
                lengths.Add(ChromosomeBase.TL15);

            lengths.Add(ChromosomeBase.TL3);

            for (int i = 0; i < 4; i++)
                lengths.Add(ChromosomeBase.TL255);

            lengths.Add(ChromosomeBase.TL3);

            for (int i = 0; i < 3; i++)
                lengths.Add(ChromosomeBase.TL15);

            lengths.Add(ChromosomeBase.TL15);
            lengths.Add(ChromosomeBase.TL31);

            for (int i = 0; i < 4; i++)
                lengths.Add(ChromosomeBase.TL31);

            lengths.Add(ChromosomeBase.TL3);

            lengths.Add(ChromosomeBase.TLBool);

            lengths.Add(ChromosomeBase.TL7);

            lengths.Add(ChromosomeBase.TLBool);

            for (int i = 0; i < 4; i++)
                lengths.Add(ChromosomeBase.TL255);

            /////MAX VALUES FOR NORMALIZATION///////

            var maxValues = Enumerable.Repeat(-1, minList.Count).ToList();
            // Special cases
            maxValues[29] = 9;
            maxValues[30] = 9;
            maxValues[31] = 9;
            maxValues[32] = 9;

            InitialBundle b = new InitialBundle()
            {
                MinValues = minList,
                MaxValues = maxList,
                Lengths = lengths,
                MaxValuesValue = maxValues
            };

            // Pass the data to the main window
            _parent.SetInitialBundle(b);

            Close();
        }

        // Helper functions to update GUI based on user choices
        private void nudUniqBColors_ValueChanged(object sender, EventArgs e)
        {
            grbC2.Enabled = nudUniqBColors.Value > 1;
            grbC3.Enabled = nudUniqBColors.Value > 2;
            grbC4.Enabled = nudUniqBColors.Value > 3;
        }

        private void nudUniqBStyles_ValueChanged(object sender, EventArgs e)
        {
            cmbBStyle2Min.Enabled = nudUniqBStyles.Value > 1;
            cmbBStyle2Max.Enabled = nudUniqBStyles.Value > 1;
            cmbBStyle3Min.Enabled = nudUniqBStyles.Value > 2;
            cmbBStyle3Max.Enabled = nudUniqBStyles.Value > 2;
            cmbBStyle4Min.Enabled = nudUniqBStyles.Value > 3;
            cmbBStyle4Max.Enabled = nudUniqBStyles.Value > 3;
        }

        private void nudUniqBWidths_ValueChanged(object sender, EventArgs e)
        {
            nudBW2Min.Enabled = nudUniqBWidths.Value > 1;
            nudBW2Max.Enabled = nudUniqBWidths.Value > 1;
            nudBW3Min.Enabled = nudUniqBWidths.Value > 2;
            nudBW3Max.Enabled = nudUniqBWidths.Value > 2;
            nudBW4Min.Enabled = nudUniqBWidths.Value > 3;
            nudBW4Max.Enabled = nudUniqBWidths.Value > 3;
        }

        private void nudUniqBRad_ValueChanged(object sender, EventArgs e)
        {
            nudBr2Min.Enabled = nudUniqBRad.Value > 1;
            nudBr2Max.Enabled = nudUniqBRad.Value > 1;
            nudBr3Min.Enabled = nudUniqBRad.Value > 2;
            nudBr3Max.Enabled = nudUniqBRad.Value > 2;
            nudBr4Min.Enabled = nudUniqBRad.Value > 3;
            nudBr4Max.Enabled = nudUniqBRad.Value > 3;
        }

        private void nudUniqPadd_ValueChanged(object sender, EventArgs e)
        {
            nudPad2Min.Enabled = nudUniqPadd.Value > 1;
            nudPad2Max.Enabled = nudUniqPadd.Value > 1;
            nudPad3Min.Enabled = nudUniqPadd.Value > 2;
            nudPad3Max.Enabled = nudUniqPadd.Value > 2;
            nudPad4Min.Enabled = nudUniqPadd.Value > 3;
            nudPad4Max.Enabled = nudUniqPadd.Value > 3;
        }

        private void chkGradient_CheckedChanged(object sender, EventArgs e)
        {
            grbGradient.Enabled = chkGradient.Checked;
            chkTransparent.Enabled = !grbGradient.Enabled;
        }

        private void chkTransparent_CheckedChanged(object sender, EventArgs e)
        {
            grbBGColor.Enabled = !chkTransparent.Checked;
            chkGradient.Enabled = grbBGColor.Enabled;
        }
    }

    public class ComboHelper
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public struct InitialBundle
    {
        public List<int> MinValues;
        public List<int> MaxValues;
        public List<int> Lengths;
        public List<int> MaxValuesValue;
    }
}
