﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSInput.cs - CSS properties for an HTML input field.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class CSSInput : ChromosomeBase
    {
        public CSSInput() : base(EDesignElement.Input)
        {
            MakeWeights();
        }

        public CSSInput(int seed) : base(EDesignElement.Input, seed)
        {
            MakeWeights();
        }

        public CSSInput(CSSInput copy) : base (copy)
        {
        }

        public override ChromosomeBase Clone()
        {
            return new CSSInput(this);
        }
    }
}
