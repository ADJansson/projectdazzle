﻿using System;
using System.Windows.Forms;

namespace ProjectDazzle
{
    /// <summary>
    /// LockParametersForm.cs - GUI that allows the user to "lock" properties of designs during runtime.
    /// Locked properties are not explored by the GA.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class LockParametersForm : Form
    {
        ElementViewer _parent;

        public LockParametersForm(ElementViewer parent)
        {
            _parent = parent;

            InitializeComponent();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            // BACKGROUND COLOR
            ChromosomeBase.CommonLockList[0] = chkColor.Checked;
            ChromosomeBase.CommonLockList[1] = chkColor.Checked;
            ChromosomeBase.CommonLockList[2] = chkColor.Checked;
            ChromosomeBase.CommonLockList[3] = chkColor.Checked;

            // HOVER COLOR
            ChromosomeBase.CommonLockList[4] = chkColor.Checked;
            ChromosomeBase.CommonLockList[5] = chkColor.Checked;
            ChromosomeBase.CommonLockList[6] = chkColor.Checked;
            ChromosomeBase.CommonLockList[7] = chkColor.Checked;

            // TEXT COLOR
            ChromosomeBase.CommonLockList[8] = chkTextColor.Checked;
            ChromosomeBase.CommonLockList[9] = chkTextColor.Checked;
            ChromosomeBase.CommonLockList[10] = chkTextColor.Checked;
            ChromosomeBase.CommonLockList[11] = chkTextColor.Checked;

            // BORDER COLORS
            ChromosomeBase.CommonLockList[12] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[13] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[14] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[15] = chkBorderColors.Checked;

            ChromosomeBase.CommonLockList[16] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[17] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[18] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[19] = chkBorderColors.Checked;

            ChromosomeBase.CommonLockList[20] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[21] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[22] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[23] = chkBorderColors.Checked;

            ChromosomeBase.CommonLockList[24] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[25] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[26] = chkBorderColors.Checked;
            ChromosomeBase.CommonLockList[27] = chkBorderColors.Checked;

            ChromosomeBase.CommonLockList[28] = chkBorderColors.Checked;

            // BORDERS STYLES
            ChromosomeBase.CommonLockList[29] = chkBorderStyles.Checked;
            ChromosomeBase.CommonLockList[30] = chkBorderStyles.Checked;
            ChromosomeBase.CommonLockList[31] = chkBorderStyles.Checked;
            ChromosomeBase.CommonLockList[32] = chkBorderStyles.Checked;

            ChromosomeBase.CommonLockList[33] = chkBorderStyles.Checked;

            // BORDER WIDTHS
            ChromosomeBase.CommonLockList[34] = chkBorderWidths.Checked;
            ChromosomeBase.CommonLockList[35] = chkBorderWidths.Checked;
            ChromosomeBase.CommonLockList[36] = chkBorderWidths.Checked;
            ChromosomeBase.CommonLockList[37] = chkBorderWidths.Checked;

            ChromosomeBase.CommonLockList[38] = chkBorderWidths.Checked;

            // BORDER RADII / "SHAPE"
            ChromosomeBase.CommonLockList[39] = chkShape.Checked;
            ChromosomeBase.CommonLockList[40] = chkShape.Checked;
            ChromosomeBase.CommonLockList[41] = chkShape.Checked;
            ChromosomeBase.CommonLockList[42] = chkShape.Checked;

            ChromosomeBase.CommonLockList[43] = chkShape.Checked;

            // FONT SIZE
            ChromosomeBase.CommonLockList[48] = chkTextSize.Checked;

            // PADDINGS
            ChromosomeBase.CommonLockList[49] = chkPaddings.Checked;
            ChromosomeBase.CommonLockList[50] = chkPaddings.Checked;
            ChromosomeBase.CommonLockList[51] = chkPaddings.Checked;
            ChromosomeBase.CommonLockList[52] = chkPaddings.Checked;

            ChromosomeBase.CommonLockList[53] = chkPaddings.Checked;

            // USE SHADE HOVER COLOR
            ChromosomeBase.CommonLockList[54] = chkHoverColor.Checked;

            // TRANSPARENT BACKGROUND
            ChromosomeBase.CommonLockList[55] = chkColor.Checked;

            // GRADIENT BACKGROUND
            ChromosomeBase.CommonLockList[56] = chkColor.Checked;

            ChromosomeBase.CommonLockList[57] = chkColor.Checked;
            ChromosomeBase.CommonLockList[58] = chkColor.Checked;
            ChromosomeBase.CommonLockList[59] = chkColor.Checked;
            ChromosomeBase.CommonLockList[60] = chkColor.Checked;

            // Update weights based on locked properties
            ChromosomeBase.UpdateStaticWeights(_parent.GetFittest());
        }
    }
}
