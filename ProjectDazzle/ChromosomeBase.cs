﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace ProjectDazzle
{
    // Various implemented crossover types
    public enum ECrossoverType
    {
        SinglePoint,
        MultiPoint,
        Arithmetic,
        PropertyLock,
    }

    // Classification type
    public enum EVote
    {
        Unrated,
        Liked,
        Disliked,
    }

    /// <summary>
    /// ChromosomeBase.cs - Designs features are encoded and held in this class.
    /// Contains methods for self mutation, crossover, delta and weight management.
    /// There are also several helper methods, including a binary string getter
    /// for testing. The class is sortable in collections using the IComparable
    /// interface, which sorts based on fitness.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public abstract class ChromosomeBase : IComparable<ChromosomeBase>
    {
        protected Random _random;
        protected GeneList _commonGenes;
        protected GeneList _uniqueGenes;
        protected double _mutationChance = 0.05;

        // Binary string lengths for the max value it can hold
        // 111 = 7, 11111 = 31 etc
        // to ensure all genes have the correct length for mutation and after arithmetic
        // crossover. If a gene is 'cut', information will be lost. A gene should be like
        // 000000101, and therefore have a chance to mutate to 100000101. If the gene
        // is only 101, it will be 'stuck' in this range, and its max value will never
        // exceed 111 = 7. Similarly, extra care has to be taken for genes with max numerical
        // values less than its nearest binary maximum. For instance, a max value of 26,
        // which is 11010, could be mutated to 11111 = 31, and break stuff in CSS if 
        // it is sent further down the system. One solution to this is to use switch
        // statements, with one case for each legal value, and make the default return a safe value in
        // case of overflow. It is also possible to exploit this, if one wants a higher chance of
        // a default value being picked. It is simply a matter of extending the random number generation
        // range to overflow on purpose, and let the switch deal with the consequences.

        // Static / common properties
        public static int TL255 { get { return 8; } }
        public static int TL127 { get { return 7; } }
        public static int TL63 { get { return 6; } }
        public static int TL31 { get { return 5; } }
        public static int TL15 { get { return 4; } }
        public static int TL7 { get { return 3; } }
        public static int TL3 { get { return 2; } }
        public static int TLBool { get { return 1; } }

        public static List<bool> CommonLockList = new List<bool>();
        public static List<bool> UniqueLockList = new List<bool>();
        public static List<double> CommonWeights = new List<double>(); // Worth the weight 
        protected List<double> _uniqueWeights; // Weights for RWGA implementation
        public static ChromosomeBase Target;

        // Which element is represented
        public virtual EDesignElement Element
        {
            get; protected set;
        }

        public EVote Vote
        {
            get; set;
        } = EVote.Unrated;

        // Holds the percentage match of K-NN classification for later use
        public double MatchPercent
        {
            get; set;
        } = 0;

        public virtual Color BGColor
        {
            get
            {
                return Color.FromArgb(
                    _commonGenes.GetValue(0),
                    _commonGenes.GetValue(1),
                    _commonGenes.GetValue(2),
                    _commonGenes.GetValue(3));
            }
        }

        public virtual Color HoverColor
        {
            get
            {
                return UseShadeHover ? ColorShader.MakeShadeTint(BGColor) :
                    Color.FromArgb(
                    _commonGenes.GetValue(4),
                    _commonGenes.GetValue(5),
                    _commonGenes.GetValue(6),
                    _commonGenes.GetValue(7));
            }
        }

        public virtual Color HoverColorRaw
        {
            get
            {
                return Color.FromArgb(
                    _commonGenes.GetValue(4),
                    _commonGenes.GetValue(5),
                    _commonGenes.GetValue(6),
                    _commonGenes.GetValue(7));
            }
        }

        public virtual Color ShadowColor
        {
            get
            {
                return Color.FromArgb(
                    _commonGenes.GetValue(8),
                    _commonGenes.GetValue(9),
                    _commonGenes.GetValue(10),
                    _commonGenes.GetValue(11));
            }
        }

        public Color[] BorderColors
        {
            get
            {
                return new[] {
                    Color.FromArgb(
                        _commonGenes.GetValue(12),
                        _commonGenes.GetValue(13),
                        _commonGenes.GetValue(14),
                        _commonGenes.GetValue(15)),
                    Color.FromArgb(
                        _commonGenes.GetValue(16),
                        _commonGenes.GetValue(17),
                        _commonGenes.GetValue(18),
                        _commonGenes.GetValue(19)),
                    Color.FromArgb(
                        _commonGenes.GetValue(20),
                        _commonGenes.GetValue(21),
                        _commonGenes.GetValue(22),
                        _commonGenes.GetValue(23)),
                    Color.FromArgb(
                        _commonGenes.GetValue(24),
                        _commonGenes.GetValue(25),
                        _commonGenes.GetValue(26),
                        _commonGenes.GetValue(27))};
            }
        }

        // 1, 2, 3 or 4 unique border colors to be used
        // Testing shows that if all 4 properties are always different, most designs will look bad.
        // Therefore, a chance to keep the same design for two or more borders has been introduced.
        // This has been discussed in the report.
        public int NoUniqueBorderColors
        {
            get
            {
                return _commonGenes.GetValue(28);
            }
        }

        // 0 - dotted, 1 - dashed, 2 - solid, 3 - double, 4 - groove, 5 - ridge,
        // 6 - inset, 7 - outset, 8 - none, 9 - hidden
        public int[] BorderStyles
        {
            get
            {
                return new[] {
                    _commonGenes.GetValue(29),
                    _commonGenes.GetValue(30),
                    _commonGenes.GetValue(31),
                    _commonGenes.GetValue(32)};
            }
        }

        // 1, 2, 3 or 4 unique border styles to be used
        public int NoUniqueBorderStyles
        {
            get
            {
                return _commonGenes.GetValue(33);
            }
        }

        public int[] BorderWidths
        {
            get
            {
                return new[] {
                    _commonGenes.GetValue(34),
                    _commonGenes.GetValue(35),
                    _commonGenes.GetValue(36),
                    _commonGenes.GetValue(37)};
            }
        }

        // 1, 2, 3 or 4 unique border widths to be used
        public int NoUniqueBorderWidths
        {
            get
            {
                return _commonGenes.GetValue(38);
            }
        }

        public int[] BorderRadii
        {
            get
            {
                // Using Math.Max, negative values are turned to 0.
                // This has been discussed in the report.
                return new[] {
                    Math.Max(0, _commonGenes.GetValue(39) - 128),
                    Math.Max(0, _commonGenes.GetValue(40) - 128),
                    Math.Max(0, _commonGenes.GetValue(41) - 128),
                    Math.Max(0, _commonGenes.GetValue(42) - 128)};
            }
        }

        // Returns the "raw" value stored for special cases
        public int[] BorderRadiiRaw
        {
            get
            {
                return new[] {
                    _commonGenes.GetValue(39),
                    _commonGenes.GetValue(40),
                    _commonGenes.GetValue(41),
                    _commonGenes.GetValue(42)};
            }
        }

        // 1, 2, 3 or 4 unique border radii to be used
        public int NoUniqueBorderRadii
        {
            get
            {
                return _commonGenes.GetValue(43);
            }
        }

        public virtual int ShadowHPos
        {
            get
            {
                return _commonGenes.GetValue(44);
            }
        }

        public virtual int ShadowVPos
        {
            get
            {
                return _commonGenes.GetValue(45);
            }
        }

        public virtual int ShadowBlur
        {
            get
            {
                return _commonGenes.GetValue(46);
            }
        }
        public virtual int FontFamilyID
        {
            get
            {
                return _commonGenes.GetValue(47);
            }
        }

        public virtual int FontSize
        {
            get
            {
                // Font size below 8 is pointless
                return Math.Max(8, _commonGenes.GetValue(48));
            }
        }

        // Returns the "raw" font size for special cases
        public virtual int FontSizeRaw
        {
            get
            {
                return _commonGenes.GetValue(48);
            }
        }

        public int[] Paddings
        {
            get
            {
                return new[] {
                    _commonGenes.GetValue(49),
                    _commonGenes.GetValue(50),
                    _commonGenes.GetValue(51),
                    _commonGenes.GetValue(52)};
            }
        }

        // 1, 2, 3 or 4 unique paddings to be used
        public int NoUniquePaddings
        {
            get
            {
                return _commonGenes.GetValue(53);
            }
        }

        public bool UseShadeHover
        {
            get
            {
                return _commonGenes.GetValue(54) == 1;
            }
        }

        public bool TransparentBG
        {
            get
            {
                return _commonGenes.GetValue(55) == 1;
            }
        }

        public int TransparentBGRaw
        {
            get
            {
                return _commonGenes.GetValue(55);
            }
        }

        public bool UseGradient
        {
            get
            {
                return _commonGenes.GetValue(56) == 1;
            }
        }

        public Color Gradient
        {
            get
            {
                return Color.FromArgb(
                    _commonGenes.GetValue(57),
                    _commonGenes.GetValue(58),
                    _commonGenes.GetValue(59),
                    _commonGenes.GetValue(60));
            }
        }

        public virtual double Fitness
        {
            get; set;
        } = double.MaxValue;

        public virtual double MutationChance
        {
            get
            {
                return _mutationChance;
            }
        }

        /// <summary>
        /// Number of genes representing the chromosome
        /// </summary>
        public int Length
        {
            get
            {
                return _commonGenes.Count + _uniqueGenes.Count;
            }
        }

        public ChromosomeBase()
        {

        }

        /// <summary>
        /// Creates a chromosome based on a seed
        /// </summary>
        /// <param name="element"></param>
        /// <param name="seed">Given seed</param>
        public ChromosomeBase(EDesignElement element, int seed)
        {
            Init(element, seed);
        }

        /// <summary>
        /// Creates a random chromosome
        /// </summary>
        /// <param name="element"></param>
        public ChromosomeBase(EDesignElement element)
        {
            // The Guid class generates a random number to use as random seed, 
            // and gives a better random distribution
            Init(element, Guid.NewGuid().GetHashCode());
        }

        /// <summary>
        /// Helper method to initialize the chromosome
        /// </summary>
        /// <param name="element"></param>
        /// <param name="seed"></param>
        private void Init(EDesignElement element, int seed)
        {
            Element = element;

            _random = new Random(seed); // Seeded random for consistent testing

            _commonGenes = new GeneList();
            _uniqueGenes = new GeneList();

            // Colors
            for (int i = 0; i < 28; i++)
                _commonGenes.AddValue(_random.Next(0, 256), TL255);

            _commonGenes.AddValue(_random.Next(0, 4), TL3); // No of unique colors

            // Border styles
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(_random.Next(0, 10), TL15, 9);

            _commonGenes.AddValue(_random.Next(0, 4), TL3); // No of unique border styles

            // Border widths
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(_random.Next(0, 16), TL15);

            _commonGenes.AddValue(_random.Next(0, 4), TL3); // No of unique border widths

            // Border radii
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(_random.Next(0, 256), TL255);

            _commonGenes.AddValue(_random.Next(0, 4), TL3); // No of unique border radii

            // Shadow H, V, blur
            for (int i = 0; i < 3; i++)
                _commonGenes.AddValue(_random.Next(0, 16), TL15);

            _commonGenes.AddValue(_random.Next(0, 32), TL31); // Font family ID

            _commonGenes.AddValue(_random.Next(8, 32), TL31); // Font size

            // Paddings
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(_random.Next(0, 32), TL31);

            _commonGenes.AddValue(_random.Next(0, 4), TL3); // No of unique paddings

            _commonGenes.AddBool(_random.Next(0, 2)); // Use bgcolor shade/tint as hover color (yes/no)

            _commonGenes.AddValue(_random.Next(0, 8), TL7); // 1/8 chance of transparent background

            _commonGenes.AddBool(_random.Next(0, 2)); // Use gradient (yes/no)

            _commonGenes.AddColor(Color.FromArgb(_random.Next(0, 256), _random.Next(0, 256), _random.Next(0, 256), _random.Next(0, 256))); // Gradient
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="copy"></param>
        public ChromosomeBase(ChromosomeBase copy)
        {
            _random = copy._random;
            _mutationChance = copy._mutationChance;
            _commonGenes = new GeneList(copy._commonGenes);
            _uniqueGenes = new GeneList(copy._uniqueGenes);
            _uniqueWeights = copy._uniqueWeights.ToList();

            Vote = copy.Vote;
            Element = copy.Element;
            Fitness = copy.Fitness;
        }

        /// <summary>
        /// Constructor to generate a chromosome based on parsed CSS data.
        /// Values are retrieved from the ParseBundle data structure.
        /// </summary>
        /// <param name="bundle"></param>
        public ChromosomeBase(ParseBundle bundle)
        {
            _commonGenes = new GeneList();
            _uniqueGenes = new GeneList();

            _random = new Random(Guid.NewGuid().GetHashCode());

            // Colors
            _commonGenes.AddColor(bundle.BGColor);
            _commonGenes.AddColor(bundle.HoverColor);
            _commonGenes.AddColor(bundle.ShadowColor);

            for (int i = 0; i < 4; i++)
                _commonGenes.AddColor(i < bundle.BorderColors.Length ? bundle.BorderColors[i] : Color.Empty);

            _commonGenes.AddValue(bundle.NoUniqueBorderColors, TL3); // No of unique colors

            // Border styles
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(i < bundle.BorderStyles.Length ? bundle.BorderStyles[i] : 0, TL15, 9); // Special maxValue

            _commonGenes.AddValue(bundle.NoUniqueBorderStyles, TL3); // No of unique border styles

            // Border widths
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(i < bundle.BorderWidths.Length ? bundle.BorderWidths[i] : 0, TL15);

            _commonGenes.AddValue(bundle.NoUniqueBorderWidths, TL3); // No of unique border widths

            // Border radii
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(i < bundle.BorderRadii.Length ? bundle.BorderRadii[i] : 0, TL255);

            _commonGenes.AddValue(bundle.NoUniqueBorderRadii, TL3); // No of unique border radii

            // Shadow H, V, blur

            _commonGenes.AddValue(bundle.ShadowHPos, TL15);
            _commonGenes.AddValue(bundle.ShadowVPos, TL15);
            _commonGenes.AddValue(bundle.ShadowBlur, TL15);

            _commonGenes.AddValue(bundle.FontFamilyID, TL31); // Font family ID

            _commonGenes.AddValue(bundle.FontSize, TL31); // Font size

            // Paddings
            for (int i = 0; i < 4; i++)
                _commonGenes.AddValue(i < bundle.Paddings.Length ? bundle.Paddings[i] : 0, TL31);

            _commonGenes.AddValue(bundle.NoUniquePaddings, TL3); // No of unique paddings

            _commonGenes.AddBool(bundle.UseShadeHover); // Use bgcolor shade/tint as hover color (yes/no)

            _commonGenes.AddValue(bundle.TransparentBG, TL7);

            _commonGenes.AddBool(bundle.UseGradient);

            _commonGenes.AddColor(bundle.Gradient);

            SetEqualWeights();
        }

        /// <summary>
        /// Similar to the above constructor, gets data from initial conditions provided
        /// by user
        /// </summary>
        /// <param name="bundle"></param>
        public ChromosomeBase(InitialBundle bundle)
        {
            if (bundle.MinValues.Count != bundle.MaxValues.Count)
                return;

            _random = new Random(Guid.NewGuid().GetHashCode());
            _commonGenes = new GeneList();
            _uniqueGenes = new GeneList();

            for (int i = 0; i < bundle.MinValues.Count; i++)
            {
                _commonGenes.AddValue(RandomHelper(bundle.MinValues[i], bundle.MaxValues[i]), bundle.Lengths[i], bundle.MaxValuesValue[i]);
            }

            SetEqualWeights();

            // Small helper to handle random range from user input,
            // as the user may enter a larger min value than max value.
            int RandomHelper(int min, int max)
            {
                if (min == max)
                    return min;

                return _random.Next(Math.Min(min, max), Math.Max(min, max));
            }
        }

        protected void MakeWeights()
        {
            // Using data from static class to generate different weight types
            if (RWGAHelper.UseRWGA)
                GenerateUniqueWeights();
            else
                SetEqualWeights();
        }

        private void GenerateUniqueWeights()
        {
            CommonWeights.Clear();
            _uniqueWeights = new List<double>();

            // Generate random weights for common
            for (int i = 0; i < _commonGenes.Count; i++)
                CommonWeights.Add(_random.Next(0, 2));

            // Generate random weights for unique
            for (int i = 0; i < _uniqueGenes.Count; i++)
                _uniqueWeights.Add(_random.Next(0, 2));
        }

        // Sets all weights to 1
        private void SetEqualWeights()
        {
            CommonWeights.Clear();
            _uniqueWeights = new List<double>();

            for (int i = 0; i < _uniqueGenes.Count; i++)
            {
                _uniqueWeights.Add(1);
                UniqueLockList.Add(false);
            }

            for (int i = 0; i < _commonGenes.Count; i++)
            {
                CommonWeights.Add(1);
                CommonLockList.Add(false);
            }
        }

        /// <summary>
        /// Weightless fitness operator
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static double operator -(ChromosomeBase left, ChromosomeBase right)
        {
            // Shouldn't happen, but one never knows in these days
            if (left.Length != right.Length)
                return double.MaxValue;

            return Delta(left._commonGenes, right._commonGenes)
                + Delta(left._uniqueGenes, right._uniqueGenes);
        }

        /// <summary>
        /// Weighted fitness operator
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static double operator /(ChromosomeBase left, ChromosomeBase right)
        {
            // Shouldn't happen, but one never knows in these days
            if (left.Length != right.Length)
                return double.MaxValue;

            return WeightedDelta(left._commonGenes, right._commonGenes, CommonWeights)
                + WeightedDelta(left._uniqueGenes, right._uniqueGenes, left._uniqueWeights);
        }

        private static double Delta(GeneList left, GeneList right)
        {
            List<double> deltas = new List<double>();

            // Assumes that both chromosomes are the same length
            for (int i = 0; i < left.Count; i++)
                deltas.Add(Math.Pow((Math.Abs(left.GetValue(i) - right.GetValue(i)) / (double)left.GetMaxValue(i)), 2)); // Divide by maxValue to normalize

            return deltas.Sum();
        }

        private static double WeightedDelta(GeneList left, GeneList right, List<double> weights)
        {
            List<double> deltas = new List<double>();

            for (int i = 0; i < left.Count; i++)
                deltas.Add(Math.Pow((Math.Abs(weights[i] * left.GetValue(i) - weights[i] * right.GetValue(i)) / left.GetMaxValue(i)), 2));

            return deltas.Sum();
        }

        public void Crossover(ChromosomeBase mate, ECrossoverType cType = ECrossoverType.SinglePoint)
        {
            switch (cType)
            {
                case ECrossoverType.SinglePoint:
                    SinglePointCrossover(mate);
                    break;
                case ECrossoverType.MultiPoint:
                    MultiPointCrossover(mate);
                    break;
                case ECrossoverType.Arithmetic:
                    ArithmeticCrossover(mate);
                    break;
                case ECrossoverType.PropertyLock:
                    PropLockCrossover(mate);
                    break;
            }

            Mutate();
        }

        public void SinglePointCrossover(ChromosomeBase mate)
        {
            SinglePointHelper(_commonGenes, mate._commonGenes);
            SinglePointHelper(_uniqueGenes, mate._uniqueGenes);

            void SinglePointHelper(GeneList myGeneList, GeneList mateGeneList)
            {
                if (myGeneList.Count != mateGeneList.Count)
                    return;

                int x = (int)(2.0 / 5.0 * myGeneList.Count); // x is offset helper to make the random range ~30% larger than the list of genes
                int k = _random.Next(0, myGeneList.Count + x); // ~70% chance for crossover due to offset x

                // k is the index of the crossover point, all genes after this are copied
                for (int i = k; i < myGeneList.Count; i++) // If k is bigger than Count, no crossover will happen ^
                    myGeneList[i] = mateGeneList[i];
            }
        }

        public void MultiPointCrossover(ChromosomeBase mate)
        {
            MultiPointHelper(_commonGenes, mate._commonGenes, CommonLockList);
            MultiPointHelper(_uniqueGenes, mate._uniqueGenes, UniqueLockList);

            void MultiPointHelper(GeneList myGeneList, GeneList mateGeneList, List<bool> locks)
            {
                if (myGeneList.Count != mateGeneList.Count)
                    return;

                int n = myGeneList.Count; // Number of genes to copy

                // Must have at least 2 genes
                if (n <= 1)
                    return;

                int[] d = new int[n];

                // Fisher-Yates modern shuffle algorithm
                // http://benpfaff.org/writings/clc/shuffle.html 21/02-17

                for (int i = 0; i < n; i++)
                    d[i] = i;

                // Shuffle the indices
                for (int i = 1; i <= n; ++i)
                    Swap(n - i, _random.Next(0, n - i), ref d);

                int k = _random.Next(1, n);

                // Copy random genes to mate
                for (int i = 0; i < k; i++)
                    myGeneList[d[i]] = mateGeneList[d[i]];
            }
        }

        // Helper function to swap elements in array
        public void Swap(int i, int j, ref int[] arr)
        {
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        }

        public void ArithmeticCrossover(ChromosomeBase mate)
        {
            for (int i = 0; i < _commonGenes.Count; i++)
                _commonGenes.SetGene(i, AverageGenes(_commonGenes[i], mate._commonGenes[i]));

            for (int i = 0; i < _uniqueGenes.Count; i++)
                _uniqueGenes.SetGene(i, AverageGenes(_uniqueGenes[i], mate._uniqueGenes[i]));

            Gene AverageGenes(Gene gene1, Gene gene2)
            {
                return new Gene
                {
                    Binary = Convert.ToString((GeneList.GetValue(gene1) + GeneList.GetValue(gene2)) / 2, 2),
                    Length = gene1.Length
                };
            }
        }

        public void PropLockCrossover(ChromosomeBase mate)
        {
            SetMutationChance(1); // Increase mutation chance to get more diverse population

            for (int i = 0; i < _commonGenes.Count; i++)
            {
                if (this.ToString() == Target.ToString())
                {
                    // We are inside target
                    if (!CommonLockList[i])
                        _commonGenes[i] = mate._commonGenes[i];
                }
                else
                {
                    // We are inside another chromosome
                    if (CommonLockList[i] && mate.ToString() == Target.ToString())
                        _commonGenes[i] = mate._commonGenes[i];
                }
            }
        }

        public void Mutate()
        {
            for (int i = 0; i < _commonGenes.Count; i++)
                if (!CommonLockList[i]) // Only mutate unlocked genes
                    _commonGenes[i] = MutateGene(_commonGenes[i]);

            for (int i = 0; i < _uniqueGenes.Count; i++)
                _uniqueGenes[i] = MutateGene(_uniqueGenes[i]);

            Gene MutateGene(Gene gene)
            {
                char[] geneChar = gene.Binary.ToCharArray();

                // Iterate over bits
                for (int i = 0; i < geneChar.Count(); i++)
                {
                    double w = _random.NextDouble();
                    if (w < _mutationChance)
                        geneChar[i] = geneChar[i] == '1' ? '0' : '1'; // Flip bits
                }

                gene.Binary = new string(geneChar);
                return gene;
            }
        }

        /// <summary>
        /// Chance is between 0 and 1
        /// </summary>
        /// <param name="chance"></param>
        public void SetMutationChance(double chance)
        {
            if (chance > 1)
                _mutationChance = 1;
            else if (chance < 0)
                _mutationChance = 0;
            else
                _mutationChance = chance;
        }

        public int CompareTo(ChromosomeBase other)
        {
            return Fitness.CompareTo(other.Fitness);
        }

        /// <summary>
        /// Initiates static lists
        /// </summary>
        public static void InitStatic(ChromosomeBase b)
        {
            Target = b;

            // I see you're hard-coding list sizes. I too like to live dangerously..
            for (int i = 0; i < 61; i++)
            {
                CommonWeights.Add(1);
                CommonLockList.Add(false);
            }
        }

        /// <summary>
        /// Updates weights based on locks. Locked properties
        /// get weight = 100, which will affect the fitness function.
        /// Unlocked properties get weight = 0, meaning they will be ignored
        /// in the fitness test, which will produce a more diverse population
        /// </summary>
        public static void UpdateStaticWeights(ChromosomeBase b)
        {
            Target = b;

            for (int i = 0; i < 61; i++)
            {
                CommonWeights[i] = CommonLockList[i] ? 1 : 0;
            }
        }

        /// <summary>
        /// Prints the binary representation of the chromosome
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _commonGenes.ToString() + _uniqueGenes.ToString();
        }

        public abstract ChromosomeBase Clone();
    }
}
