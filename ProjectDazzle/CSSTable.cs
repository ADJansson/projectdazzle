﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSTable.cs - Represents a CSS table design. Extends the base chromosome with multiple properties
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class CSSTable : ChromosomeBase
    {
        #region common overrides
        // TD = table data, TH = table header
        public int BorderStyle
        {
            get
            {
                return BorderStyles[0];
            }
        }

        public int BorderRadius
        {
            get
            {
                return BorderRadii[0];
            }
        }

        public int BorderWith
        {
            get
            {
                return BorderWidths[0];
            }
        }

        public Color BorderColor
        {
            get
            {
                return BorderColors[0];
            }
        }

        public int TDBorderStyle
        {
            get
            {
                return BorderStyles[1];
            }
        }

        public int TDBorderRadius
        {
            get
            {
                return BorderRadii[1];
            }
        }

        public int TDBorderWith
        {
            get
            {
                return BorderWidths[1];
            }
        }

        public Color TDBorderColor
        {
            get
            {
                return BorderColors[1];
            }
        }

        public int TDPadding
        {
            get
            {
                return Paddings[1];
            }
        }

        public int THBorderStyle
        {
            get
            {
                return BorderStyles[2];
            }
        }

        public int THBorderRadius
        {
            get
            {
                return BorderRadii[2];
            }
        }

        public int THBorderWith
        {
            get
            {
                return BorderWidths[2];
            }
        }

        public Color THBorderColor
        {
            get
            {
                return BorderColors[2];
            }
        }

        public int THPadding
        {
            get
            {
                return Paddings[2];
            }
        }

        public Color THBGColor
        {
            get
            {
                return BorderColors[3];
            }
        }

        #endregion

        // 0 = none, 1 = collapse
        public bool BorderCollapse
        {
            get
            {
                return _uniqueGenes.GetValue(0) == 1;
            }
        }

        public int TDBorderPos
        {
            get
            {
                return _uniqueGenes.GetValue(1);
            }
        }

        public int THBorderPos
        {
            get
            {
                return _uniqueGenes.GetValue(2);
            }
        }

        public int TDTextAlign
        {
            get
            {
                return _uniqueGenes.GetValue(3);
            }
        }

        public int THTextAlign
        {
            get
            {
                return _uniqueGenes.GetValue(4);
            }
        }

        public Color TDTextColor
        {
            get
            {
                return Color.FromArgb(
                    _uniqueGenes.GetValue(5),
                    _uniqueGenes.GetValue(6),
                    _uniqueGenes.GetValue(7));
            }
        }

        public Color THTextColor
        {
            get
            {
                return Color.FromArgb(
                    _uniqueGenes.GetValue(8),
                    _uniqueGenes.GetValue(9),
                    _uniqueGenes.GetValue(10));
            }
        }

        public Color StripeBGColor
        {
            get
            {
                return UseShadeStriped ? ColorShader.MakeShadeTint(HoverColor) : 
                    Color.FromArgb(
                    _uniqueGenes.GetValue(11),
                    _uniqueGenes.GetValue(12),
                    _uniqueGenes.GetValue(13));
            }
        }

        public bool StripedTable
        {
            get
            {
                return _uniqueGenes.GetValue(14) == 1;
            }
        }

        public int THFontSize
        {
            get
            {
                // Header text should be equal or larger than td text
                return Math.Max(FontSize, _uniqueGenes.GetValue(15));
            }
        }

        public bool UseShadeStriped
        {
            get
            {
                return _uniqueGenes.GetValue(16) == 1;
            }
        }

        public CSSTable() : base(EDesignElement.Table)
        {
            Init();
        }

        public CSSTable(int seed) : base(EDesignElement.Table, seed)
        {
            Init();
        }

        private void Init()
        {
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // border collapse
            _uniqueGenes.AddValue(_random.Next(0, 11), TL15, 10); // td border pos
            _uniqueGenes.AddValue(_random.Next(0, 11), TL15, 10); // th border pos
            _uniqueGenes.AddValue(_random.Next(0, 3), TL3, 2); // td h align
            _uniqueGenes.AddValue(_random.Next(0, 3), TL3, 2); // th h align

            for (int i = 0; i < 9; i++)
                _uniqueGenes.AddValue(_random.Next(0, 255), TL255); //text color

            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // striped table
            _uniqueGenes.AddValue(_random.Next(8, 32), TL31); // th font size
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // Use bgcolor shade/tint as stripe color (yes/no)

            MakeWeights();
        }

        public CSSTable(CSSTable copy) : base(copy)
        {

        }

        public override ChromosomeBase Clone()
        {
            return new CSSTable(this);
        }
    }
}
