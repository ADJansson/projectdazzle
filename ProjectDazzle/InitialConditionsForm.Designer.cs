﻿namespace ProjectDazzle
{
    partial class InitialConditionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.nudFontSzMax = new System.Windows.Forms.NumericUpDown();
            this.nudFontSzMin = new System.Windows.Forms.NumericUpDown();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.cmbFontFamMax = new System.Windows.Forms.ComboBox();
            this.label107 = new System.Windows.Forms.Label();
            this.cmbFontFamMin = new System.Windows.Forms.ComboBox();
            this.label108 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.nudPad4Max = new System.Windows.Forms.NumericUpDown();
            this.nudPad3Max = new System.Windows.Forms.NumericUpDown();
            this.nudPad2Max = new System.Windows.Forms.NumericUpDown();
            this.nudPad1Max = new System.Windows.Forms.NumericUpDown();
            this.nudPad4Min = new System.Windows.Forms.NumericUpDown();
            this.nudPad3Min = new System.Windows.Forms.NumericUpDown();
            this.nudPad2Min = new System.Windows.Forms.NumericUpDown();
            this.nudPad1Min = new System.Windows.Forms.NumericUpDown();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.nudUniqPadd = new System.Windows.Forms.NumericUpDown();
            this.label106 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.nudShadBlurMax = new System.Windows.Forms.NumericUpDown();
            this.nudShadVMax = new System.Windows.Forms.NumericUpDown();
            this.nudShadHMax = new System.Windows.Forms.NumericUpDown();
            this.nudShadBlurMin = new System.Windows.Forms.NumericUpDown();
            this.nudShadVMin = new System.Windows.Forms.NumericUpDown();
            this.nudShadHMin = new System.Windows.Forms.NumericUpDown();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.nudBr4Max = new System.Windows.Forms.NumericUpDown();
            this.nudBr3Max = new System.Windows.Forms.NumericUpDown();
            this.nudBr2Max = new System.Windows.Forms.NumericUpDown();
            this.nudBr1Max = new System.Windows.Forms.NumericUpDown();
            this.nudBr4Min = new System.Windows.Forms.NumericUpDown();
            this.nudBr3Min = new System.Windows.Forms.NumericUpDown();
            this.nudBr2Min = new System.Windows.Forms.NumericUpDown();
            this.nudBr1Min = new System.Windows.Forms.NumericUpDown();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.nudUniqBRad = new System.Windows.Forms.NumericUpDown();
            this.label91 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.nudBW4Max = new System.Windows.Forms.NumericUpDown();
            this.nudBW3Max = new System.Windows.Forms.NumericUpDown();
            this.nudBW2Max = new System.Windows.Forms.NumericUpDown();
            this.nudBW1Max = new System.Windows.Forms.NumericUpDown();
            this.nudBW4Min = new System.Windows.Forms.NumericUpDown();
            this.nudBW3Min = new System.Windows.Forms.NumericUpDown();
            this.nudBW2Min = new System.Windows.Forms.NumericUpDown();
            this.nudBW1Min = new System.Windows.Forms.NumericUpDown();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.nudUniqBWidths = new System.Windows.Forms.NumericUpDown();
            this.label82 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cmbBStyle4Max = new System.Windows.Forms.ComboBox();
            this.label72 = new System.Windows.Forms.Label();
            this.cmbBStyle4Min = new System.Windows.Forms.ComboBox();
            this.label73 = new System.Windows.Forms.Label();
            this.cmbBStyle3Max = new System.Windows.Forms.ComboBox();
            this.label70 = new System.Windows.Forms.Label();
            this.cmbBStyle3Min = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.cmbBStyle2Max = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.cmbBStyle2Min = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.cmbBStyle1Max = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this.cmbBStyle1Min = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.nudUniqBStyles = new System.Windows.Forms.NumericUpDown();
            this.label65 = new System.Windows.Forms.Label();
            this.grbColors = new System.Windows.Forms.GroupBox();
            this.chkTransparent = new System.Windows.Forms.CheckBox();
            this.chkGradient = new System.Windows.Forms.CheckBox();
            this.grbGradient = new System.Windows.Forms.GroupBox();
            this.nudGradAlphaMax = new System.Windows.Forms.NumericUpDown();
            this.label144 = new System.Windows.Forms.Label();
            this.nudGradAlphaMin = new System.Windows.Forms.NumericUpDown();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.nudGradBlueMax = new System.Windows.Forms.NumericUpDown();
            this.nudGradGreenMax = new System.Windows.Forms.NumericUpDown();
            this.nudGradRedMax = new System.Windows.Forms.NumericUpDown();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.nudGradBlueMin = new System.Windows.Forms.NumericUpDown();
            this.nudGradGreenMin = new System.Windows.Forms.NumericUpDown();
            this.nudGradRedMin = new System.Windows.Forms.NumericUpDown();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.nudUniqBColors = new System.Windows.Forms.NumericUpDown();
            this.label64 = new System.Windows.Forms.Label();
            this.grbC4 = new System.Windows.Forms.GroupBox();
            this.nudBC4AMax = new System.Windows.Forms.NumericUpDown();
            this.label129 = new System.Windows.Forms.Label();
            this.nudBC4AMin = new System.Windows.Forms.NumericUpDown();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.nudBC4BMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC4GMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC4RMax = new System.Windows.Forms.NumericUpDown();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.nudBC4BMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC4GMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC4RMin = new System.Windows.Forms.NumericUpDown();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.grbC3 = new System.Windows.Forms.GroupBox();
            this.nudBC3AMax = new System.Windows.Forms.NumericUpDown();
            this.label126 = new System.Windows.Forms.Label();
            this.nudBC3AMin = new System.Windows.Forms.NumericUpDown();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.nudBC3BMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC3GMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC3RMax = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.nudBC3BMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC3GMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC3RMin = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label132 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nudShadowAlphaMax = new System.Windows.Forms.NumericUpDown();
            this.label117 = new System.Windows.Forms.Label();
            this.nudShadowAlphaMin = new System.Windows.Forms.NumericUpDown();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.nudShadowBlueMax = new System.Windows.Forms.NumericUpDown();
            this.nudShadowGreenMax = new System.Windows.Forms.NumericUpDown();
            this.nudShadowRedMax = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.nudShadowBlueMin = new System.Windows.Forms.NumericUpDown();
            this.nudShadowGreenMin = new System.Windows.Forms.NumericUpDown();
            this.nudShadowRedMin = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.grbC2 = new System.Windows.Forms.GroupBox();
            this.nudBC2AMax = new System.Windows.Forms.NumericUpDown();
            this.label123 = new System.Windows.Forms.Label();
            this.nudBC2AMin = new System.Windows.Forms.NumericUpDown();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.nudBC2BMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC2GMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC2RMax = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.nudBC2BMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC2GMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC2RMin = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudHoverAlphaMax = new System.Windows.Forms.NumericUpDown();
            this.label114 = new System.Windows.Forms.Label();
            this.nudHoverAlphaMin = new System.Windows.Forms.NumericUpDown();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.nudHoverBlueMax = new System.Windows.Forms.NumericUpDown();
            this.nudHoverGreenMax = new System.Windows.Forms.NumericUpDown();
            this.nudHoverRedMax = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nudHoverBlueMin = new System.Windows.Forms.NumericUpDown();
            this.nudHoverGreenMin = new System.Windows.Forms.NumericUpDown();
            this.nudHoverRedMin = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.grbC1 = new System.Windows.Forms.GroupBox();
            this.nudBC1AMax = new System.Windows.Forms.NumericUpDown();
            this.label120 = new System.Windows.Forms.Label();
            this.nudBC1AMin = new System.Windows.Forms.NumericUpDown();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.nudBC1BMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC1GMax = new System.Windows.Forms.NumericUpDown();
            this.nudBC1RMax = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.nudBC1BMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC1GMin = new System.Windows.Forms.NumericUpDown();
            this.nudBC1RMin = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.grbBGColor = new System.Windows.Forms.GroupBox();
            this.nudBGAlphaMax = new System.Windows.Forms.NumericUpDown();
            this.label111 = new System.Windows.Forms.Label();
            this.nudBGAlphaMin = new System.Windows.Forms.NumericUpDown();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.nudBGBlueMax = new System.Windows.Forms.NumericUpDown();
            this.nudBGGreenMax = new System.Windows.Forms.NumericUpDown();
            this.nudBGRedMax = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nudBGBlueMin = new System.Windows.Forms.NumericUpDown();
            this.nudBGGreenMin = new System.Windows.Forms.NumericUpDown();
            this.nudBGRedMin = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSzMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSzMin)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqPadd)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadBlurMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadVMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadHMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadBlurMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadVMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadHMin)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBRad)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW4Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW3Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW2Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW1Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW4Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW3Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW2Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW1Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBWidths)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBStyles)).BeginInit();
            this.grbColors.SuspendLayout();
            this.grbGradient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradAlphaMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradAlphaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradBlueMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradGreenMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradRedMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradBlueMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradGreenMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradRedMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBColors)).BeginInit();
            this.grbC4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4AMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4AMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4BMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4GMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4RMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4BMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4GMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4RMin)).BeginInit();
            this.grbC3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3AMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3AMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3BMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3GMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3RMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3BMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3GMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3RMin)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowAlphaMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowAlphaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowBlueMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowGreenMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowRedMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowBlueMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowGreenMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowRedMin)).BeginInit();
            this.grbC2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2AMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2AMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2BMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2GMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2RMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2BMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2GMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2RMin)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverAlphaMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverAlphaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverBlueMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverGreenMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverRedMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverBlueMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverGreenMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverRedMin)).BeginInit();
            this.grbC1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1AMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1AMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1BMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1GMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1RMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1BMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1GMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1RMin)).BeginInit();
            this.grbBGColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGAlphaMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGAlphaMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGBlueMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGGreenMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGRedMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGBlueMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGGreenMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGRedMin)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.btnOk);
            this.pnlMain.Controls.Add(this.groupBox13);
            this.pnlMain.Controls.Add(this.groupBox12);
            this.pnlMain.Controls.Add(this.groupBox11);
            this.pnlMain.Controls.Add(this.groupBox7);
            this.pnlMain.Controls.Add(this.grbColors);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1135, 741);
            this.pnlMain.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(1048, 730);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 68;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.nudFontSzMax);
            this.groupBox13.Controls.Add(this.nudFontSzMin);
            this.groupBox13.Controls.Add(this.label109);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.cmbFontFamMax);
            this.groupBox13.Controls.Add(this.label107);
            this.groupBox13.Controls.Add(this.cmbFontFamMin);
            this.groupBox13.Controls.Add(this.label108);
            this.groupBox13.Location = new System.Drawing.Point(561, 570);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(562, 154);
            this.groupBox13.TabIndex = 67;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Text properties";
            // 
            // nudFontSzMax
            // 
            this.nudFontSzMax.Location = new System.Drawing.Point(275, 97);
            this.nudFontSzMax.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudFontSzMax.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudFontSzMax.Name = "nudFontSzMax";
            this.nudFontSzMax.Size = new System.Drawing.Size(59, 20);
            this.nudFontSzMax.TabIndex = 65;
            this.nudFontSzMax.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // nudFontSzMin
            // 
            this.nudFontSzMin.Location = new System.Drawing.Point(110, 100);
            this.nudFontSzMin.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudFontSzMin.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudFontSzMin.Name = "nudFontSzMin";
            this.nudFontSzMin.Size = new System.Drawing.Size(59, 20);
            this.nudFontSzMin.TabIndex = 64;
            this.nudFontSzMin.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(175, 102);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(71, 13);
            this.label109.TabIndex = 63;
            this.label109.Text = "Font size max";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(13, 100);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(68, 13);
            this.label110.TabIndex = 62;
            this.label110.Text = "Font size min";
            // 
            // cmbFontFamMax
            // 
            this.cmbFontFamMax.FormattingEnabled = true;
            this.cmbFontFamMax.Location = new System.Drawing.Point(113, 54);
            this.cmbFontFamMax.Name = "cmbFontFamMax";
            this.cmbFontFamMax.Size = new System.Drawing.Size(443, 21);
            this.cmbFontFamMax.TabIndex = 39;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(13, 54);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(79, 13);
            this.label107.TabIndex = 38;
            this.label107.Text = "Font family max";
            // 
            // cmbFontFamMin
            // 
            this.cmbFontFamMin.FormattingEnabled = true;
            this.cmbFontFamMin.Location = new System.Drawing.Point(113, 24);
            this.cmbFontFamMin.Name = "cmbFontFamMin";
            this.cmbFontFamMin.Size = new System.Drawing.Size(443, 21);
            this.cmbFontFamMin.TabIndex = 37;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(13, 24);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(76, 13);
            this.label108.TabIndex = 36;
            this.label108.Text = "Font family min";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.nudPad4Max);
            this.groupBox12.Controls.Add(this.nudPad3Max);
            this.groupBox12.Controls.Add(this.nudPad2Max);
            this.groupBox12.Controls.Add(this.nudPad1Max);
            this.groupBox12.Controls.Add(this.nudPad4Min);
            this.groupBox12.Controls.Add(this.nudPad3Min);
            this.groupBox12.Controls.Add(this.nudPad2Min);
            this.groupBox12.Controls.Add(this.nudPad1Min);
            this.groupBox12.Controls.Add(this.label98);
            this.groupBox12.Controls.Add(this.label99);
            this.groupBox12.Controls.Add(this.label100);
            this.groupBox12.Controls.Add(this.label101);
            this.groupBox12.Controls.Add(this.label102);
            this.groupBox12.Controls.Add(this.label103);
            this.groupBox12.Controls.Add(this.label104);
            this.groupBox12.Controls.Add(this.label105);
            this.groupBox12.Controls.Add(this.nudUniqPadd);
            this.groupBox12.Controls.Add(this.label106);
            this.groupBox12.Location = new System.Drawing.Point(12, 595);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(542, 154);
            this.groupBox12.TabIndex = 66;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Text padding";
            // 
            // nudPad4Max
            // 
            this.nudPad4Max.Enabled = false;
            this.nudPad4Max.Location = new System.Drawing.Point(268, 122);
            this.nudPad4Max.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad4Max.Name = "nudPad4Max";
            this.nudPad4Max.Size = new System.Drawing.Size(59, 20);
            this.nudPad4Max.TabIndex = 64;
            this.nudPad4Max.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // nudPad3Max
            // 
            this.nudPad3Max.Enabled = false;
            this.nudPad3Max.Location = new System.Drawing.Point(268, 98);
            this.nudPad3Max.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad3Max.Name = "nudPad3Max";
            this.nudPad3Max.Size = new System.Drawing.Size(59, 20);
            this.nudPad3Max.TabIndex = 63;
            this.nudPad3Max.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // nudPad2Max
            // 
            this.nudPad2Max.Enabled = false;
            this.nudPad2Max.Location = new System.Drawing.Point(268, 70);
            this.nudPad2Max.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad2Max.Name = "nudPad2Max";
            this.nudPad2Max.Size = new System.Drawing.Size(59, 20);
            this.nudPad2Max.TabIndex = 62;
            this.nudPad2Max.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // nudPad1Max
            // 
            this.nudPad1Max.Location = new System.Drawing.Point(268, 44);
            this.nudPad1Max.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad1Max.Name = "nudPad1Max";
            this.nudPad1Max.Size = new System.Drawing.Size(59, 20);
            this.nudPad1Max.TabIndex = 61;
            this.nudPad1Max.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // nudPad4Min
            // 
            this.nudPad4Min.Enabled = false;
            this.nudPad4Min.Location = new System.Drawing.Point(103, 121);
            this.nudPad4Min.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad4Min.Name = "nudPad4Min";
            this.nudPad4Min.Size = new System.Drawing.Size(59, 20);
            this.nudPad4Min.TabIndex = 60;
            // 
            // nudPad3Min
            // 
            this.nudPad3Min.Enabled = false;
            this.nudPad3Min.Location = new System.Drawing.Point(103, 95);
            this.nudPad3Min.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad3Min.Name = "nudPad3Min";
            this.nudPad3Min.Size = new System.Drawing.Size(59, 20);
            this.nudPad3Min.TabIndex = 59;
            // 
            // nudPad2Min
            // 
            this.nudPad2Min.Enabled = false;
            this.nudPad2Min.Location = new System.Drawing.Point(103, 70);
            this.nudPad2Min.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad2Min.Name = "nudPad2Min";
            this.nudPad2Min.Size = new System.Drawing.Size(59, 20);
            this.nudPad2Min.TabIndex = 58;
            // 
            // nudPad1Min
            // 
            this.nudPad1Min.Location = new System.Drawing.Point(103, 47);
            this.nudPad1Min.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudPad1Min.Name = "nudPad1Min";
            this.nudPad1Min.Size = new System.Drawing.Size(59, 20);
            this.nudPad1Min.TabIndex = 57;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(168, 130);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(77, 13);
            this.label98.TabIndex = 56;
            this.label98.Text = "Padding 4 max";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 128);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(74, 13);
            this.label99.TabIndex = 55;
            this.label99.Text = "Padding 4 min";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(168, 103);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(77, 13);
            this.label100.TabIndex = 54;
            this.label100.Text = "Padding 3 max";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 101);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(74, 13);
            this.label101.TabIndex = 53;
            this.label101.Text = "Padding 3 min";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(168, 76);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(77, 13);
            this.label102.TabIndex = 52;
            this.label102.Text = "Padding 2 max";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 74);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(74, 13);
            this.label103.TabIndex = 51;
            this.label103.Text = "Padding 2 min";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(168, 49);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(77, 13);
            this.label104.TabIndex = 50;
            this.label104.Text = "Padding 1 max";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 47);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(74, 13);
            this.label105.TabIndex = 49;
            this.label105.Text = "Padding 1 min";
            // 
            // nudUniqPadd
            // 
            this.nudUniqPadd.Location = new System.Drawing.Point(170, 22);
            this.nudUniqPadd.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudUniqPadd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqPadd.Name = "nudUniqPadd";
            this.nudUniqPadd.Size = new System.Drawing.Size(46, 20);
            this.nudUniqPadd.TabIndex = 48;
            this.nudUniqPadd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqPadd.ValueChanged += new System.EventHandler(this.nudUniqPadd_ValueChanged);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 24);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(140, 13);
            this.label106.TabIndex = 47;
            this.label106.Text = "Number of unique paddings:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.nudShadBlurMax);
            this.groupBox11.Controls.Add(this.nudShadVMax);
            this.groupBox11.Controls.Add(this.nudShadHMax);
            this.groupBox11.Controls.Add(this.nudShadBlurMin);
            this.groupBox11.Controls.Add(this.nudShadVMin);
            this.groupBox11.Controls.Add(this.nudShadHMin);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this.label97);
            this.groupBox11.Location = new System.Drawing.Point(12, 489);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(542, 100);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Shadows";
            // 
            // nudShadBlurMax
            // 
            this.nudShadBlurMax.Location = new System.Drawing.Point(275, 73);
            this.nudShadBlurMax.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadBlurMax.Name = "nudShadBlurMax";
            this.nudShadBlurMax.Size = new System.Drawing.Size(59, 20);
            this.nudShadBlurMax.TabIndex = 75;
            this.nudShadBlurMax.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudShadVMax
            // 
            this.nudShadVMax.Location = new System.Drawing.Point(275, 45);
            this.nudShadVMax.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadVMax.Name = "nudShadVMax";
            this.nudShadVMax.Size = new System.Drawing.Size(59, 20);
            this.nudShadVMax.TabIndex = 74;
            this.nudShadVMax.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudShadHMax
            // 
            this.nudShadHMax.Location = new System.Drawing.Point(275, 19);
            this.nudShadHMax.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadHMax.Name = "nudShadHMax";
            this.nudShadHMax.Size = new System.Drawing.Size(59, 20);
            this.nudShadHMax.TabIndex = 73;
            this.nudShadHMax.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudShadBlurMin
            // 
            this.nudShadBlurMin.Location = new System.Drawing.Point(110, 70);
            this.nudShadBlurMin.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadBlurMin.Name = "nudShadBlurMin";
            this.nudShadBlurMin.Size = new System.Drawing.Size(59, 20);
            this.nudShadBlurMin.TabIndex = 72;
            // 
            // nudShadVMin
            // 
            this.nudShadVMin.Location = new System.Drawing.Point(110, 45);
            this.nudShadVMin.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadVMin.Name = "nudShadVMin";
            this.nudShadVMin.Size = new System.Drawing.Size(59, 20);
            this.nudShadVMin.TabIndex = 71;
            // 
            // nudShadHMin
            // 
            this.nudShadHMin.Location = new System.Drawing.Point(110, 22);
            this.nudShadHMin.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudShadHMin.Name = "nudShadHMin";
            this.nudShadHMin.Size = new System.Drawing.Size(59, 20);
            this.nudShadHMin.TabIndex = 70;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(175, 78);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(88, 13);
            this.label92.TabIndex = 69;
            this.label92.Text = "Shadow blur max";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(13, 76);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(85, 13);
            this.label93.TabIndex = 68;
            this.label93.Text = "Shadow blur min";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(175, 51);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(95, 13);
            this.label94.TabIndex = 67;
            this.label94.Text = "Shadow V pos min";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(13, 49);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(95, 13);
            this.label95.TabIndex = 66;
            this.label95.Text = "Shadow V pos min";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(175, 24);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(99, 13);
            this.label96.TabIndex = 65;
            this.label96.Text = "Shadow H pos max";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(13, 22);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(96, 13);
            this.label97.TabIndex = 64;
            this.label97.Text = "Shadow H pos min";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox10);
            this.groupBox7.Controls.Add(this.groupBox9);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Location = new System.Drawing.Point(561, 13);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(562, 551);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Border appearance";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.nudBr4Max);
            this.groupBox10.Controls.Add(this.nudBr3Max);
            this.groupBox10.Controls.Add(this.nudBr2Max);
            this.groupBox10.Controls.Add(this.nudBr1Max);
            this.groupBox10.Controls.Add(this.nudBr4Min);
            this.groupBox10.Controls.Add(this.nudBr3Min);
            this.groupBox10.Controls.Add(this.nudBr2Min);
            this.groupBox10.Controls.Add(this.nudBr1Min);
            this.groupBox10.Controls.Add(this.label83);
            this.groupBox10.Controls.Add(this.label84);
            this.groupBox10.Controls.Add(this.label85);
            this.groupBox10.Controls.Add(this.label86);
            this.groupBox10.Controls.Add(this.label87);
            this.groupBox10.Controls.Add(this.label88);
            this.groupBox10.Controls.Add(this.label89);
            this.groupBox10.Controls.Add(this.label90);
            this.groupBox10.Controls.Add(this.nudUniqBRad);
            this.groupBox10.Controls.Add(this.label91);
            this.groupBox10.Location = new System.Drawing.Point(7, 348);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(549, 154);
            this.groupBox10.TabIndex = 65;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Border radius";
            // 
            // nudBr4Max
            // 
            this.nudBr4Max.Enabled = false;
            this.nudBr4Max.Location = new System.Drawing.Point(268, 122);
            this.nudBr4Max.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr4Max.Name = "nudBr4Max";
            this.nudBr4Max.Size = new System.Drawing.Size(59, 20);
            this.nudBr4Max.TabIndex = 64;
            this.nudBr4Max.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // nudBr3Max
            // 
            this.nudBr3Max.Enabled = false;
            this.nudBr3Max.Location = new System.Drawing.Point(268, 98);
            this.nudBr3Max.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr3Max.Name = "nudBr3Max";
            this.nudBr3Max.Size = new System.Drawing.Size(59, 20);
            this.nudBr3Max.TabIndex = 63;
            this.nudBr3Max.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // nudBr2Max
            // 
            this.nudBr2Max.Enabled = false;
            this.nudBr2Max.Location = new System.Drawing.Point(268, 70);
            this.nudBr2Max.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr2Max.Name = "nudBr2Max";
            this.nudBr2Max.Size = new System.Drawing.Size(59, 20);
            this.nudBr2Max.TabIndex = 62;
            this.nudBr2Max.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // nudBr1Max
            // 
            this.nudBr1Max.Location = new System.Drawing.Point(268, 44);
            this.nudBr1Max.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr1Max.Name = "nudBr1Max";
            this.nudBr1Max.Size = new System.Drawing.Size(59, 20);
            this.nudBr1Max.TabIndex = 61;
            this.nudBr1Max.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // nudBr4Min
            // 
            this.nudBr4Min.Enabled = false;
            this.nudBr4Min.Location = new System.Drawing.Point(103, 121);
            this.nudBr4Min.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr4Min.Name = "nudBr4Min";
            this.nudBr4Min.Size = new System.Drawing.Size(59, 20);
            this.nudBr4Min.TabIndex = 60;
            // 
            // nudBr3Min
            // 
            this.nudBr3Min.Enabled = false;
            this.nudBr3Min.Location = new System.Drawing.Point(103, 95);
            this.nudBr3Min.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr3Min.Name = "nudBr3Min";
            this.nudBr3Min.Size = new System.Drawing.Size(59, 20);
            this.nudBr3Min.TabIndex = 59;
            // 
            // nudBr2Min
            // 
            this.nudBr2Min.Enabled = false;
            this.nudBr2Min.Location = new System.Drawing.Point(103, 70);
            this.nudBr2Min.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr2Min.Name = "nudBr2Min";
            this.nudBr2Min.Size = new System.Drawing.Size(59, 20);
            this.nudBr2Min.TabIndex = 58;
            // 
            // nudBr1Min
            // 
            this.nudBr1Min.Location = new System.Drawing.Point(103, 47);
            this.nudBr1Min.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.nudBr1Min.Name = "nudBr1Min";
            this.nudBr1Min.Size = new System.Drawing.Size(59, 20);
            this.nudBr1Min.TabIndex = 57;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(168, 130);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(100, 13);
            this.label83.TabIndex = 56;
            this.label83.Text = "Border radius 4 max";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 128);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(97, 13);
            this.label84.TabIndex = 55;
            this.label84.Text = "Border radius 4 min";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(168, 103);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(100, 13);
            this.label85.TabIndex = 54;
            this.label85.Text = "Border radius 3 max";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 101);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(97, 13);
            this.label86.TabIndex = 53;
            this.label86.Text = "Border radius 3 min";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(168, 76);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(100, 13);
            this.label87.TabIndex = 52;
            this.label87.Text = "Border radius 2 max";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 74);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(97, 13);
            this.label88.TabIndex = 51;
            this.label88.Text = "Border radius 2 min";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(168, 49);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(100, 13);
            this.label89.TabIndex = 50;
            this.label89.Text = "Border radius 1 max";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 47);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(97, 13);
            this.label90.TabIndex = 49;
            this.label90.Text = "Border radius 1 min";
            // 
            // nudUniqBRad
            // 
            this.nudUniqBRad.Location = new System.Drawing.Point(170, 22);
            this.nudUniqBRad.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudUniqBRad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBRad.Name = "nudUniqBRad";
            this.nudUniqBRad.Size = new System.Drawing.Size(46, 20);
            this.nudUniqBRad.TabIndex = 48;
            this.nudUniqBRad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBRad.ValueChanged += new System.EventHandler(this.nudUniqBRad_ValueChanged);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(6, 24);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(149, 13);
            this.label91.TabIndex = 47;
            this.label91.Text = "Number of unique border radii:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.nudBW4Max);
            this.groupBox9.Controls.Add(this.nudBW3Max);
            this.groupBox9.Controls.Add(this.nudBW2Max);
            this.groupBox9.Controls.Add(this.nudBW1Max);
            this.groupBox9.Controls.Add(this.nudBW4Min);
            this.groupBox9.Controls.Add(this.nudBW3Min);
            this.groupBox9.Controls.Add(this.nudBW2Min);
            this.groupBox9.Controls.Add(this.nudBW1Min);
            this.groupBox9.Controls.Add(this.label74);
            this.groupBox9.Controls.Add(this.label75);
            this.groupBox9.Controls.Add(this.label76);
            this.groupBox9.Controls.Add(this.label77);
            this.groupBox9.Controls.Add(this.label78);
            this.groupBox9.Controls.Add(this.label79);
            this.groupBox9.Controls.Add(this.label80);
            this.groupBox9.Controls.Add(this.label81);
            this.groupBox9.Controls.Add(this.nudUniqBWidths);
            this.groupBox9.Controls.Add(this.label82);
            this.groupBox9.Location = new System.Drawing.Point(7, 188);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(549, 154);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Border thickness";
            // 
            // nudBW4Max
            // 
            this.nudBW4Max.Enabled = false;
            this.nudBW4Max.Location = new System.Drawing.Point(268, 122);
            this.nudBW4Max.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW4Max.Name = "nudBW4Max";
            this.nudBW4Max.Size = new System.Drawing.Size(59, 20);
            this.nudBW4Max.TabIndex = 64;
            this.nudBW4Max.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudBW3Max
            // 
            this.nudBW3Max.Enabled = false;
            this.nudBW3Max.Location = new System.Drawing.Point(268, 98);
            this.nudBW3Max.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW3Max.Name = "nudBW3Max";
            this.nudBW3Max.Size = new System.Drawing.Size(59, 20);
            this.nudBW3Max.TabIndex = 63;
            this.nudBW3Max.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudBW2Max
            // 
            this.nudBW2Max.Enabled = false;
            this.nudBW2Max.Location = new System.Drawing.Point(268, 70);
            this.nudBW2Max.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW2Max.Name = "nudBW2Max";
            this.nudBW2Max.Size = new System.Drawing.Size(59, 20);
            this.nudBW2Max.TabIndex = 62;
            this.nudBW2Max.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudBW1Max
            // 
            this.nudBW1Max.Location = new System.Drawing.Point(268, 44);
            this.nudBW1Max.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW1Max.Name = "nudBW1Max";
            this.nudBW1Max.Size = new System.Drawing.Size(59, 20);
            this.nudBW1Max.TabIndex = 61;
            this.nudBW1Max.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // nudBW4Min
            // 
            this.nudBW4Min.Enabled = false;
            this.nudBW4Min.Location = new System.Drawing.Point(103, 121);
            this.nudBW4Min.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW4Min.Name = "nudBW4Min";
            this.nudBW4Min.Size = new System.Drawing.Size(59, 20);
            this.nudBW4Min.TabIndex = 60;
            // 
            // nudBW3Min
            // 
            this.nudBW3Min.Enabled = false;
            this.nudBW3Min.Location = new System.Drawing.Point(103, 95);
            this.nudBW3Min.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW3Min.Name = "nudBW3Min";
            this.nudBW3Min.Size = new System.Drawing.Size(59, 20);
            this.nudBW3Min.TabIndex = 59;
            // 
            // nudBW2Min
            // 
            this.nudBW2Min.Enabled = false;
            this.nudBW2Min.Location = new System.Drawing.Point(103, 70);
            this.nudBW2Min.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW2Min.Name = "nudBW2Min";
            this.nudBW2Min.Size = new System.Drawing.Size(59, 20);
            this.nudBW2Min.TabIndex = 58;
            // 
            // nudBW1Min
            // 
            this.nudBW1Min.Location = new System.Drawing.Point(103, 47);
            this.nudBW1Min.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudBW1Min.Name = "nudBW1Min";
            this.nudBW1Min.Size = new System.Drawing.Size(59, 20);
            this.nudBW1Min.TabIndex = 57;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(168, 130);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(97, 13);
            this.label74.TabIndex = 56;
            this.label74.Text = "Border width 4 max";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(6, 128);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(94, 13);
            this.label75.TabIndex = 55;
            this.label75.Text = "Border width 4 min";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(168, 103);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(97, 13);
            this.label76.TabIndex = 54;
            this.label76.Text = "Border width 3 max";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 101);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(94, 13);
            this.label77.TabIndex = 53;
            this.label77.Text = "Border width 3 min";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(168, 76);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(97, 13);
            this.label78.TabIndex = 52;
            this.label78.Text = "Border width 2 max";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 74);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(94, 13);
            this.label79.TabIndex = 51;
            this.label79.Text = "Border width 2 min";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(168, 49);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(97, 13);
            this.label80.TabIndex = 50;
            this.label80.Text = "Border width 1 max";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(94, 13);
            this.label81.TabIndex = 49;
            this.label81.Text = "Border width 1 min";
            // 
            // nudUniqBWidths
            // 
            this.nudUniqBWidths.Location = new System.Drawing.Point(170, 22);
            this.nudUniqBWidths.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudUniqBWidths.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBWidths.Name = "nudUniqBWidths";
            this.nudUniqBWidths.Size = new System.Drawing.Size(46, 20);
            this.nudUniqBWidths.TabIndex = 48;
            this.nudUniqBWidths.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBWidths.ValueChanged += new System.EventHandler(this.nudUniqBWidths_ValueChanged);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 24);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(160, 13);
            this.label82.TabIndex = 47;
            this.label82.Text = "Number of unique border widths:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cmbBStyle4Max);
            this.groupBox8.Controls.Add(this.label72);
            this.groupBox8.Controls.Add(this.cmbBStyle4Min);
            this.groupBox8.Controls.Add(this.label73);
            this.groupBox8.Controls.Add(this.cmbBStyle3Max);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Controls.Add(this.cmbBStyle3Min);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Controls.Add(this.cmbBStyle2Max);
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Controls.Add(this.cmbBStyle2Min);
            this.groupBox8.Controls.Add(this.label69);
            this.groupBox8.Controls.Add(this.cmbBStyle1Max);
            this.groupBox8.Controls.Add(this.label67);
            this.groupBox8.Controls.Add(this.cmbBStyle1Min);
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Controls.Add(this.nudUniqBStyles);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Location = new System.Drawing.Point(7, 20);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(549, 158);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Border styles";
            // 
            // cmbBStyle4Max
            // 
            this.cmbBStyle4Max.Enabled = false;
            this.cmbBStyle4Max.FormattingEnabled = true;
            this.cmbBStyle4Max.Location = new System.Drawing.Point(333, 131);
            this.cmbBStyle4Max.Name = "cmbBStyle4Max";
            this.cmbBStyle4Max.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle4Max.TabIndex = 47;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(233, 131);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(93, 13);
            this.label72.TabIndex = 46;
            this.label72.Text = "Border style 4 max";
            // 
            // cmbBStyle4Min
            // 
            this.cmbBStyle4Min.Enabled = false;
            this.cmbBStyle4Min.FormattingEnabled = true;
            this.cmbBStyle4Min.Location = new System.Drawing.Point(106, 128);
            this.cmbBStyle4Min.Name = "cmbBStyle4Min";
            this.cmbBStyle4Min.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle4Min.TabIndex = 45;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(90, 13);
            this.label73.TabIndex = 44;
            this.label73.Text = "Border style 4 min";
            // 
            // cmbBStyle3Max
            // 
            this.cmbBStyle3Max.Enabled = false;
            this.cmbBStyle3Max.FormattingEnabled = true;
            this.cmbBStyle3Max.Location = new System.Drawing.Point(333, 104);
            this.cmbBStyle3Max.Name = "cmbBStyle3Max";
            this.cmbBStyle3Max.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle3Max.TabIndex = 43;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(233, 104);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(93, 13);
            this.label70.TabIndex = 42;
            this.label70.Text = "Border style 3 max";
            // 
            // cmbBStyle3Min
            // 
            this.cmbBStyle3Min.Enabled = false;
            this.cmbBStyle3Min.FormattingEnabled = true;
            this.cmbBStyle3Min.Location = new System.Drawing.Point(106, 101);
            this.cmbBStyle3Min.Name = "cmbBStyle3Min";
            this.cmbBStyle3Min.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle3Min.TabIndex = 41;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 101);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(90, 13);
            this.label71.TabIndex = 40;
            this.label71.Text = "Border style 3 min";
            // 
            // cmbBStyle2Max
            // 
            this.cmbBStyle2Max.Enabled = false;
            this.cmbBStyle2Max.FormattingEnabled = true;
            this.cmbBStyle2Max.Location = new System.Drawing.Point(333, 77);
            this.cmbBStyle2Max.Name = "cmbBStyle2Max";
            this.cmbBStyle2Max.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle2Max.TabIndex = 39;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(233, 77);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(93, 13);
            this.label68.TabIndex = 38;
            this.label68.Text = "Border style 2 max";
            // 
            // cmbBStyle2Min
            // 
            this.cmbBStyle2Min.Enabled = false;
            this.cmbBStyle2Min.FormattingEnabled = true;
            this.cmbBStyle2Min.Location = new System.Drawing.Point(106, 74);
            this.cmbBStyle2Min.Name = "cmbBStyle2Min";
            this.cmbBStyle2Min.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle2Min.TabIndex = 37;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 74);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(90, 13);
            this.label69.TabIndex = 36;
            this.label69.Text = "Border style 2 min";
            // 
            // cmbBStyle1Max
            // 
            this.cmbBStyle1Max.FormattingEnabled = true;
            this.cmbBStyle1Max.Location = new System.Drawing.Point(333, 50);
            this.cmbBStyle1Max.Name = "cmbBStyle1Max";
            this.cmbBStyle1Max.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle1Max.TabIndex = 35;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(233, 50);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(93, 13);
            this.label67.TabIndex = 34;
            this.label67.Text = "Border style 1 max";
            // 
            // cmbBStyle1Min
            // 
            this.cmbBStyle1Min.FormattingEnabled = true;
            this.cmbBStyle1Min.Location = new System.Drawing.Point(106, 47);
            this.cmbBStyle1Min.Name = "cmbBStyle1Min";
            this.cmbBStyle1Min.Size = new System.Drawing.Size(121, 21);
            this.cmbBStyle1Min.TabIndex = 33;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 47);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(90, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "Border style 1 min";
            // 
            // nudUniqBStyles
            // 
            this.nudUniqBStyles.Location = new System.Drawing.Point(170, 22);
            this.nudUniqBStyles.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudUniqBStyles.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBStyles.Name = "nudUniqBStyles";
            this.nudUniqBStyles.Size = new System.Drawing.Size(46, 20);
            this.nudUniqBStyles.TabIndex = 31;
            this.nudUniqBStyles.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBStyles.ValueChanged += new System.EventHandler(this.nudUniqBStyles_ValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 24);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(156, 13);
            this.label65.TabIndex = 30;
            this.label65.Text = "Number of unique border styles:";
            // 
            // grbColors
            // 
            this.grbColors.Controls.Add(this.chkTransparent);
            this.grbColors.Controls.Add(this.chkGradient);
            this.grbColors.Controls.Add(this.grbGradient);
            this.grbColors.Controls.Add(this.nudUniqBColors);
            this.grbColors.Controls.Add(this.label64);
            this.grbColors.Controls.Add(this.grbC4);
            this.grbColors.Controls.Add(this.grbC3);
            this.grbColors.Controls.Add(this.groupBox3);
            this.grbColors.Controls.Add(this.groupBox2);
            this.grbColors.Controls.Add(this.grbC2);
            this.grbColors.Controls.Add(this.groupBox1);
            this.grbColors.Controls.Add(this.grbC1);
            this.grbColors.Controls.Add(this.grbBGColor);
            this.grbColors.Location = new System.Drawing.Point(12, 12);
            this.grbColors.Name = "grbColors";
            this.grbColors.Size = new System.Drawing.Size(542, 471);
            this.grbColors.TabIndex = 0;
            this.grbColors.TabStop = false;
            this.grbColors.Text = "Colors";
            // 
            // chkTransparent
            // 
            this.chkTransparent.AutoSize = true;
            this.chkTransparent.Location = new System.Drawing.Point(139, 444);
            this.chkTransparent.Name = "chkTransparent";
            this.chkTransparent.Size = new System.Drawing.Size(100, 17);
            this.chkTransparent.TabIndex = 25;
            this.chkTransparent.Text = "No background";
            this.chkTransparent.UseVisualStyleBackColor = true;
            this.chkTransparent.CheckedChanged += new System.EventHandler(this.chkTransparent_CheckedChanged);
            // 
            // chkGradient
            // 
            this.chkGradient.AutoSize = true;
            this.chkGradient.Location = new System.Drawing.Point(7, 444);
            this.chkGradient.Name = "chkGradient";
            this.chkGradient.Size = new System.Drawing.Size(126, 17);
            this.chkGradient.TabIndex = 24;
            this.chkGradient.Text = "Gradient background";
            this.chkGradient.UseVisualStyleBackColor = true;
            this.chkGradient.CheckedChanged += new System.EventHandler(this.chkGradient_CheckedChanged);
            // 
            // grbGradient
            // 
            this.grbGradient.Controls.Add(this.nudGradAlphaMax);
            this.grbGradient.Controls.Add(this.label144);
            this.grbGradient.Controls.Add(this.nudGradAlphaMin);
            this.grbGradient.Controls.Add(this.label145);
            this.grbGradient.Controls.Add(this.label146);
            this.grbGradient.Controls.Add(this.nudGradBlueMax);
            this.grbGradient.Controls.Add(this.nudGradGreenMax);
            this.grbGradient.Controls.Add(this.nudGradRedMax);
            this.grbGradient.Controls.Add(this.label147);
            this.grbGradient.Controls.Add(this.label148);
            this.grbGradient.Controls.Add(this.label149);
            this.grbGradient.Controls.Add(this.nudGradBlueMin);
            this.grbGradient.Controls.Add(this.nudGradGreenMin);
            this.grbGradient.Controls.Add(this.nudGradRedMin);
            this.grbGradient.Controls.Add(this.label150);
            this.grbGradient.Controls.Add(this.label151);
            this.grbGradient.Controls.Add(this.label152);
            this.grbGradient.Controls.Add(this.label153);
            this.grbGradient.Controls.Add(this.label154);
            this.grbGradient.Controls.Add(this.label155);
            this.grbGradient.Enabled = false;
            this.grbGradient.Location = new System.Drawing.Point(9, 339);
            this.grbGradient.Name = "grbGradient";
            this.grbGradient.Size = new System.Drawing.Size(262, 100);
            this.grbGradient.TabIndex = 21;
            this.grbGradient.TabStop = false;
            this.grbGradient.Text = "Gradient color range";
            // 
            // nudGradAlphaMax
            // 
            this.nudGradAlphaMax.Location = new System.Drawing.Point(185, 73);
            this.nudGradAlphaMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradAlphaMax.Name = "nudGradAlphaMax";
            this.nudGradAlphaMax.Size = new System.Drawing.Size(66, 20);
            this.nudGradAlphaMax.TabIndex = 20;
            this.nudGradAlphaMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(152, 79);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(27, 13);
            this.label144.TabIndex = 19;
            this.label144.Text = "Max";
            // 
            // nudGradAlphaMin
            // 
            this.nudGradAlphaMin.Location = new System.Drawing.Point(83, 73);
            this.nudGradAlphaMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradAlphaMin.Name = "nudGradAlphaMin";
            this.nudGradAlphaMin.Size = new System.Drawing.Size(66, 20);
            this.nudGradAlphaMin.TabIndex = 18;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(50, 79);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(24, 13);
            this.label145.TabIndex = 17;
            this.label145.Text = "Min";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(7, 78);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(34, 13);
            this.label146.TabIndex = 16;
            this.label146.Text = "Alpha";
            // 
            // nudGradBlueMax
            // 
            this.nudGradBlueMax.Location = new System.Drawing.Point(185, 54);
            this.nudGradBlueMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradBlueMax.Name = "nudGradBlueMax";
            this.nudGradBlueMax.Size = new System.Drawing.Size(66, 20);
            this.nudGradBlueMax.TabIndex = 15;
            this.nudGradBlueMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudGradGreenMax
            // 
            this.nudGradGreenMax.Location = new System.Drawing.Point(185, 37);
            this.nudGradGreenMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradGreenMax.Name = "nudGradGreenMax";
            this.nudGradGreenMax.Size = new System.Drawing.Size(66, 20);
            this.nudGradGreenMax.TabIndex = 14;
            this.nudGradGreenMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudGradRedMax
            // 
            this.nudGradRedMax.Location = new System.Drawing.Point(185, 18);
            this.nudGradRedMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradRedMax.Name = "nudGradRedMax";
            this.nudGradRedMax.Size = new System.Drawing.Size(66, 20);
            this.nudGradRedMax.TabIndex = 13;
            this.nudGradRedMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(152, 60);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(27, 13);
            this.label147.TabIndex = 12;
            this.label147.Text = "Max";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(152, 40);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(27, 13);
            this.label148.TabIndex = 11;
            this.label148.Text = "Max";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(152, 20);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(27, 13);
            this.label149.TabIndex = 10;
            this.label149.Text = "Max";
            // 
            // nudGradBlueMin
            // 
            this.nudGradBlueMin.Location = new System.Drawing.Point(83, 54);
            this.nudGradBlueMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradBlueMin.Name = "nudGradBlueMin";
            this.nudGradBlueMin.Size = new System.Drawing.Size(66, 20);
            this.nudGradBlueMin.TabIndex = 9;
            // 
            // nudGradGreenMin
            // 
            this.nudGradGreenMin.Location = new System.Drawing.Point(83, 37);
            this.nudGradGreenMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradGreenMin.Name = "nudGradGreenMin";
            this.nudGradGreenMin.Size = new System.Drawing.Size(66, 20);
            this.nudGradGreenMin.TabIndex = 8;
            // 
            // nudGradRedMin
            // 
            this.nudGradRedMin.Location = new System.Drawing.Point(83, 18);
            this.nudGradRedMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudGradRedMin.Name = "nudGradRedMin";
            this.nudGradRedMin.Size = new System.Drawing.Size(66, 20);
            this.nudGradRedMin.TabIndex = 7;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(50, 60);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(24, 13);
            this.label150.TabIndex = 6;
            this.label150.Text = "Min";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(50, 40);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(24, 13);
            this.label151.TabIndex = 5;
            this.label151.Text = "Min";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(50, 20);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(24, 13);
            this.label152.TabIndex = 4;
            this.label152.Text = "Min";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(7, 59);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(28, 13);
            this.label153.TabIndex = 2;
            this.label153.Text = "Blue";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(7, 40);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(36, 13);
            this.label154.TabIndex = 1;
            this.label154.Text = "Green";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(7, 20);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(27, 13);
            this.label155.TabIndex = 0;
            this.label155.Text = "Red";
            // 
            // nudUniqBColors
            // 
            this.nudUniqBColors.Location = new System.Drawing.Point(489, 444);
            this.nudUniqBColors.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudUniqBColors.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBColors.Name = "nudUniqBColors";
            this.nudUniqBColors.Size = new System.Drawing.Size(46, 20);
            this.nudUniqBColors.TabIndex = 23;
            this.nudUniqBColors.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudUniqBColors.ValueChanged += new System.EventHandler(this.nudUniqBColors_ValueChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(325, 446);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(158, 13);
            this.label64.TabIndex = 22;
            this.label64.Text = "Number of unique border colors:";
            // 
            // grbC4
            // 
            this.grbC4.Controls.Add(this.nudBC4AMax);
            this.grbC4.Controls.Add(this.label129);
            this.grbC4.Controls.Add(this.nudBC4AMin);
            this.grbC4.Controls.Add(this.label130);
            this.grbC4.Controls.Add(this.label131);
            this.grbC4.Controls.Add(this.nudBC4BMax);
            this.grbC4.Controls.Add(this.nudBC4GMax);
            this.grbC4.Controls.Add(this.nudBC4RMax);
            this.grbC4.Controls.Add(this.label55);
            this.grbC4.Controls.Add(this.label56);
            this.grbC4.Controls.Add(this.label57);
            this.grbC4.Controls.Add(this.nudBC4BMin);
            this.grbC4.Controls.Add(this.nudBC4GMin);
            this.grbC4.Controls.Add(this.nudBC4RMin);
            this.grbC4.Controls.Add(this.label58);
            this.grbC4.Controls.Add(this.label59);
            this.grbC4.Controls.Add(this.label60);
            this.grbC4.Controls.Add(this.label61);
            this.grbC4.Controls.Add(this.label62);
            this.grbC4.Controls.Add(this.label63);
            this.grbC4.Enabled = false;
            this.grbC4.Location = new System.Drawing.Point(275, 338);
            this.grbC4.Name = "grbC4";
            this.grbC4.Size = new System.Drawing.Size(262, 100);
            this.grbC4.TabIndex = 21;
            this.grbC4.TabStop = false;
            this.grbC4.Text = "Border color 4 range";
            // 
            // nudBC4AMax
            // 
            this.nudBC4AMax.Location = new System.Drawing.Point(185, 73);
            this.nudBC4AMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4AMax.Name = "nudBC4AMax";
            this.nudBC4AMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC4AMax.TabIndex = 20;
            this.nudBC4AMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(152, 79);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(27, 13);
            this.label129.TabIndex = 19;
            this.label129.Text = "Max";
            // 
            // nudBC4AMin
            // 
            this.nudBC4AMin.Location = new System.Drawing.Point(83, 73);
            this.nudBC4AMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4AMin.Name = "nudBC4AMin";
            this.nudBC4AMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC4AMin.TabIndex = 18;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(50, 79);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(24, 13);
            this.label130.TabIndex = 17;
            this.label130.Text = "Min";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(7, 78);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(34, 13);
            this.label131.TabIndex = 16;
            this.label131.Text = "Alpha";
            // 
            // nudBC4BMax
            // 
            this.nudBC4BMax.Location = new System.Drawing.Point(185, 54);
            this.nudBC4BMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4BMax.Name = "nudBC4BMax";
            this.nudBC4BMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC4BMax.TabIndex = 15;
            this.nudBC4BMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC4GMax
            // 
            this.nudBC4GMax.Location = new System.Drawing.Point(185, 37);
            this.nudBC4GMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4GMax.Name = "nudBC4GMax";
            this.nudBC4GMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC4GMax.TabIndex = 14;
            this.nudBC4GMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC4RMax
            // 
            this.nudBC4RMax.Location = new System.Drawing.Point(185, 18);
            this.nudBC4RMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4RMax.Name = "nudBC4RMax";
            this.nudBC4RMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC4RMax.TabIndex = 13;
            this.nudBC4RMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(152, 60);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 12;
            this.label55.Text = "Max";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(152, 40);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(27, 13);
            this.label56.TabIndex = 11;
            this.label56.Text = "Max";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(152, 20);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(27, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "Max";
            // 
            // nudBC4BMin
            // 
            this.nudBC4BMin.Location = new System.Drawing.Point(83, 54);
            this.nudBC4BMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4BMin.Name = "nudBC4BMin";
            this.nudBC4BMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC4BMin.TabIndex = 9;
            // 
            // nudBC4GMin
            // 
            this.nudBC4GMin.Location = new System.Drawing.Point(83, 37);
            this.nudBC4GMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4GMin.Name = "nudBC4GMin";
            this.nudBC4GMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC4GMin.TabIndex = 8;
            // 
            // nudBC4RMin
            // 
            this.nudBC4RMin.Location = new System.Drawing.Point(83, 18);
            this.nudBC4RMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC4RMin.Name = "nudBC4RMin";
            this.nudBC4RMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC4RMin.TabIndex = 7;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(50, 60);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(24, 13);
            this.label58.TabIndex = 6;
            this.label58.Text = "Min";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(50, 40);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(24, 13);
            this.label59.TabIndex = 5;
            this.label59.Text = "Min";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(50, 20);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(24, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "Min";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(7, 59);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "Blue";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(7, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(36, 13);
            this.label62.TabIndex = 1;
            this.label62.Text = "Green";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(7, 20);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(27, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Red";
            // 
            // grbC3
            // 
            this.grbC3.Controls.Add(this.nudBC3AMax);
            this.grbC3.Controls.Add(this.label126);
            this.grbC3.Controls.Add(this.nudBC3AMin);
            this.grbC3.Controls.Add(this.label127);
            this.grbC3.Controls.Add(this.label128);
            this.grbC3.Controls.Add(this.nudBC3BMax);
            this.grbC3.Controls.Add(this.nudBC3GMax);
            this.grbC3.Controls.Add(this.nudBC3RMax);
            this.grbC3.Controls.Add(this.label28);
            this.grbC3.Controls.Add(this.label29);
            this.grbC3.Controls.Add(this.label30);
            this.grbC3.Controls.Add(this.nudBC3BMin);
            this.grbC3.Controls.Add(this.nudBC3GMin);
            this.grbC3.Controls.Add(this.nudBC3RMin);
            this.grbC3.Controls.Add(this.label31);
            this.grbC3.Controls.Add(this.label32);
            this.grbC3.Controls.Add(this.label33);
            this.grbC3.Controls.Add(this.label34);
            this.grbC3.Controls.Add(this.label35);
            this.grbC3.Controls.Add(this.label36);
            this.grbC3.Enabled = false;
            this.grbC3.Location = new System.Drawing.Point(275, 232);
            this.grbC3.Name = "grbC3";
            this.grbC3.Size = new System.Drawing.Size(262, 100);
            this.grbC3.TabIndex = 20;
            this.grbC3.TabStop = false;
            this.grbC3.Text = "Border color 3 range";
            // 
            // nudBC3AMax
            // 
            this.nudBC3AMax.Location = new System.Drawing.Point(185, 73);
            this.nudBC3AMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3AMax.Name = "nudBC3AMax";
            this.nudBC3AMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC3AMax.TabIndex = 20;
            this.nudBC3AMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(152, 79);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(27, 13);
            this.label126.TabIndex = 19;
            this.label126.Text = "Max";
            // 
            // nudBC3AMin
            // 
            this.nudBC3AMin.Location = new System.Drawing.Point(83, 73);
            this.nudBC3AMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3AMin.Name = "nudBC3AMin";
            this.nudBC3AMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC3AMin.TabIndex = 18;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(50, 79);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(24, 13);
            this.label127.TabIndex = 17;
            this.label127.Text = "Min";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(7, 78);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(34, 13);
            this.label128.TabIndex = 16;
            this.label128.Text = "Alpha";
            // 
            // nudBC3BMax
            // 
            this.nudBC3BMax.Location = new System.Drawing.Point(185, 54);
            this.nudBC3BMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3BMax.Name = "nudBC3BMax";
            this.nudBC3BMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC3BMax.TabIndex = 15;
            this.nudBC3BMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC3GMax
            // 
            this.nudBC3GMax.Location = new System.Drawing.Point(185, 37);
            this.nudBC3GMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3GMax.Name = "nudBC3GMax";
            this.nudBC3GMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC3GMax.TabIndex = 14;
            this.nudBC3GMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC3RMax
            // 
            this.nudBC3RMax.Location = new System.Drawing.Point(185, 18);
            this.nudBC3RMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3RMax.Name = "nudBC3RMax";
            this.nudBC3RMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC3RMax.TabIndex = 13;
            this.nudBC3RMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(152, 60);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 13);
            this.label28.TabIndex = 12;
            this.label28.Text = "Max";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(152, 40);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 13);
            this.label29.TabIndex = 11;
            this.label29.Text = "Max";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(152, 20);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "Max";
            // 
            // nudBC3BMin
            // 
            this.nudBC3BMin.Location = new System.Drawing.Point(83, 54);
            this.nudBC3BMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3BMin.Name = "nudBC3BMin";
            this.nudBC3BMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC3BMin.TabIndex = 9;
            // 
            // nudBC3GMin
            // 
            this.nudBC3GMin.Location = new System.Drawing.Point(83, 37);
            this.nudBC3GMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3GMin.Name = "nudBC3GMin";
            this.nudBC3GMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC3GMin.TabIndex = 8;
            // 
            // nudBC3RMin
            // 
            this.nudBC3RMin.Location = new System.Drawing.Point(83, 18);
            this.nudBC3RMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC3RMin.Name = "nudBC3RMin";
            this.nudBC3RMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC3RMin.TabIndex = 7;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(50, 60);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "Min";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(50, 40);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Min";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(50, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(24, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Min";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 59);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Blue";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 40);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Green";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 20);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(27, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Red";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Controls.Add(this.label132);
            this.groupBox3.Controls.Add(this.numericUpDown2);
            this.groupBox3.Controls.Add(this.label133);
            this.groupBox3.Controls.Add(this.label134);
            this.groupBox3.Controls.Add(this.numericUpDown3);
            this.groupBox3.Controls.Add(this.numericUpDown4);
            this.groupBox3.Controls.Add(this.numericUpDown5);
            this.groupBox3.Controls.Add(this.label135);
            this.groupBox3.Controls.Add(this.label136);
            this.groupBox3.Controls.Add(this.label137);
            this.groupBox3.Controls.Add(this.numericUpDown6);
            this.groupBox3.Controls.Add(this.numericUpDown7);
            this.groupBox3.Controls.Add(this.numericUpDown8);
            this.groupBox3.Controls.Add(this.label138);
            this.groupBox3.Controls.Add(this.label139);
            this.groupBox3.Controls.Add(this.label140);
            this.groupBox3.Controls.Add(this.label141);
            this.groupBox3.Controls.Add(this.label142);
            this.groupBox3.Controls.Add(this.label143);
            this.groupBox3.Location = new System.Drawing.Point(7, 232);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(262, 100);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Text color / Shadow color range";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(185, 73);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(152, 79);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(27, 13);
            this.label132.TabIndex = 19;
            this.label132.Text = "Max";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(83, 73);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown2.TabIndex = 18;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(50, 79);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(24, 13);
            this.label133.TabIndex = 17;
            this.label133.Text = "Min";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(7, 78);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(34, 13);
            this.label134.TabIndex = 16;
            this.label134.Text = "Alpha";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(185, 54);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown3.TabIndex = 15;
            this.numericUpDown3.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(185, 37);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown4.TabIndex = 14;
            this.numericUpDown4.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(185, 18);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown5.TabIndex = 13;
            this.numericUpDown5.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(152, 60);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(27, 13);
            this.label135.TabIndex = 12;
            this.label135.Text = "Max";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(152, 40);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(27, 13);
            this.label136.TabIndex = 11;
            this.label136.Text = "Max";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(152, 20);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(27, 13);
            this.label137.TabIndex = 10;
            this.label137.Text = "Max";
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Location = new System.Drawing.Point(83, 54);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown6.TabIndex = 9;
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.Location = new System.Drawing.Point(83, 37);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown7.TabIndex = 8;
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.Location = new System.Drawing.Point(83, 18);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(66, 20);
            this.numericUpDown8.TabIndex = 7;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(50, 60);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(24, 13);
            this.label138.TabIndex = 6;
            this.label138.Text = "Min";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(50, 40);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(24, 13);
            this.label139.TabIndex = 5;
            this.label139.Text = "Min";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(50, 20);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(24, 13);
            this.label140.TabIndex = 4;
            this.label140.Text = "Min";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(7, 59);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(28, 13);
            this.label141.TabIndex = 2;
            this.label141.Text = "Blue";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(7, 40);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(36, 13);
            this.label142.TabIndex = 1;
            this.label142.Text = "Green";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(7, 20);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(27, 13);
            this.label143.TabIndex = 0;
            this.label143.Text = "Red";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nudShadowAlphaMax);
            this.groupBox2.Controls.Add(this.label117);
            this.groupBox2.Controls.Add(this.nudShadowAlphaMin);
            this.groupBox2.Controls.Add(this.label118);
            this.groupBox2.Controls.Add(this.label119);
            this.groupBox2.Controls.Add(this.nudShadowBlueMax);
            this.groupBox2.Controls.Add(this.nudShadowGreenMax);
            this.groupBox2.Controls.Add(this.nudShadowRedMax);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.nudShadowBlueMin);
            this.groupBox2.Controls.Add(this.nudShadowGreenMin);
            this.groupBox2.Controls.Add(this.nudShadowRedMin);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Location = new System.Drawing.Point(7, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(262, 100);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Text color / Shadow color range";
            // 
            // nudShadowAlphaMax
            // 
            this.nudShadowAlphaMax.Location = new System.Drawing.Point(185, 73);
            this.nudShadowAlphaMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowAlphaMax.Name = "nudShadowAlphaMax";
            this.nudShadowAlphaMax.Size = new System.Drawing.Size(66, 20);
            this.nudShadowAlphaMax.TabIndex = 20;
            this.nudShadowAlphaMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(152, 79);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(27, 13);
            this.label117.TabIndex = 19;
            this.label117.Text = "Max";
            // 
            // nudShadowAlphaMin
            // 
            this.nudShadowAlphaMin.Location = new System.Drawing.Point(83, 73);
            this.nudShadowAlphaMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowAlphaMin.Name = "nudShadowAlphaMin";
            this.nudShadowAlphaMin.Size = new System.Drawing.Size(66, 20);
            this.nudShadowAlphaMin.TabIndex = 18;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(50, 79);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(24, 13);
            this.label118.TabIndex = 17;
            this.label118.Text = "Min";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(7, 78);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(34, 13);
            this.label119.TabIndex = 16;
            this.label119.Text = "Alpha";
            // 
            // nudShadowBlueMax
            // 
            this.nudShadowBlueMax.Location = new System.Drawing.Point(185, 54);
            this.nudShadowBlueMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowBlueMax.Name = "nudShadowBlueMax";
            this.nudShadowBlueMax.Size = new System.Drawing.Size(66, 20);
            this.nudShadowBlueMax.TabIndex = 15;
            this.nudShadowBlueMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudShadowGreenMax
            // 
            this.nudShadowGreenMax.Location = new System.Drawing.Point(185, 37);
            this.nudShadowGreenMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowGreenMax.Name = "nudShadowGreenMax";
            this.nudShadowGreenMax.Size = new System.Drawing.Size(66, 20);
            this.nudShadowGreenMax.TabIndex = 14;
            this.nudShadowGreenMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudShadowRedMax
            // 
            this.nudShadowRedMax.Location = new System.Drawing.Point(185, 18);
            this.nudShadowRedMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowRedMax.Name = "nudShadowRedMax";
            this.nudShadowRedMax.Size = new System.Drawing.Size(66, 20);
            this.nudShadowRedMax.TabIndex = 13;
            this.nudShadowRedMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(152, 60);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Max";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(152, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "Max";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(152, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "Max";
            // 
            // nudShadowBlueMin
            // 
            this.nudShadowBlueMin.Location = new System.Drawing.Point(83, 54);
            this.nudShadowBlueMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowBlueMin.Name = "nudShadowBlueMin";
            this.nudShadowBlueMin.Size = new System.Drawing.Size(66, 20);
            this.nudShadowBlueMin.TabIndex = 9;
            // 
            // nudShadowGreenMin
            // 
            this.nudShadowGreenMin.Location = new System.Drawing.Point(83, 37);
            this.nudShadowGreenMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowGreenMin.Name = "nudShadowGreenMin";
            this.nudShadowGreenMin.Size = new System.Drawing.Size(66, 20);
            this.nudShadowGreenMin.TabIndex = 8;
            // 
            // nudShadowRedMin
            // 
            this.nudShadowRedMin.Location = new System.Drawing.Point(83, 18);
            this.nudShadowRedMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudShadowRedMin.Name = "nudShadowRedMin";
            this.nudShadowRedMin.Size = new System.Drawing.Size(66, 20);
            this.nudShadowRedMin.TabIndex = 7;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(50, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Min";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(50, 40);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 13);
            this.label23.TabIndex = 5;
            this.label23.Text = "Min";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(50, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(24, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Min";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 59);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(28, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Blue";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Green";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Red";
            // 
            // grbC2
            // 
            this.grbC2.Controls.Add(this.nudBC2AMax);
            this.grbC2.Controls.Add(this.label123);
            this.grbC2.Controls.Add(this.nudBC2AMin);
            this.grbC2.Controls.Add(this.label124);
            this.grbC2.Controls.Add(this.label125);
            this.grbC2.Controls.Add(this.nudBC2BMax);
            this.grbC2.Controls.Add(this.nudBC2GMax);
            this.grbC2.Controls.Add(this.nudBC2RMax);
            this.grbC2.Controls.Add(this.label37);
            this.grbC2.Controls.Add(this.label38);
            this.grbC2.Controls.Add(this.label39);
            this.grbC2.Controls.Add(this.nudBC2BMin);
            this.grbC2.Controls.Add(this.nudBC2GMin);
            this.grbC2.Controls.Add(this.nudBC2RMin);
            this.grbC2.Controls.Add(this.label40);
            this.grbC2.Controls.Add(this.label41);
            this.grbC2.Controls.Add(this.label42);
            this.grbC2.Controls.Add(this.label43);
            this.grbC2.Controls.Add(this.label44);
            this.grbC2.Controls.Add(this.label45);
            this.grbC2.Enabled = false;
            this.grbC2.Location = new System.Drawing.Point(275, 126);
            this.grbC2.Name = "grbC2";
            this.grbC2.Size = new System.Drawing.Size(262, 100);
            this.grbC2.TabIndex = 19;
            this.grbC2.TabStop = false;
            this.grbC2.Text = "Border color 2 range";
            // 
            // nudBC2AMax
            // 
            this.nudBC2AMax.Location = new System.Drawing.Point(185, 73);
            this.nudBC2AMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2AMax.Name = "nudBC2AMax";
            this.nudBC2AMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC2AMax.TabIndex = 20;
            this.nudBC2AMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(152, 79);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(27, 13);
            this.label123.TabIndex = 19;
            this.label123.Text = "Max";
            // 
            // nudBC2AMin
            // 
            this.nudBC2AMin.Location = new System.Drawing.Point(83, 73);
            this.nudBC2AMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2AMin.Name = "nudBC2AMin";
            this.nudBC2AMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC2AMin.TabIndex = 18;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(50, 79);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(24, 13);
            this.label124.TabIndex = 17;
            this.label124.Text = "Min";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(7, 78);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(34, 13);
            this.label125.TabIndex = 16;
            this.label125.Text = "Alpha";
            // 
            // nudBC2BMax
            // 
            this.nudBC2BMax.Location = new System.Drawing.Point(185, 54);
            this.nudBC2BMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2BMax.Name = "nudBC2BMax";
            this.nudBC2BMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC2BMax.TabIndex = 15;
            this.nudBC2BMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC2GMax
            // 
            this.nudBC2GMax.Location = new System.Drawing.Point(185, 37);
            this.nudBC2GMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2GMax.Name = "nudBC2GMax";
            this.nudBC2GMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC2GMax.TabIndex = 14;
            this.nudBC2GMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC2RMax
            // 
            this.nudBC2RMax.Location = new System.Drawing.Point(185, 18);
            this.nudBC2RMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2RMax.Name = "nudBC2RMax";
            this.nudBC2RMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC2RMax.TabIndex = 13;
            this.nudBC2RMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(152, 60);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 13);
            this.label37.TabIndex = 12;
            this.label37.Text = "Max";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(152, 40);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 13);
            this.label38.TabIndex = 11;
            this.label38.Text = "Max";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(152, 20);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(27, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Max";
            // 
            // nudBC2BMin
            // 
            this.nudBC2BMin.Location = new System.Drawing.Point(83, 54);
            this.nudBC2BMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2BMin.Name = "nudBC2BMin";
            this.nudBC2BMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC2BMin.TabIndex = 9;
            // 
            // nudBC2GMin
            // 
            this.nudBC2GMin.Location = new System.Drawing.Point(83, 37);
            this.nudBC2GMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2GMin.Name = "nudBC2GMin";
            this.nudBC2GMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC2GMin.TabIndex = 8;
            // 
            // nudBC2RMin
            // 
            this.nudBC2RMin.Location = new System.Drawing.Point(83, 18);
            this.nudBC2RMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC2RMin.Name = "nudBC2RMin";
            this.nudBC2RMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC2RMin.TabIndex = 7;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(50, 60);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(24, 13);
            this.label40.TabIndex = 6;
            this.label40.Text = "Min";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(50, 40);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(24, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "Min";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(50, 20);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(24, 13);
            this.label42.TabIndex = 4;
            this.label42.Text = "Min";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(7, 59);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 2;
            this.label43.Text = "Blue";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 40);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 13);
            this.label44.TabIndex = 1;
            this.label44.Text = "Green";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(7, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(27, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Red";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudHoverAlphaMax);
            this.groupBox1.Controls.Add(this.label114);
            this.groupBox1.Controls.Add(this.nudHoverAlphaMin);
            this.groupBox1.Controls.Add(this.label115);
            this.groupBox1.Controls.Add(this.label116);
            this.groupBox1.Controls.Add(this.nudHoverBlueMax);
            this.groupBox1.Controls.Add(this.nudHoverGreenMax);
            this.groupBox1.Controls.Add(this.nudHoverRedMax);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.nudHoverBlueMin);
            this.groupBox1.Controls.Add(this.nudHoverGreenMin);
            this.groupBox1.Controls.Add(this.nudHoverRedMin);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Location = new System.Drawing.Point(7, 126);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 100);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hover color range";
            // 
            // nudHoverAlphaMax
            // 
            this.nudHoverAlphaMax.Location = new System.Drawing.Point(185, 73);
            this.nudHoverAlphaMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverAlphaMax.Name = "nudHoverAlphaMax";
            this.nudHoverAlphaMax.Size = new System.Drawing.Size(66, 20);
            this.nudHoverAlphaMax.TabIndex = 20;
            this.nudHoverAlphaMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(152, 80);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(27, 13);
            this.label114.TabIndex = 19;
            this.label114.Text = "Max";
            // 
            // nudHoverAlphaMin
            // 
            this.nudHoverAlphaMin.Location = new System.Drawing.Point(83, 73);
            this.nudHoverAlphaMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverAlphaMin.Name = "nudHoverAlphaMin";
            this.nudHoverAlphaMin.Size = new System.Drawing.Size(66, 20);
            this.nudHoverAlphaMin.TabIndex = 18;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(50, 79);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(24, 13);
            this.label115.TabIndex = 17;
            this.label115.Text = "Min";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(7, 78);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(34, 13);
            this.label116.TabIndex = 16;
            this.label116.Text = "Alpha";
            // 
            // nudHoverBlueMax
            // 
            this.nudHoverBlueMax.Location = new System.Drawing.Point(185, 54);
            this.nudHoverBlueMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverBlueMax.Name = "nudHoverBlueMax";
            this.nudHoverBlueMax.Size = new System.Drawing.Size(66, 20);
            this.nudHoverBlueMax.TabIndex = 15;
            this.nudHoverBlueMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudHoverGreenMax
            // 
            this.nudHoverGreenMax.Location = new System.Drawing.Point(185, 37);
            this.nudHoverGreenMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverGreenMax.Name = "nudHoverGreenMax";
            this.nudHoverGreenMax.Size = new System.Drawing.Size(66, 20);
            this.nudHoverGreenMax.TabIndex = 14;
            this.nudHoverGreenMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudHoverRedMax
            // 
            this.nudHoverRedMax.Location = new System.Drawing.Point(185, 18);
            this.nudHoverRedMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverRedMax.Name = "nudHoverRedMax";
            this.nudHoverRedMax.Size = new System.Drawing.Size(66, 20);
            this.nudHoverRedMax.TabIndex = 13;
            this.nudHoverRedMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(152, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Max";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(152, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Max";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(152, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Max";
            // 
            // nudHoverBlueMin
            // 
            this.nudHoverBlueMin.Location = new System.Drawing.Point(83, 54);
            this.nudHoverBlueMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverBlueMin.Name = "nudHoverBlueMin";
            this.nudHoverBlueMin.Size = new System.Drawing.Size(66, 20);
            this.nudHoverBlueMin.TabIndex = 9;
            // 
            // nudHoverGreenMin
            // 
            this.nudHoverGreenMin.Location = new System.Drawing.Point(83, 37);
            this.nudHoverGreenMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverGreenMin.Name = "nudHoverGreenMin";
            this.nudHoverGreenMin.Size = new System.Drawing.Size(66, 20);
            this.nudHoverGreenMin.TabIndex = 8;
            // 
            // nudHoverRedMin
            // 
            this.nudHoverRedMin.Location = new System.Drawing.Point(83, 18);
            this.nudHoverRedMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHoverRedMin.Name = "nudHoverRedMin";
            this.nudHoverRedMin.Size = new System.Drawing.Size(66, 20);
            this.nudHoverRedMin.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(50, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Min";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(50, 40);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Min";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(50, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(24, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Min";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Blue";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Green";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Red";
            // 
            // grbC1
            // 
            this.grbC1.Controls.Add(this.nudBC1AMax);
            this.grbC1.Controls.Add(this.label120);
            this.grbC1.Controls.Add(this.nudBC1AMin);
            this.grbC1.Controls.Add(this.label121);
            this.grbC1.Controls.Add(this.label122);
            this.grbC1.Controls.Add(this.nudBC1BMax);
            this.grbC1.Controls.Add(this.nudBC1GMax);
            this.grbC1.Controls.Add(this.nudBC1RMax);
            this.grbC1.Controls.Add(this.label46);
            this.grbC1.Controls.Add(this.label47);
            this.grbC1.Controls.Add(this.label48);
            this.grbC1.Controls.Add(this.nudBC1BMin);
            this.grbC1.Controls.Add(this.nudBC1GMin);
            this.grbC1.Controls.Add(this.nudBC1RMin);
            this.grbC1.Controls.Add(this.label49);
            this.grbC1.Controls.Add(this.label50);
            this.grbC1.Controls.Add(this.label51);
            this.grbC1.Controls.Add(this.label52);
            this.grbC1.Controls.Add(this.label53);
            this.grbC1.Controls.Add(this.label54);
            this.grbC1.Location = new System.Drawing.Point(275, 20);
            this.grbC1.Name = "grbC1";
            this.grbC1.Size = new System.Drawing.Size(262, 100);
            this.grbC1.TabIndex = 18;
            this.grbC1.TabStop = false;
            this.grbC1.Text = "Border color 1 range";
            // 
            // nudBC1AMax
            // 
            this.nudBC1AMax.Location = new System.Drawing.Point(185, 73);
            this.nudBC1AMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1AMax.Name = "nudBC1AMax";
            this.nudBC1AMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC1AMax.TabIndex = 20;
            this.nudBC1AMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(152, 79);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(27, 13);
            this.label120.TabIndex = 19;
            this.label120.Text = "Max";
            // 
            // nudBC1AMin
            // 
            this.nudBC1AMin.Location = new System.Drawing.Point(83, 73);
            this.nudBC1AMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1AMin.Name = "nudBC1AMin";
            this.nudBC1AMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC1AMin.TabIndex = 18;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(50, 79);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(24, 13);
            this.label121.TabIndex = 17;
            this.label121.Text = "Min";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(7, 78);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(34, 13);
            this.label122.TabIndex = 16;
            this.label122.Text = "Alpha";
            // 
            // nudBC1BMax
            // 
            this.nudBC1BMax.Location = new System.Drawing.Point(185, 54);
            this.nudBC1BMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1BMax.Name = "nudBC1BMax";
            this.nudBC1BMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC1BMax.TabIndex = 15;
            this.nudBC1BMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC1GMax
            // 
            this.nudBC1GMax.Location = new System.Drawing.Point(185, 37);
            this.nudBC1GMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1GMax.Name = "nudBC1GMax";
            this.nudBC1GMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC1GMax.TabIndex = 14;
            this.nudBC1GMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBC1RMax
            // 
            this.nudBC1RMax.Location = new System.Drawing.Point(185, 18);
            this.nudBC1RMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1RMax.Name = "nudBC1RMax";
            this.nudBC1RMax.Size = new System.Drawing.Size(66, 20);
            this.nudBC1RMax.TabIndex = 13;
            this.nudBC1RMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(152, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(27, 13);
            this.label46.TabIndex = 12;
            this.label46.Text = "Max";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(152, 40);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(27, 13);
            this.label47.TabIndex = 11;
            this.label47.Text = "Max";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(152, 20);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(27, 13);
            this.label48.TabIndex = 10;
            this.label48.Text = "Max";
            // 
            // nudBC1BMin
            // 
            this.nudBC1BMin.Location = new System.Drawing.Point(83, 54);
            this.nudBC1BMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1BMin.Name = "nudBC1BMin";
            this.nudBC1BMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC1BMin.TabIndex = 9;
            // 
            // nudBC1GMin
            // 
            this.nudBC1GMin.Location = new System.Drawing.Point(83, 37);
            this.nudBC1GMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1GMin.Name = "nudBC1GMin";
            this.nudBC1GMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC1GMin.TabIndex = 8;
            // 
            // nudBC1RMin
            // 
            this.nudBC1RMin.Location = new System.Drawing.Point(83, 18);
            this.nudBC1RMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBC1RMin.Name = "nudBC1RMin";
            this.nudBC1RMin.Size = new System.Drawing.Size(66, 20);
            this.nudBC1RMin.TabIndex = 7;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(50, 60);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(24, 13);
            this.label49.TabIndex = 6;
            this.label49.Text = "Min";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(50, 40);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(24, 13);
            this.label50.TabIndex = 5;
            this.label50.Text = "Min";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(50, 20);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(24, 13);
            this.label51.TabIndex = 4;
            this.label51.Text = "Min";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(7, 59);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(28, 13);
            this.label52.TabIndex = 2;
            this.label52.Text = "Blue";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(7, 40);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(36, 13);
            this.label53.TabIndex = 1;
            this.label53.Text = "Green";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(7, 20);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(27, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Red";
            // 
            // grbBGColor
            // 
            this.grbBGColor.Controls.Add(this.nudBGAlphaMax);
            this.grbBGColor.Controls.Add(this.label111);
            this.grbBGColor.Controls.Add(this.nudBGAlphaMin);
            this.grbBGColor.Controls.Add(this.label112);
            this.grbBGColor.Controls.Add(this.label113);
            this.grbBGColor.Controls.Add(this.nudBGBlueMax);
            this.grbBGColor.Controls.Add(this.nudBGGreenMax);
            this.grbBGColor.Controls.Add(this.nudBGRedMax);
            this.grbBGColor.Controls.Add(this.label7);
            this.grbBGColor.Controls.Add(this.label8);
            this.grbBGColor.Controls.Add(this.label9);
            this.grbBGColor.Controls.Add(this.nudBGBlueMin);
            this.grbBGColor.Controls.Add(this.nudBGGreenMin);
            this.grbBGColor.Controls.Add(this.nudBGRedMin);
            this.grbBGColor.Controls.Add(this.label6);
            this.grbBGColor.Controls.Add(this.label4);
            this.grbBGColor.Controls.Add(this.label5);
            this.grbBGColor.Controls.Add(this.label3);
            this.grbBGColor.Controls.Add(this.label2);
            this.grbBGColor.Controls.Add(this.label1);
            this.grbBGColor.Location = new System.Drawing.Point(7, 20);
            this.grbBGColor.Name = "grbBGColor";
            this.grbBGColor.Size = new System.Drawing.Size(262, 100);
            this.grbBGColor.TabIndex = 0;
            this.grbBGColor.TabStop = false;
            this.grbBGColor.Text = "Background color range";
            // 
            // nudBGAlphaMax
            // 
            this.nudBGAlphaMax.Location = new System.Drawing.Point(185, 73);
            this.nudBGAlphaMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGAlphaMax.Name = "nudBGAlphaMax";
            this.nudBGAlphaMax.Size = new System.Drawing.Size(66, 20);
            this.nudBGAlphaMax.TabIndex = 20;
            this.nudBGAlphaMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(152, 79);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(27, 13);
            this.label111.TabIndex = 19;
            this.label111.Text = "Max";
            // 
            // nudBGAlphaMin
            // 
            this.nudBGAlphaMin.Location = new System.Drawing.Point(83, 73);
            this.nudBGAlphaMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGAlphaMin.Name = "nudBGAlphaMin";
            this.nudBGAlphaMin.Size = new System.Drawing.Size(66, 20);
            this.nudBGAlphaMin.TabIndex = 18;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(50, 79);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(24, 13);
            this.label112.TabIndex = 17;
            this.label112.Text = "Min";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(7, 78);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(34, 13);
            this.label113.TabIndex = 16;
            this.label113.Text = "Alpha";
            // 
            // nudBGBlueMax
            // 
            this.nudBGBlueMax.Location = new System.Drawing.Point(185, 54);
            this.nudBGBlueMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGBlueMax.Name = "nudBGBlueMax";
            this.nudBGBlueMax.Size = new System.Drawing.Size(66, 20);
            this.nudBGBlueMax.TabIndex = 15;
            this.nudBGBlueMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBGGreenMax
            // 
            this.nudBGGreenMax.Location = new System.Drawing.Point(185, 37);
            this.nudBGGreenMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGGreenMax.Name = "nudBGGreenMax";
            this.nudBGGreenMax.Size = new System.Drawing.Size(66, 20);
            this.nudBGGreenMax.TabIndex = 14;
            this.nudBGGreenMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // nudBGRedMax
            // 
            this.nudBGRedMax.Location = new System.Drawing.Point(185, 18);
            this.nudBGRedMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGRedMax.Name = "nudBGRedMax";
            this.nudBGRedMax.Size = new System.Drawing.Size(66, 20);
            this.nudBGRedMax.TabIndex = 13;
            this.nudBGRedMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(152, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Max";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(152, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Max";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(152, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Max";
            // 
            // nudBGBlueMin
            // 
            this.nudBGBlueMin.Location = new System.Drawing.Point(83, 54);
            this.nudBGBlueMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGBlueMin.Name = "nudBGBlueMin";
            this.nudBGBlueMin.Size = new System.Drawing.Size(66, 20);
            this.nudBGBlueMin.TabIndex = 9;
            // 
            // nudBGGreenMin
            // 
            this.nudBGGreenMin.Location = new System.Drawing.Point(83, 37);
            this.nudBGGreenMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGGreenMin.Name = "nudBGGreenMin";
            this.nudBGGreenMin.Size = new System.Drawing.Size(66, 20);
            this.nudBGGreenMin.TabIndex = 8;
            // 
            // nudBGRedMin
            // 
            this.nudBGRedMin.Location = new System.Drawing.Point(83, 18);
            this.nudBGRedMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBGRedMin.Name = "nudBGRedMin";
            this.nudBGRedMin.Size = new System.Drawing.Size(66, 20);
            this.nudBGRedMin.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Min";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Min";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Min";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Blue";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Green";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Red";
            // 
            // InitialConditionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 741);
            this.Controls.Add(this.pnlMain);
            this.Name = "InitialConditionsForm";
            this.Text = "InitialConditionsForm";
            this.pnlMain.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSzMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSzMin)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPad1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqPadd)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadBlurMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadVMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadHMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadBlurMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadVMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadHMin)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBr1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBRad)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW4Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW3Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW2Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW1Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW4Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW3Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW2Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBW1Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBWidths)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBStyles)).EndInit();
            this.grbColors.ResumeLayout(false);
            this.grbColors.PerformLayout();
            this.grbGradient.ResumeLayout(false);
            this.grbGradient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradAlphaMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradAlphaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradBlueMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradGreenMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradRedMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradBlueMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradGreenMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGradRedMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudUniqBColors)).EndInit();
            this.grbC4.ResumeLayout(false);
            this.grbC4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4AMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4AMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4BMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4GMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4RMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4BMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4GMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC4RMin)).EndInit();
            this.grbC3.ResumeLayout(false);
            this.grbC3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3AMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3AMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3BMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3GMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3RMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3BMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3GMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC3RMin)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowAlphaMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowAlphaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowBlueMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowGreenMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowRedMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowBlueMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowGreenMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShadowRedMin)).EndInit();
            this.grbC2.ResumeLayout(false);
            this.grbC2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2AMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2AMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2BMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2GMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2RMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2BMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2GMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC2RMin)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverAlphaMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverAlphaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverBlueMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverGreenMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverRedMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverBlueMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverGreenMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoverRedMin)).EndInit();
            this.grbC1.ResumeLayout(false);
            this.grbC1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1AMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1AMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1BMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1GMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1RMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1BMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1GMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBC1RMin)).EndInit();
            this.grbBGColor.ResumeLayout(false);
            this.grbBGColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGAlphaMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGAlphaMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGBlueMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGGreenMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGRedMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGBlueMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGGreenMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBGRedMin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.GroupBox grbColors;
        private System.Windows.Forms.GroupBox grbBGColor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudBGBlueMax;
        private System.Windows.Forms.NumericUpDown nudBGGreenMax;
        private System.Windows.Forms.NumericUpDown nudBGRedMax;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudBGBlueMin;
        private System.Windows.Forms.NumericUpDown nudBGGreenMin;
        private System.Windows.Forms.NumericUpDown nudBGRedMin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudHoverBlueMax;
        private System.Windows.Forms.NumericUpDown nudHoverGreenMax;
        private System.Windows.Forms.NumericUpDown nudHoverRedMax;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudHoverBlueMin;
        private System.Windows.Forms.NumericUpDown nudHoverGreenMin;
        private System.Windows.Forms.NumericUpDown nudHoverRedMin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nudShadowBlueMax;
        private System.Windows.Forms.NumericUpDown nudShadowGreenMax;
        private System.Windows.Forms.NumericUpDown nudShadowRedMax;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nudShadowBlueMin;
        private System.Windows.Forms.NumericUpDown nudShadowGreenMin;
        private System.Windows.Forms.NumericUpDown nudShadowRedMin;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox grbC4;
        private System.Windows.Forms.NumericUpDown nudBC4BMax;
        private System.Windows.Forms.NumericUpDown nudBC4GMax;
        private System.Windows.Forms.NumericUpDown nudBC4RMax;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.NumericUpDown nudBC4BMin;
        private System.Windows.Forms.NumericUpDown nudBC4GMin;
        private System.Windows.Forms.NumericUpDown nudBC4RMin;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.GroupBox grbC3;
        private System.Windows.Forms.NumericUpDown nudBC3BMax;
        private System.Windows.Forms.NumericUpDown nudBC3GMax;
        private System.Windows.Forms.NumericUpDown nudBC3RMax;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.NumericUpDown nudBC3BMin;
        private System.Windows.Forms.NumericUpDown nudBC3GMin;
        private System.Windows.Forms.NumericUpDown nudBC3RMin;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox grbC2;
        private System.Windows.Forms.NumericUpDown nudBC2BMax;
        private System.Windows.Forms.NumericUpDown nudBC2GMax;
        private System.Windows.Forms.NumericUpDown nudBC2RMax;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.NumericUpDown nudBC2BMin;
        private System.Windows.Forms.NumericUpDown nudBC2GMin;
        private System.Windows.Forms.NumericUpDown nudBC2RMin;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox grbC1;
        private System.Windows.Forms.NumericUpDown nudBC1BMax;
        private System.Windows.Forms.NumericUpDown nudBC1GMax;
        private System.Windows.Forms.NumericUpDown nudBC1RMax;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown nudBC1BMin;
        private System.Windows.Forms.NumericUpDown nudBC1GMin;
        private System.Windows.Forms.NumericUpDown nudBC1RMin;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown nudUniqBColors;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox cmbBStyle1Max;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox cmbBStyle1Min;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.NumericUpDown nudUniqBStyles;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox cmbBStyle4Max;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ComboBox cmbBStyle4Min;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.ComboBox cmbBStyle3Max;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.ComboBox cmbBStyle3Min;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ComboBox cmbBStyle2Max;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ComboBox cmbBStyle2Min;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.NumericUpDown nudBW1Min;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.NumericUpDown nudUniqBWidths;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.NumericUpDown nudBW3Max;
        private System.Windows.Forms.NumericUpDown nudBW2Max;
        private System.Windows.Forms.NumericUpDown nudBW1Max;
        private System.Windows.Forms.NumericUpDown nudBW4Min;
        private System.Windows.Forms.NumericUpDown nudBW3Min;
        private System.Windows.Forms.NumericUpDown nudBW2Min;
        private System.Windows.Forms.NumericUpDown nudBW4Max;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.NumericUpDown nudBr4Max;
        private System.Windows.Forms.NumericUpDown nudBr3Max;
        private System.Windows.Forms.NumericUpDown nudBr2Max;
        private System.Windows.Forms.NumericUpDown nudBr1Max;
        private System.Windows.Forms.NumericUpDown nudBr4Min;
        private System.Windows.Forms.NumericUpDown nudBr3Min;
        private System.Windows.Forms.NumericUpDown nudBr2Min;
        private System.Windows.Forms.NumericUpDown nudBr1Min;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.NumericUpDown nudUniqBRad;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.NumericUpDown nudShadBlurMax;
        private System.Windows.Forms.NumericUpDown nudShadVMax;
        private System.Windows.Forms.NumericUpDown nudShadHMax;
        private System.Windows.Forms.NumericUpDown nudShadBlurMin;
        private System.Windows.Forms.NumericUpDown nudShadVMin;
        private System.Windows.Forms.NumericUpDown nudShadHMin;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.NumericUpDown nudPad4Max;
        private System.Windows.Forms.NumericUpDown nudPad3Max;
        private System.Windows.Forms.NumericUpDown nudPad2Max;
        private System.Windows.Forms.NumericUpDown nudPad1Max;
        private System.Windows.Forms.NumericUpDown nudPad4Min;
        private System.Windows.Forms.NumericUpDown nudPad3Min;
        private System.Windows.Forms.NumericUpDown nudPad2Min;
        private System.Windows.Forms.NumericUpDown nudPad1Min;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.NumericUpDown nudUniqPadd;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox cmbFontFamMax;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.ComboBox cmbFontFamMin;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.NumericUpDown nudFontSzMax;
        private System.Windows.Forms.NumericUpDown nudFontSzMin;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.NumericUpDown nudShadowAlphaMax;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.NumericUpDown nudShadowAlphaMin;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.NumericUpDown nudHoverAlphaMax;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.NumericUpDown nudHoverAlphaMin;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.NumericUpDown nudBGAlphaMax;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.NumericUpDown nudBGAlphaMin;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.NumericUpDown nudBC3AMax;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.NumericUpDown nudBC3AMin;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.NumericUpDown nudBC2AMax;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.NumericUpDown nudBC2AMin;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.NumericUpDown nudBC1AMax;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.NumericUpDown nudBC1AMin;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.NumericUpDown nudBC4AMax;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.NumericUpDown nudBC4AMin;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.CheckBox chkGradient;
        private System.Windows.Forms.GroupBox grbGradient;
        private System.Windows.Forms.NumericUpDown nudGradAlphaMax;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.NumericUpDown nudGradAlphaMin;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.NumericUpDown nudGradBlueMax;
        private System.Windows.Forms.NumericUpDown nudGradGreenMax;
        private System.Windows.Forms.NumericUpDown nudGradRedMax;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.NumericUpDown nudGradBlueMin;
        private System.Windows.Forms.NumericUpDown nudGradGreenMin;
        private System.Windows.Forms.NumericUpDown nudGradRedMin;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.CheckBox chkTransparent;
    }
}