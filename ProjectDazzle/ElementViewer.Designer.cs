﻿namespace ProjectDazzle
{
    partial class ElementViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ElementViewer));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tlpInterface = new System.Windows.Forms.TableLayoutPanel();
            this.grbElements = new System.Windows.Forms.GroupBox();
            this.btnSetText = new System.Windows.Forms.Button();
            this.rbtBorder = new System.Windows.Forms.RadioButton();
            this.rbtParagraph = new System.Windows.Forms.RadioButton();
            this.rbtTable = new System.Windows.Forms.RadioButton();
            this.rbtInput = new System.Windows.Forms.RadioButton();
            this.rbtButton = new System.Windows.Forms.RadioButton();
            this.grbAlgoProp = new System.Windows.Forms.GroupBox();
            this.btnOpenTermCond = new System.Windows.Forms.Button();
            this.chkRandom = new System.Windows.Forms.CheckBox();
            this.grbAlgoType = new System.Windows.Forms.GroupBox();
            this.rbtRWGA = new System.Windows.Forms.RadioButton();
            this.rbtStandardGA = new System.Windows.Forms.RadioButton();
            this.btnCrossover = new System.Windows.Forms.Button();
            this.chkMultiSelect = new System.Windows.Forms.CheckBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.grbStatus = new System.Windows.Forms.GroupBox();
            this.lblPopFit = new System.Windows.Forms.Label();
            this.lblGeneration = new System.Windows.Forms.Label();
            this.grbCrossover = new System.Windows.Forms.GroupBox();
            this.rbtPropLock = new System.Windows.Forms.RadioButton();
            this.rbtArithmetic = new System.Windows.Forms.RadioButton();
            this.rbtMultiP = new System.Windows.Forms.RadioButton();
            this.rbtSingleP = new System.Windows.Forms.RadioButton();
            this.grbSelection = new System.Windows.Forms.GroupBox();
            this.rbtRoulette = new System.Windows.Forms.RadioButton();
            this.rbtElitist = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRunFitness = new System.Windows.Forms.Button();
            this.mnsMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCSSfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDevConsoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetVoteDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeBackgroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiWhite = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiBlack = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiAlpha = new System.Windows.Forms.ToolStripMenuItem();
            this.setInitalConditionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lockParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toggleHoverstateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterPreviewModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblPopVotes = new System.Windows.Forms.Label();
            this.tlpMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tlpInterface.SuspendLayout();
            this.grbElements.SuspendLayout();
            this.grbAlgoProp.SuspendLayout();
            this.grbAlgoType.SuspendLayout();
            this.grbStatus.SuspendLayout();
            this.grbCrossover.SuspendLayout();
            this.grbSelection.SuspendLayout();
            this.mnsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            resources.ApplyResources(this.tlpMain, "tlpMain");
            this.tlpMain.Controls.Add(this.panel1, 1, 1);
            this.tlpMain.Controls.Add(this.mnsMain, 0, 0);
            this.tlpMain.Name = "tlpMain";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tlpInterface);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // tlpInterface
            // 
            resources.ApplyResources(this.tlpInterface, "tlpInterface");
            this.tlpInterface.Controls.Add(this.grbElements, 0, 0);
            this.tlpInterface.Controls.Add(this.grbAlgoProp, 0, 1);
            this.tlpInterface.Name = "tlpInterface";
            // 
            // grbElements
            // 
            this.grbElements.Controls.Add(this.btnSetText);
            this.grbElements.Controls.Add(this.rbtBorder);
            this.grbElements.Controls.Add(this.rbtParagraph);
            this.grbElements.Controls.Add(this.rbtTable);
            this.grbElements.Controls.Add(this.rbtInput);
            this.grbElements.Controls.Add(this.rbtButton);
            resources.ApplyResources(this.grbElements, "grbElements");
            this.grbElements.Name = "grbElements";
            this.grbElements.TabStop = false;
            // 
            // btnSetText
            // 
            resources.ApplyResources(this.btnSetText, "btnSetText");
            this.btnSetText.Name = "btnSetText";
            this.btnSetText.UseVisualStyleBackColor = true;
            this.btnSetText.Click += new System.EventHandler(this.btnSetText_Click);
            // 
            // rbtBorder
            // 
            resources.ApplyResources(this.rbtBorder, "rbtBorder");
            this.rbtBorder.Name = "rbtBorder";
            this.rbtBorder.TabStop = true;
            this.rbtBorder.UseVisualStyleBackColor = true;
            this.rbtBorder.Click += new System.EventHandler(this.rbtBorder_Click);
            // 
            // rbtParagraph
            // 
            resources.ApplyResources(this.rbtParagraph, "rbtParagraph");
            this.rbtParagraph.Name = "rbtParagraph";
            this.rbtParagraph.TabStop = true;
            this.rbtParagraph.UseVisualStyleBackColor = true;
            this.rbtParagraph.Click += new System.EventHandler(this.rbtParagraph_Click);
            // 
            // rbtTable
            // 
            resources.ApplyResources(this.rbtTable, "rbtTable");
            this.rbtTable.Name = "rbtTable";
            this.rbtTable.TabStop = true;
            this.rbtTable.UseVisualStyleBackColor = true;
            this.rbtTable.Click += new System.EventHandler(this.rbtTable_Click);
            // 
            // rbtInput
            // 
            resources.ApplyResources(this.rbtInput, "rbtInput");
            this.rbtInput.Name = "rbtInput";
            this.rbtInput.UseVisualStyleBackColor = true;
            this.rbtInput.Click += new System.EventHandler(this.rbtInput_Click);
            // 
            // rbtButton
            // 
            resources.ApplyResources(this.rbtButton, "rbtButton");
            this.rbtButton.Checked = true;
            this.rbtButton.Name = "rbtButton";
            this.rbtButton.TabStop = true;
            this.rbtButton.UseVisualStyleBackColor = true;
            this.rbtButton.Click += new System.EventHandler(this.rbtButton_Click);
            // 
            // grbAlgoProp
            // 
            this.grbAlgoProp.Controls.Add(this.btnOpenTermCond);
            this.grbAlgoProp.Controls.Add(this.chkRandom);
            this.grbAlgoProp.Controls.Add(this.grbAlgoType);
            this.grbAlgoProp.Controls.Add(this.btnCrossover);
            this.grbAlgoProp.Controls.Add(this.chkMultiSelect);
            this.grbAlgoProp.Controls.Add(this.btnClear);
            this.grbAlgoProp.Controls.Add(this.grbStatus);
            this.grbAlgoProp.Controls.Add(this.grbCrossover);
            this.grbAlgoProp.Controls.Add(this.grbSelection);
            this.grbAlgoProp.Controls.Add(this.btnSave);
            this.grbAlgoProp.Controls.Add(this.btnExport);
            this.grbAlgoProp.Controls.Add(this.btnReset);
            this.grbAlgoProp.Controls.Add(this.btnRunFitness);
            resources.ApplyResources(this.grbAlgoProp, "grbAlgoProp");
            this.grbAlgoProp.Name = "grbAlgoProp";
            this.grbAlgoProp.TabStop = false;
            // 
            // btnOpenTermCond
            // 
            resources.ApplyResources(this.btnOpenTermCond, "btnOpenTermCond");
            this.btnOpenTermCond.Name = "btnOpenTermCond";
            this.btnOpenTermCond.UseVisualStyleBackColor = true;
            this.btnOpenTermCond.Click += new System.EventHandler(this.btnOpenTermCond_Click);
            // 
            // chkRandom
            // 
            resources.ApplyResources(this.chkRandom, "chkRandom");
            this.chkRandom.Checked = true;
            this.chkRandom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRandom.Name = "chkRandom";
            this.chkRandom.UseVisualStyleBackColor = true;
            // 
            // grbAlgoType
            // 
            this.grbAlgoType.Controls.Add(this.rbtRWGA);
            this.grbAlgoType.Controls.Add(this.rbtStandardGA);
            resources.ApplyResources(this.grbAlgoType, "grbAlgoType");
            this.grbAlgoType.Name = "grbAlgoType";
            this.grbAlgoType.TabStop = false;
            // 
            // rbtRWGA
            // 
            resources.ApplyResources(this.rbtRWGA, "rbtRWGA");
            this.rbtRWGA.Name = "rbtRWGA";
            this.rbtRWGA.UseVisualStyleBackColor = true;
            this.rbtRWGA.Click += new System.EventHandler(this.rbtRWGA_Click);
            // 
            // rbtStandardGA
            // 
            resources.ApplyResources(this.rbtStandardGA, "rbtStandardGA");
            this.rbtStandardGA.Checked = true;
            this.rbtStandardGA.Name = "rbtStandardGA";
            this.rbtStandardGA.TabStop = true;
            this.rbtStandardGA.UseVisualStyleBackColor = true;
            this.rbtStandardGA.Click += new System.EventHandler(this.rbtStandardGA_Click);
            // 
            // btnCrossover
            // 
            resources.ApplyResources(this.btnCrossover, "btnCrossover");
            this.btnCrossover.Name = "btnCrossover";
            this.btnCrossover.UseVisualStyleBackColor = true;
            this.btnCrossover.Click += new System.EventHandler(this.btnCrossover_Click);
            // 
            // chkMultiSelect
            // 
            resources.ApplyResources(this.chkMultiSelect, "chkMultiSelect");
            this.chkMultiSelect.Name = "chkMultiSelect";
            this.chkMultiSelect.UseVisualStyleBackColor = true;
            this.chkMultiSelect.CheckedChanged += new System.EventHandler(this.chkMultiSelect_CheckedChanged);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // grbStatus
            // 
            this.grbStatus.Controls.Add(this.lblPopVotes);
            this.grbStatus.Controls.Add(this.lblPopFit);
            this.grbStatus.Controls.Add(this.lblGeneration);
            resources.ApplyResources(this.grbStatus, "grbStatus");
            this.grbStatus.Name = "grbStatus";
            this.grbStatus.TabStop = false;
            // 
            // lblPopFit
            // 
            resources.ApplyResources(this.lblPopFit, "lblPopFit");
            this.lblPopFit.Name = "lblPopFit";
            // 
            // lblGeneration
            // 
            resources.ApplyResources(this.lblGeneration, "lblGeneration");
            this.lblGeneration.Name = "lblGeneration";
            // 
            // grbCrossover
            // 
            this.grbCrossover.Controls.Add(this.rbtPropLock);
            this.grbCrossover.Controls.Add(this.rbtArithmetic);
            this.grbCrossover.Controls.Add(this.rbtMultiP);
            this.grbCrossover.Controls.Add(this.rbtSingleP);
            resources.ApplyResources(this.grbCrossover, "grbCrossover");
            this.grbCrossover.Name = "grbCrossover";
            this.grbCrossover.TabStop = false;
            // 
            // rbtPropLock
            // 
            resources.ApplyResources(this.rbtPropLock, "rbtPropLock");
            this.rbtPropLock.Name = "rbtPropLock";
            this.rbtPropLock.TabStop = true;
            this.rbtPropLock.UseVisualStyleBackColor = true;
            // 
            // rbtArithmetic
            // 
            resources.ApplyResources(this.rbtArithmetic, "rbtArithmetic");
            this.rbtArithmetic.Name = "rbtArithmetic";
            this.rbtArithmetic.UseVisualStyleBackColor = true;
            this.rbtArithmetic.Click += new System.EventHandler(this.rbtArithmetic_Click);
            // 
            // rbtMultiP
            // 
            resources.ApplyResources(this.rbtMultiP, "rbtMultiP");
            this.rbtMultiP.Name = "rbtMultiP";
            this.rbtMultiP.UseVisualStyleBackColor = true;
            this.rbtMultiP.Click += new System.EventHandler(this.rbtMultiP_Click);
            // 
            // rbtSingleP
            // 
            resources.ApplyResources(this.rbtSingleP, "rbtSingleP");
            this.rbtSingleP.Checked = true;
            this.rbtSingleP.Name = "rbtSingleP";
            this.rbtSingleP.TabStop = true;
            this.rbtSingleP.UseVisualStyleBackColor = true;
            this.rbtSingleP.Click += new System.EventHandler(this.rbtSingleP_Click);
            // 
            // grbSelection
            // 
            this.grbSelection.Controls.Add(this.rbtRoulette);
            this.grbSelection.Controls.Add(this.rbtElitist);
            resources.ApplyResources(this.grbSelection, "grbSelection");
            this.grbSelection.Name = "grbSelection";
            this.grbSelection.TabStop = false;
            // 
            // rbtRoulette
            // 
            resources.ApplyResources(this.rbtRoulette, "rbtRoulette");
            this.rbtRoulette.Name = "rbtRoulette";
            this.rbtRoulette.UseVisualStyleBackColor = true;
            this.rbtRoulette.Click += new System.EventHandler(this.rbtRoulette_Click);
            // 
            // rbtElitist
            // 
            resources.ApplyResources(this.rbtElitist, "rbtElitist");
            this.rbtElitist.Checked = true;
            this.rbtElitist.Name = "rbtElitist";
            this.rbtElitist.TabStop = true;
            this.rbtElitist.UseVisualStyleBackColor = true;
            this.rbtElitist.Click += new System.EventHandler(this.rbtElitist_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExport
            // 
            resources.ApplyResources(this.btnExport, "btnExport");
            this.btnExport.Name = "btnExport";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnReset
            // 
            resources.ApplyResources(this.btnReset, "btnReset");
            this.btnReset.Name = "btnReset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnRunFitness
            // 
            resources.ApplyResources(this.btnRunFitness, "btnRunFitness");
            this.btnRunFitness.Name = "btnRunFitness";
            this.btnRunFitness.UseVisualStyleBackColor = true;
            this.btnRunFitness.Click += new System.EventHandler(this.btnRunFitness_Click);
            // 
            // mnsMain
            // 
            resources.ApplyResources(this.mnsMain, "mnsMain");
            this.mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.mnsMain.Name = "mnsMain";
            this.mnsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCSSfilesToolStripMenuItem,
            this.openDevConsoleToolStripMenuItem,
            this.resetVoteDataToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // loadCSSfilesToolStripMenuItem
            // 
            this.loadCSSfilesToolStripMenuItem.Name = "loadCSSfilesToolStripMenuItem";
            resources.ApplyResources(this.loadCSSfilesToolStripMenuItem, "loadCSSfilesToolStripMenuItem");
            this.loadCSSfilesToolStripMenuItem.Click += new System.EventHandler(this.loadCSSfilesToolStripMenuItem_Click);
            // 
            // openDevConsoleToolStripMenuItem
            // 
            this.openDevConsoleToolStripMenuItem.Name = "openDevConsoleToolStripMenuItem";
            resources.ApplyResources(this.openDevConsoleToolStripMenuItem, "openDevConsoleToolStripMenuItem");
            this.openDevConsoleToolStripMenuItem.Click += new System.EventHandler(this.openDevConsoleToolStripMenuItem_Click);
            // 
            // resetVoteDataToolStripMenuItem
            // 
            this.resetVoteDataToolStripMenuItem.Name = "resetVoteDataToolStripMenuItem";
            resources.ApplyResources(this.resetVoteDataToolStripMenuItem, "resetVoteDataToolStripMenuItem");
            this.resetVoteDataToolStripMenuItem.Click += new System.EventHandler(this.resetVoteDataToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeBackgroundToolStripMenuItem,
            this.setInitalConditionsToolStripMenuItem,
            this.lockParametersToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            resources.ApplyResources(this.editToolStripMenuItem, "editToolStripMenuItem");
            // 
            // changeBackgroundToolStripMenuItem
            // 
            this.changeBackgroundToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmiWhite,
            this.tmiBlack,
            this.tmiAlpha});
            this.changeBackgroundToolStripMenuItem.Name = "changeBackgroundToolStripMenuItem";
            resources.ApplyResources(this.changeBackgroundToolStripMenuItem, "changeBackgroundToolStripMenuItem");
            // 
            // tmiWhite
            // 
            this.tmiWhite.Checked = true;
            this.tmiWhite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tmiWhite.Name = "tmiWhite";
            resources.ApplyResources(this.tmiWhite, "tmiWhite");
            this.tmiWhite.Click += new System.EventHandler(this.tmiWhite_Click);
            // 
            // tmiBlack
            // 
            this.tmiBlack.Name = "tmiBlack";
            resources.ApplyResources(this.tmiBlack, "tmiBlack");
            this.tmiBlack.Click += new System.EventHandler(this.tmiBlack_Click);
            // 
            // tmiAlpha
            // 
            this.tmiAlpha.Name = "tmiAlpha";
            resources.ApplyResources(this.tmiAlpha, "tmiAlpha");
            this.tmiAlpha.Click += new System.EventHandler(this.tmiAlpha_Click);
            // 
            // setInitalConditionsToolStripMenuItem
            // 
            this.setInitalConditionsToolStripMenuItem.Name = "setInitalConditionsToolStripMenuItem";
            resources.ApplyResources(this.setInitalConditionsToolStripMenuItem, "setInitalConditionsToolStripMenuItem");
            this.setInitalConditionsToolStripMenuItem.Click += new System.EventHandler(this.setInitalConditionsToolStripMenuItem_Click);
            // 
            // lockParametersToolStripMenuItem
            // 
            this.lockParametersToolStripMenuItem.Name = "lockParametersToolStripMenuItem";
            resources.ApplyResources(this.lockParametersToolStripMenuItem, "lockParametersToolStripMenuItem");
            this.lockParametersToolStripMenuItem.Click += new System.EventHandler(this.lockParametersToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toggleHoverstateToolStripMenuItem,
            this.enterPreviewModeToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            resources.ApplyResources(this.viewToolStripMenuItem, "viewToolStripMenuItem");
            // 
            // toggleHoverstateToolStripMenuItem
            // 
            this.toggleHoverstateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.offToolStripMenuItem,
            this.onToolStripMenuItem});
            this.toggleHoverstateToolStripMenuItem.Name = "toggleHoverstateToolStripMenuItem";
            resources.ApplyResources(this.toggleHoverstateToolStripMenuItem, "toggleHoverstateToolStripMenuItem");
            // 
            // offToolStripMenuItem
            // 
            this.offToolStripMenuItem.Checked = true;
            this.offToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.offToolStripMenuItem.Name = "offToolStripMenuItem";
            resources.ApplyResources(this.offToolStripMenuItem, "offToolStripMenuItem");
            this.offToolStripMenuItem.Click += new System.EventHandler(this.offToolStripMenuItem_Click);
            // 
            // onToolStripMenuItem
            // 
            this.onToolStripMenuItem.Name = "onToolStripMenuItem";
            resources.ApplyResources(this.onToolStripMenuItem, "onToolStripMenuItem");
            this.onToolStripMenuItem.Click += new System.EventHandler(this.onToolStripMenuItem_Click);
            // 
            // enterPreviewModeToolStripMenuItem
            // 
            this.enterPreviewModeToolStripMenuItem.Name = "enterPreviewModeToolStripMenuItem";
            resources.ApplyResources(this.enterPreviewModeToolStripMenuItem, "enterPreviewModeToolStripMenuItem");
            this.enterPreviewModeToolStripMenuItem.Click += new System.EventHandler(this.enterPreviewModeToolStripMenuItem_Click);
            // 
            // lblPopVotes
            // 
            resources.ApplyResources(this.lblPopVotes, "lblPopVotes");
            this.lblPopVotes.Name = "lblPopVotes";
            // 
            // ElementViewer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.MainMenuStrip = this.mnsMain;
            this.Name = "ElementViewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ElementViewer_FormClosing);
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tlpInterface.ResumeLayout(false);
            this.grbElements.ResumeLayout(false);
            this.grbElements.PerformLayout();
            this.grbAlgoProp.ResumeLayout(false);
            this.grbAlgoProp.PerformLayout();
            this.grbAlgoType.ResumeLayout(false);
            this.grbAlgoType.PerformLayout();
            this.grbStatus.ResumeLayout(false);
            this.grbStatus.PerformLayout();
            this.grbCrossover.ResumeLayout(false);
            this.grbCrossover.PerformLayout();
            this.grbSelection.ResumeLayout(false);
            this.grbSelection.PerformLayout();
            this.mnsMain.ResumeLayout(false);
            this.mnsMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tlpInterface;
        private System.Windows.Forms.GroupBox grbElements;
        private System.Windows.Forms.RadioButton rbtBorder;
        private System.Windows.Forms.RadioButton rbtParagraph;
        private System.Windows.Forms.RadioButton rbtTable;
        private System.Windows.Forms.RadioButton rbtInput;
        private System.Windows.Forms.RadioButton rbtButton;
        private System.Windows.Forms.GroupBox grbAlgoProp;
        private System.Windows.Forms.Button btnOpenTermCond;
        private System.Windows.Forms.CheckBox chkRandom;
        private System.Windows.Forms.GroupBox grbAlgoType;
        private System.Windows.Forms.RadioButton rbtRWGA;
        private System.Windows.Forms.RadioButton rbtStandardGA;
        private System.Windows.Forms.Button btnCrossover;
        private System.Windows.Forms.CheckBox chkMultiSelect;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox grbStatus;
        private System.Windows.Forms.Label lblPopFit;
        private System.Windows.Forms.Label lblGeneration;
        private System.Windows.Forms.GroupBox grbCrossover;
        private System.Windows.Forms.RadioButton rbtArithmetic;
        private System.Windows.Forms.RadioButton rbtMultiP;
        private System.Windows.Forms.RadioButton rbtSingleP;
        private System.Windows.Forms.GroupBox grbSelection;
        private System.Windows.Forms.RadioButton rbtRoulette;
        private System.Windows.Forms.RadioButton rbtElitist;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRunFitness;
        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCSSfilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.Button btnSetText;
        private System.Windows.Forms.ToolStripMenuItem changeBackgroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tmiWhite;
        private System.Windows.Forms.ToolStripMenuItem tmiBlack;
        private System.Windows.Forms.ToolStripMenuItem tmiAlpha;
        private System.Windows.Forms.ToolStripMenuItem setInitalConditionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lockParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDevConsoleToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbtPropLock;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toggleHoverstateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem offToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterPreviewModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetVoteDataToolStripMenuItem;
        private System.Windows.Forms.Label lblPopVotes;
    }
}