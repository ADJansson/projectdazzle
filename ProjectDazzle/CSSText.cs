﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSText.cs - represents design of header text and content on web page. Extends the base chromosome
    /// with various HTML text related properties.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class CSSText : ChromosomeBase
    {
        // 0 - left, 1 - center, 2 - right, 3 - justify
        public int HeaderTextAlign
        {
            get
            {
                return _uniqueGenes.GetValue(0);
            }
        }

        public int TextAlign
        {
            get
            {
                return _uniqueGenes.GetValue(1);
            }
        }

        // 0 - none, 1 - overline, 2 - line-through, 3 - underline
        public int HeaderDecoration
        {
            get
            {
                return _uniqueGenes.GetValue(2);
            }
        }

        // 0 - none, 1 - uppercase, 2 - lowercase, 3 - capitalize
        public int HeaderTransform
        {
            get
            {
                return _uniqueGenes.GetValue(3);
            }
        }

        public int HeaderLetterSpacing
        {
            get
            {
                return _uniqueGenes.GetValue(4) - 5;
            }
        }

        // -5 -> +5
        public int LetterSpacing
        {
            get
            {
                return _uniqueGenes.GetValue(5) - 5;
            }
        }

        // 0.5 -> 2 X 10
        public double LineHeight
        {
            get
            {
                return Math.Round(_uniqueGenes.GetValue(6) / 15.0, 2);
            }
        }

        // -~5 -> +10
        public int HeaderWordSpacing
        {
            get
            {
                return _uniqueGenes.GetValue(7) - 5;
            }
        }

        public int WordSpacing
        {
            get
            {
                return _uniqueGenes.GetValue(8) - 5;
            }
        }

        public bool HeaderTextShadow
        {
            get
            {
                return _uniqueGenes.GetValue(9) == 1;
            }
        }

        public bool TextShadow
        {
            get
            {
                return _uniqueGenes.GetValue(10) == 1;
            }
        }

        public int HeaderFontFamilyID
        {
            get
            {
                return _uniqueGenes.GetValue(11);
            }
        }

        public int HeaderFontSize
        {
            get
            {
                return Math.Max(FontSize, _uniqueGenes.GetValue(12));
            }
        }

        public CSSText() : base(EDesignElement.Text)
        {
            Init();
        }

        public CSSText(int seed) : base(EDesignElement.Text, seed)
        {
            Init();
        }

        // Special constructor for other text-like elements (h1 etc.)
        public CSSText(EDesignElement overrideElement) : base(overrideElement)
        {
            Init();
        }

        public CSSText(EDesignElement overrideElement, int seed) : base(overrideElement, seed)
        {
            Init();
        }

        public CSSText(CSSText copy) : base(copy)
        {

        }

        private void Init()
        {
            _uniqueGenes.AddValue(_random.Next(0, 4), TL3); // Header text align
            _uniqueGenes.AddValue(_random.Next(0, 4), TL3); // Align
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Header text decoration
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Header text transformation
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Header letter spacing
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Letter spacing
            _uniqueGenes.AddValue(_random.Next(6, 32), TL31); // Line height
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Header word spacing
            _uniqueGenes.AddValue(_random.Next(0, 8), TL7); // Word spacing
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // Header text shadow
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // Text shadow
            _uniqueGenes.AddValue(_random.Next(0, 32), TL31); // Header font family ID
            _uniqueGenes.AddValue(_random.Next(8, 32), TL31); // Header font size

            MakeWeights();
        }

        public override ChromosomeBase Clone()
        {
            return new CSSText(this);
        }
    }
}
