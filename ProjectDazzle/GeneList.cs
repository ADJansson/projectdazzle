﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ProjectDazzle
{
    // Simple data structure to hold binary data and associated helper values
    [Serializable]
    public struct Gene
    {
        public string Binary; // The data
        public int Length; // The associated length of the binary string
        public int MaxValue; // Maximum value of property used for scaling
    }

    /// <summary>
    /// GeneList.cs - Extended container class to hold genes. Contains helper functions for converting to and from
    /// binary, handling lengths and displaying the full binary string.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class GeneList
    {
        private List<Gene> _geneList;

        public List<Gene> List
        {
            get
            {
                return _geneList;
            }
        }

        public virtual int Count
        {
            get
            {
                return _geneList.Count;
            }
        }

        public GeneList()
        {
            _geneList = new List<Gene>();
        }

        public GeneList(GeneList copy)
        {
            _geneList = copy._geneList.ToList();
        }

        public GeneList(List<Gene> geneList)
        {
            _geneList = geneList;
        }

        public void AddValue(int value, int binaryMaxLength)
        {
            // Max value is calculated based on the length of the binary string by setting the string to 111..1 and
            // converting to base 10.
            // Used to fill array with same value:
            // http://stackoverflow.com/questions/1897555/what-is-the-equivalent-of-memset-in-c (12/4-17)
            int f = Convert.ToInt32(new string(Enumerable.Repeat('1', binaryMaxLength).ToArray()), 2);
            AddValue(value, binaryMaxLength, f);
        }

        public void AddValue(int value, int binaryMaxLength, int maxValue)
        {
            if (maxValue < 0) // -1 is flag for generating maxValue in above function, if a value does not span the entire binary range
                AddValue(value, binaryMaxLength);
            else
                _geneList.Add(FixBinaryStringLength(binaryMaxLength, Convert.ToString(value, 2), maxValue));
        }

        public void AddColor(Color color)
        {
            AddValue(color.A, ChromosomeBase.TL255, 255);
            AddValue(color.R, ChromosomeBase.TL255, 255);
            AddValue(color.G, ChromosomeBase.TL255, 255);
            AddValue(color.B, ChromosomeBase.TL255, 255);
        }

        public void AddBinaryString(string bString)
        {
            _geneList.Add(new Gene { Binary = bString, Length = bString.Length, MaxValue = bString.Length });
        }

        public void AddGene(Gene gene)
        {
            _geneList.Add(gene);
        }

        public void AddBool(int i)
        {
            AddBool(i == 1);
        }

        public void AddBool(bool b)
        {
            AddValue(b ? 1 : 0, ChromosomeBase.TLBool, 1);
        }

        // Converts values from binary to base 10
        public virtual int GetValue(int index)
        {
            return Convert.ToInt32(_geneList[index].Binary, 2);
        }

        public void SetValue(int index, int value, int binaryMaxLength, int maxValue)
        {
            _geneList[index] = FixBinaryStringLength(binaryMaxLength, Convert.ToString(value, 2), maxValue);
        }

        public void SetGene(int index, Gene gene)
        {
            _geneList[index] = gene;
        }

        public string GetBinary(int index)
        {
            return _geneList[index].Binary;
        }

        public int GetLength(int index)
        {
            return _geneList[index].Length;
        }

        /// <summary>
        /// To normalize the values in the fitness function, we
        /// need to know the maximum value of each gene
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetMaxValue(int index)
        {
            return _geneList[index].MaxValue;
        }

        public Gene this[int i]
        {
            get
            {
                return _geneList[i];
            }
            set
            {
                _geneList[i] = value;
            }
        }

        /// <summary>
        /// Helper for adding leading 0's to binary string
        /// </summary>
        /// <param name="targetLength">Desired length of string</param>
        /// <param name="input">String to expand</param>
        /// <returns></returns>
        public Gene FixBinaryStringLength(int targetLength, string input, int maxValue)
        {
            int k = targetLength - input.Length;

            for (int i = 0; i < k; i++)
                input = "0" + input;

            return new Gene { Binary = input, Length = targetLength, MaxValue = maxValue };
        }

        public override string ToString()
        {
            string result = "";

            for (int i = 0; i < _geneList.Count; i++)
                result += _geneList[i].Binary;

            return result;
        }

        public static int GetValue(Gene gene)
        {
            return Convert.ToInt32(gene.Binary, 2);
        }
    }
}
