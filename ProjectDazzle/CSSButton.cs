﻿using System;
using System.Drawing;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSButton.cs - Represents a CSS button. Extends base chromosome.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class CSSButton : ChromosomeBase
    {
        public CSSButton() : base(EDesignElement.Button)
        {
            MakeWeights();
        }

        public CSSButton(int seed) : base(EDesignElement.Button, seed)
        {
            MakeWeights();
        }

        /// <summary>
        /// For unit testing only
        /// </summary>
        /// <param name="mockList"></param>
        public CSSButton(GeneList mockCommon, GeneList mockUnique)
        {
            _commonGenes = mockCommon;
            _uniqueGenes = mockUnique;

            MakeWeights();
        }

        public CSSButton(CSSButton copy) : base(copy)
        {

        }

        // Only buttons support parsing for now
        public CSSButton(ParseBundle bundle) : base(bundle)
        {

        }

        public CSSButton(InitialBundle bundle) : base(bundle)
        {

        }

        public override ChromosomeBase Clone()
        {
            return new CSSButton(this);
        }
    }
}
