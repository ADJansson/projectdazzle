﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ProjectDazzle
{
    public enum EDesignElement
    {
        Button, Border, Input, Text, Table,
    }

    /// <summary>
    /// HTMLHelper.cs - Static helper class to write the preview HTML-file used to display the population in the
    /// embedded web browser. To avoid hard-coding, most HTML-code is loaded and baked together to form the final
    /// file.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public static class HTMLHelper
    {
        private static List<string> _elements = new List<string>();
        private static string[] _fontFamily;
        private static string[] _borderStyles = new string[10];

        private static string _googleFontLink = "<link href='https://fonts.googleapis.com/css?family={0}' rel='stylesheet'>";
        private static string _htmlPath = $"{ AppDomain.CurrentDomain.BaseDirectory }\\HTML\\"; // Path to required files
        private static string _stylesPath = $"{_htmlPath}\\styles\\"; // Styles folder for generated css
        private static string _cellContent; // Table cell content string
        private static string _voteButtons; // Vote button strings

        public static string CustomHeading
        {
            get; set;
        } = "Preview";

        public static string CustomText
        {
            get; set;
        }

        public static void Init()
        {
            // Load the HTML-code for the various supported elements
            string allElements = System.IO.File.ReadAllText($"{_htmlPath}design_elements.txt");
            _elements = allElements.Split(';').ToList();

            // Load the default sample text
            CustomText = System.IO.File.ReadAllText($"{_htmlPath}sample_text.txt");

            // Load the list of links to the Google-fonts
            _fontFamily = System.IO.File.ReadAllLines($"{_htmlPath}googlefonts.txt");

            // Load the HTML code for cell content and vote buttons
            _cellContent = System.IO.File.ReadAllText($"{_htmlPath}cell_content.txt");
            _voteButtons = System.IO.File.ReadAllText($"{_htmlPath}vote_buttons.txt");

            // Initialize border styles
            _borderStyles[0] = "dotted";
            _borderStyles[1] = "dashed";
            _borderStyles[2] = "solid";
            _borderStyles[3] = "double";
            _borderStyles[4] = "groove";
            _borderStyles[5] = "ridge";
            _borderStyles[6] = "inset";
            _borderStyles[7] = "outset";
            _borderStyles[8] = "none";
            _borderStyles[9] = "hidden";
        }

        // Special function to create the HTML-file used when previewing parsed files
        public static void CreateParsedPreviewHtmlFile(int rowC)
        {
            WriteHtmlHelper(rowC, 1, EDesignElement.Button, "parsed_preview", "parsed",
                $"{_cellContent.Replace("{element}", GetDesignElementHTML(EDesignElement.Button)).Replace("{vote_buttons}", "<br>")}");
        }

        // Special function to create preview with classification data visible
        public static void CreatePreviewHtmlFileWithClassification(int rowC, int colC, EDesignElement tagName, List<ChromosomeBase> population)
        {
            WriteHtmlHelper(rowC, colC, tagName, "preview", "temp",
                $"{_cellContent.Replace("{element}", GetDesignElementHTML(tagName)).Replace("{vote_buttons}", _voteButtons)}", population);
        }
        // Creates default preview table with elements to design
        public static void CreatePreviewHtmlFile(int rowC, int colC, EDesignElement tagName, string fileName, string className)
        {
            WriteHtmlHelper(rowC, colC, tagName, fileName, className,
                $"{_cellContent.Replace("{element}", GetDesignElementHTML(tagName)).Replace("{vote_buttons}", _voteButtons)}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowC">Number of table rows</param>
        /// <param name="colC">Number of table columns</param>
        /// <param name="tagName">Name of the element</param>
        /// <param name="fileName">Name of output file</param>
        /// <param name="className">Name of associated CSS file</param>
        /// <param name="cellContent">HTML content string of table cell</param>
        /// <param name="population">The list of designs</param>
        private static void WriteHtmlHelper(int rowC, int colC, EDesignElement tagName, string fileName, string className, string cellContent, List<ChromosomeBase> population = null)
        {
            System.IO.Directory.CreateDirectory(_htmlPath);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter($"{_htmlPath}{fileName}.html"))
            {
                WriteHeaderStuff(file);
                file.WriteLine("\t<body>");
                file.WriteLine("\t\t<link href=\"styles/main.css\" rel=\"stylesheet\" type=\"text/css\"/>");
                file.WriteLine($"\t\t<link href=\"styles/{className}.css\" rel=\"stylesheet\" type=\"text/css\"/>");

                // Embed the Google font-links
                if (tagName == EDesignElement.Text)
                    for (int i = 0; i < _fontFamily.Count(); i++)
                        file.WriteLine(string.Format(_googleFontLink, _fontFamily[i]));

                file.WriteLine("\t\t<table id=\"previewTable\" class=\"preview\">");

                for (int i = 0, k = 0; i < rowC; i++)
                {
                    file.WriteLine("\t\t\t<tr>");
                    for (int j = 0; j < colC; j++, k++)
                    {
                        string predictedVote = "";

                        // If not population is provided, classification is not displayed
                        if (population != null)
                            predictedVote = $"{GetPredictedVoteString(population[k].Vote)} ({population[k].MatchPercent}%)";
                        else
                            predictedVote = "";

                        file.WriteLine(cellContent.Replace("{id}", $"{k}").Replace("{predicted_vote}", predictedVote));
                    }
                    file.WriteLine("\t\t\t</tr>");
                }
                file.WriteLine("\t\t</table>");

                file.WriteLine("\t\t<script src=\"tableClicker.js\"></script>");

                file.WriteLine("\t</body>");
                file.WriteLine("</html>");
            }
        }

        // Helper function to display classification on a more readable format
        private static string GetPredictedVoteString(EVote vote)
        {
            string result = "Predicted vote: ";
            switch (vote)
            {
                case EVote.Liked:
                    result += "Liked";
                    break;
                case EVote.Disliked:
                    result += "Disliked";
                    break;
                case EVote.Unrated:
                    result += "N/A";
                    break;
            }

            return result;
        }

        // Helper function to write required HTML5 data at top of file
        private static void WriteHeaderStuff(System.IO.StreamWriter file)
        {
            file.WriteLine("<!DOCTYPE html>");
            file.WriteLine("<html>");
            file.WriteLine("\t<head>");
            file.WriteLine("\t\t<meta charset = \"utf-8\"/>");
            file.WriteLine("\t\t<title>Genetic Algorithm Generated Web Page</title>");
            file.WriteLine("\t</head>");
        }

        // Writes the preview CSS-file based on population data
        public static void WriteTempCSSClassFile(List<ChromosomeBase> population)
        {
            System.IO.Directory.CreateDirectory(_stylesPath); 

            using (System.IO.StreamWriter file = new System.IO.StreamWriter($"{_stylesPath}temp.css"))
            {
                file.Write(CSSConstants.Meta);

                int i = 0;
                foreach (var c in population)
                {
                    file.Write(ExtractCSSProperties($".temp{i++}", c));
                }
            }
        }

        // Writes the parsed CSS data to file to be used in parsed preview window
        public static void WriteParsedElementCSS(List<ChromosomeBase> population)
        {
            System.IO.Directory.CreateDirectory(_stylesPath);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter($"{_stylesPath}parsed.css"))
            {
                file.Write(CSSConstants.Meta);

                int i = 0;
                foreach (var c in population)
                {
                    file.Write(ExtractCSSProperties($".temp{i++}", c));
                }
            }
        }

        // Write CSS-file with designs exported by user
        public static void WriteFinalCSSClassFile(List<ChromosomeBase> queue)
        {
            System.IO.Directory.CreateDirectory(_stylesPath);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter($"{_stylesPath}demo_styles.css"))
            {
                file.Write(CSSConstants.Meta);

                foreach (var c in queue)
                {
                    file.Write(ExtractCSSProperties(GetElementString(c.Element), c));
                }
            }
        }

        // Helper function to map the correct properties for each different element
        private static string ExtractCSSProperties(string name, ChromosomeBase c)
        {
            switch (c.Element)
            {
                case EDesignElement.Button:
                    return ExtractButtonProperties(name, c as CSSButton);
                case EDesignElement.Input:
                    return ExtractInputProperties(name, c as CSSInput);
                case EDesignElement.Table:
                    return ExtractTableProperties(name, c as CSSTable);
                case EDesignElement.Text:
                    return ExtractTextProperties(name, c as CSSText);
                case EDesignElement.Border:
                    return ExtractBorderProperties(name, c as CSSBorder);
                default:
                    return "";
            }
        }

        private static string ExtractButtonProperties(string name, CSSButton bc)
        {
            string result =
                CSSConstants.ClassTag(name,
                CSSConstants.Padding("px", bc.NoUniquePaddings, bc.Paddings),
                CSSConstants.Background(bc),
                CSSConstants.Color("", bc.ShadowColor),
                CSSConstants.FontSize(bc.FontSize, "px"),
                CSSConstants.BorderStyles(bc.NoUniqueBorderStyles, bc.BorderStyles),
                CSSConstants.Border("width", "px", bc.NoUniqueBorderWidths, bc.BorderWidths),
                CSSConstants.Border("radius", "px", bc.NoUniqueBorderRadii, bc.BorderRadii),
                CSSConstants.BorderColors(bc.NoUniqueBorderColors, bc.BorderColors),
                CSSConstants.Align("center"));

            if (bc.HoverColor.Name != "0")
                result +=
                CSSConstants.ClassTag($"{name}:hover, {name}-hoverState",
                CSSConstants.Color("background-", bc.HoverColor)
                );

            return result;
        }

        private static string ExtractInputProperties(string name, CSSInput ic)
        {
            string result =
                CSSConstants.ClassTag(name,
                CSSConstants.Padding("px", ic.NoUniquePaddings, ic.Paddings),
                CSSConstants.Background(ic),
                CSSConstants.Color("", ic.ShadowColor),
                CSSConstants.FontSize(ic.FontSize, "px"),
                CSSConstants.BorderStyles(ic.NoUniqueBorderStyles, ic.BorderStyles),
                CSSConstants.Border("width", "px", ic.NoUniqueBorderWidths, ic.BorderWidths),
                CSSConstants.Border("radius", "px", ic.NoUniqueBorderRadii, ic.BorderRadii),
                CSSConstants.BorderColors(ic.NoUniqueBorderColors, ic.BorderColors),
                CSSConstants.Align("center"));

            if (ic.HoverColor.Name != "0")
                result +=
                CSSConstants.ClassTag($"{name}:hover, {name}-hoverState",
                CSSConstants.Color("background-", ic.HoverColor)
                );

            return result;
        }

        private static string ExtractTableProperties(string name, CSSTable tc)
        {
            string result =
                CSSConstants.ClassTag(name,
                CSSConstants.BorderCollapse(tc.BorderCollapse),
                CSSConstants.Border(tc.BorderWith, "px", GetBorderStyleString(tc.BorderStyle), tc.BorderColor),
                CSSConstants.BorderRadius(tc.BorderRadius, "px"),
                CSSConstants.Background(tc),
                CSSConstants.Color("", tc.TDTextColor),
                CSSConstants.Align(GetHAlignString(tc.TDTextAlign)),
                CSSConstants.FontSize(tc.FontSize, "px"),
                CSSConstants.FontFamily(GetFontFamily(tc.FontFamilyID)),
                CSSConstants.Width("", 100, "%"))
                +
                CSSConstants.ClassTag(name + " td",
                CSSConstants.Padding("", tc.TDPadding, "px"),
                CSSConstants.BorderMultiConfig(tc.TDBorderPos, tc.TDBorderWith, "px", GetBorderStyleString(tc.TDBorderStyle), tc.TDBorderColor),
                CSSConstants.BorderRadius(tc.TDBorderRadius, "px"));

            if (tc.StripedTable)
            {
                result +=
                    CSSConstants.ClassTag($"{name} tr:nth-child(even)",
                    CSSConstants.Color("background-", tc.StripeBGColor));
            }

            result +=
                CSSConstants.ClassTag($"{name}:hover, {name}-hoverState",
                CSSConstants.Color("background-", tc.HoverColor))
                +
                CSSConstants.ClassTag($"{name} th",
                CSSConstants.Padding("", tc.THPadding, "px"),
                CSSConstants.Align(GetHAlignString(tc.THTextAlign)),
                CSSConstants.Background(CSSConstants.RGBA(tc.THBGColor)),
                CSSConstants.FontSize(tc.THFontSize, "px"),
                CSSConstants.BorderMultiConfig(tc.THBorderPos, tc.THBorderWith, "px", GetBorderStyleString(tc.THBorderStyle), tc.THBorderColor),
                CSSConstants.Color("", tc.THTextColor)
                );

            return result;
        }

        private static string ExtractTextProperties(string name, CSSText tc)
        {
            return
                CSSConstants.ClassTag(name,
                CSSConstants.Color("", tc.BorderColors[0]),
                CSSConstants.FontFamily(GetFontFamily(tc.FontFamilyID)),
                CSSConstants.FontSize(tc.FontSize, "px"),
                CSSConstants.Align(GetHAlignString(tc.TextAlign)),
                CSSConstants.Spacing("letter-", tc.LetterSpacing, "px"),
                CSSConstants.Spacing("word-", tc.WordSpacing, "px"),
                CSSConstants.Height("line-", tc.LineHeight, ""),
                tc.TextShadow ? CSSConstants.Shadow("text-", $"{tc.ShadowHPos}px {tc.ShadowVPos}px {tc.ShadowBlur}px", tc.ShadowColor) : ""
                )
                +
                CSSConstants.ClassTag($"{name}-h",
                CSSConstants.Color("", tc.BorderColors[1]),
                CSSConstants.FontFamily(GetFontFamily(tc.HeaderFontFamilyID)),
                CSSConstants.FontSize(tc.HeaderFontSize, "px"),
                CSSConstants.Align(GetHAlignString(tc.HeaderTextAlign)),
                CSSConstants.Spacing("letter-", tc.HeaderLetterSpacing, "px"),
                CSSConstants.Spacing("word-", tc.HeaderWordSpacing, "px"),
                CSSConstants.TextDecoration(GetTextDecorationString(tc.HeaderDecoration)),
                CSSConstants.TextTransform(GetTextTransformationString(tc.HeaderTransform)),
                tc.HeaderTextShadow ? CSSConstants.Shadow("text-", $"{tc.ShadowHPos}px {tc.ShadowVPos}px {tc.ShadowBlur}px", tc.ShadowColor) : ""
                );
        }

        private static string ExtractBorderProperties(string name, CSSBorder bc)
        {
            return
                CSSConstants.ClassTag(name,
                CSSConstants.Padding("px", bc.NoUniquePaddings, bc.Paddings),
                CSSConstants.Background(bc),
                CSSConstants.Color("", bc.HoverColor),
                CSSConstants.FontSize(bc.FontSize, "px"),
                CSSConstants.BorderStyles(bc.NoUniqueBorderStyles, bc.BorderStyles),
                CSSConstants.Border("width", "px", bc.NoUniqueBorderWidths, bc.BorderWidths),
                CSSConstants.Border("radius", "px", bc.NoUniqueBorderRadii, bc.BorderRadii),
                CSSConstants.BorderColors(bc.NoUniqueBorderColors, bc.BorderColors),
                CSSConstants.Align("center"),
                bc.BoxShadow ? CSSConstants.Shadow("box-", $"{bc.ShadowHPos}px {bc.ShadowVPos}px {bc.ShadowBlur}px", bc.ShadowColor) : ""
                );
        }

        public static string GetFontFamily(int id)
        {
            if (id < _fontFamily.Length)
                return $"\"{_fontFamily[id]}\"";
            else
                return "serif";
        }

        public static int GetFontFamilyId(string fontFamily)
        {
            for (int i = 0; i < _fontFamily.Length; i++)
            {
                if (_fontFamily[i] == fontFamily)
                    return i;
            }

            return 0;
        }

        // Gets the correct HTML code for each element, and replaces placeholders with sample text
        public static string GetDesignElementHTML(EDesignElement name)
        {
            switch (name)
            {
                case EDesignElement.Button:
                    return _elements[0].Replace("{ch}", CustomHeading);
                case EDesignElement.Input:
                    return _elements[1].Replace("{ch}", CustomHeading);
                case EDesignElement.Table:
                    return _elements[2];
                case EDesignElement.Text:
                    return _elements[3].Replace("{ch}", CustomHeading).Replace("{ct}", CustomText);
                case EDesignElement.Border:
                    return _elements[4].Replace("{ch}", CustomHeading);
                default:
                    return "<p>Unknown element</p>";
            }
        }

        // Returns the correct HTML tag name for each element
        public static string GetElementString(EDesignElement element)
        {
            switch (element)
            {
                case EDesignElement.Button:
                    return "button";
                case EDesignElement.Input:
                    return "input";
                case EDesignElement.Table:
                    return "table";
                case EDesignElement.Text:
                    return "h1, p";
                case EDesignElement.Border:
                    return "blockquote";
                default:
                    return "div";
            }
        }

        public static string GetHAlignString(int align)
        {
            switch (align)
            {
                case 1:
                    return "center";
                case 2:
                    return "right";
                case 3:
                    return "justify";
                default:
                    return "left";
            }
        }

        public static string GetVAlignString(int align)
        {
            switch (align)
            {
                case 0:
                    return "top";
                case 2:
                    return "bottom";
                default:
                    return "middle";
            }
        }

        public static string GetTextDecorationString(int decoration)
        {
            switch (decoration)
            {
                case 1:
                    return "overline";
                case 2:
                    return "line-through";
                case 3:
                    return "underline";
                default:
                    return "none";
            }
        }

        public static string GetTextTransformationString(int transformation)
        {
            switch (transformation)
            {
                case 1:
                    return "uppercase";
                case 2:
                    return "lowercase";
                case 3:
                    return "capitalize";
                default:
                    return "none";
            }
        }

        public static string GetBorderStyleString(int style)
        {
            if (style < _borderStyles.Length)
                return _borderStyles[style];
            else
                return "solid"; // Solid is default
        }

        public static int GetBorderStyleId(string borderStyle)
        {
            for (int i = 0; i < _borderStyles.Length; i++)
            {
                if (_borderStyles[i] == borderStyle)
                    return i;
            }

            return 0;
        }
    }
}
