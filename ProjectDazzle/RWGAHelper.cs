﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// RWGAHelper.cs - Static helper class to hold information regarding the use of weights. The class is static
    /// because it was easier to use its globally available variable to pass data, rather than to pass the variable
    /// trough several classes and constructors before it gets to the right place.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public static class RWGAHelper
    {
        public static bool UseRWGA
        {
            get; set;
        } = false;
    }
}
