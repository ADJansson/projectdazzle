﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// ElementViewer.cs - The main graphical user interface class. Contains the embedded web browser preview,
    /// the GeneticEngine along with the K-NN classification algorithm. To avoid blocking the GUI thread, generation
    /// iteration runs in a separate thread. Voting data is loaded and saved by this class.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class ElementViewer : Form, IClickable
    {
        public delegate void UpdateLabelsDelegate();
        public delegate void UpdateRunButtonTextDelegate();

        private const int _rowC = 10; // Number of rows
        private const int _colC = 6; // Number of columns
        private const int _K = 15; // K-nearest neighbors

        public ChromiumWebBrowser _browser;

        private JavaScriptCommunicator _jsCom;
        private TerminationConditionForm _tForm;
        private CustomTextForm _ctForm;
        private InitialConditionsForm _iForm;
        private Random _random;
        private GeneticEngine _gEngine;
        private bool _doWork = false;
        private bool _showHoverState = false;
        private bool _inPreviewMode = false;
        private bool _hasUserSelected = false;
        private ESelectionFunction _selFunc = ESelectionFunction.Elitist;
        private List<ChromosomeBase> _finalDesigns;
        private List<ChromosomeBase> _selectedDesigns;
        private double _likedPercent = 0;

        private InitialBundle _bundle;

#if DEBUG
        private const string _votesFolder = "..\\..\\..\\data\\votes";
#else
        private const string _votesFolder = "votes";
#endif
        private const string _likedFile = "liked.bin";
        private const string _dislikedFile = "disliked.bin";
        private List<ChromosomeBase> _likedList;
        private List<ChromosomeBase> _dislikedList;

        // Designs selected for manual crossover
        public List<ChromosomeBase> SelectedDesigns
        {
            get
            {
                return _selectedDesigns;
            }
        }

        public ETermCondition TerminationCondition
        {
            get; set;
        } = ETermCondition.FitnessBased; // Default termination condition

        public int TerminationLimit
        {
            get; set;
        } = 1000; // Default termination limit

        public EDesignElement Element
        {
            get; private set;
        } = EDesignElement.Button;

        public GeneticEngine GEngine
        {
            get
            {
                return _gEngine;
            }
        }

        /// <summary>
        /// For unit testing only
        /// </summary>
        /// <param name="ge"></param>
        public ElementViewer(GeneticEngine ge)
        {
            _random = new Random();
            _gEngine = ge;
            _finalDesigns = new List<ChromosomeBase>();
            _selectedDesigns = new List<ChromosomeBase>();
        }

        public ElementViewer()
        {
            InitializeComponent();

            // Initiate static classes

            HTMLHelper.Init();
            ChromosomeBase.InitStatic(new CSSBorder());

            _jsCom = new JavaScriptCommunicator(this);
            _tForm = new TerminationConditionForm(this);
            _ctForm = new CustomTextForm(this);
            _iForm = new InitialConditionsForm(this);

            _random = new Random();
            _gEngine = new GeneticEngine(_rowC * _colC, Element, chkRandom.Checked);
            _finalDesigns = new List<ChromosomeBase>();
            _selectedDesigns = new List<ChromosomeBase>();

            // http://ourcodeworld.com/articles/read/173/how-to-use-cefsharp-chromium-embedded-framework-csharp-in-a-winforms-application 3/3-17

            string applicationDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            string index = Path.Combine(applicationDirectory, "HTML\\preview.html");

            // Set up browser

            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            _browser = new ChromiumWebBrowser(index);
            _browser.FrameLoadEnd += _browser_FrameLoadEnd;

            tlpMain.Controls.Add(_browser, 0, 1);
            _browser.Dock = DockStyle.Fill;
            btnCrossover.Enabled = false;

            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            _browser.BrowserSettings = browserSettings;

            _browser.RegisterJsObject("external", _jsCom);

            // Load liked/disliked designs

            _likedList = LoadVotes(_likedFile);
            _dislikedList = LoadVotes(_dislikedFile);

            List<ChromosomeBase> LoadVotes(string fileName)
            {
                if (File.Exists($"{_votesFolder}\\{fileName}"))
                {
                    IFormatter fm = new BinaryFormatter();
                    Stream stream = new FileStream($"{_votesFolder}\\{fileName}", FileMode.Open, FileAccess.Read, FileShare.Read);

                    var result = (List<ChromosomeBase>)fm.Deserialize(stream);
                    stream.Close();
                    return result;
                }
                else
                    return new List<ChromosomeBase>();
            }
        }

        #region JavaScript function calls

        public void SelectFittest(int index)
        {
            _hasUserSelected = true; // User has selected a design, and the algorithm should obey this
            _selectedDesigns.Clear();

            LikeSelected(index); // Assume the user likes the selected design, and add it

            // When the user selects a target, use equal weights in
            // order for the program to be able to sort based on fitness
            _gEngine.CalculatePopulationFitness(index, true, false);
            _gEngine.MakeNextGeneration(_selFunc);

            ReloadDemoPage();

            this.Invoke(new UpdateLabelsDelegate(UpdateLabels));
        }

        private void ComputeKNearestNeighbor()
        {
            // K-nearest neighbor classification

            // 1) Put all votes in the same list
            var allVotes = _dislikedList.ToList();
            allVotes.AddRange(_likedList.ToList());

            // Array of tasks(threads)
            Task[] tasks = new Task[_rowC];

            // Store number of individuals classified as 'liked' for use in
            // termination condition test later
            _likedPercent = 0;

            if (_K <= allVotes.Count) // Need at least K votes
            {
                for (int i = 0, j = 0; i < _rowC; i++, j += _colC)
                {
                    int s = j; // Start index of partition
                    int e = j + _colC; // End index of partition
                    // Make threads
                    tasks[i] = Task.Run(() => ComputeNeighborhood(s, e, allVotes.Select(v => v.Clone()).ToList()));
                }
                Task.WaitAll(tasks);
            }

            _likedPercent /= (double)_gEngine.Population.Count;
            _likedPercent *= 100;
        }

        // Helper function to compute K-NN on partitions of the full list of designs
        private void ComputeNeighborhood(int start, int end, List<ChromosomeBase> votes)
        {
            // 2) For each proposed design D:
            for (int i = start; i < end; i++)
            {
                var d = _gEngine.Population[i];

                // Compute neighborhood to votes based on each new design
                foreach (var c in votes)
                {
                    c.Fitness = Math.Sqrt(d - c);
                }

                votes.Sort(); // Sort on fitness/"nearness"

                int liked = 0;
                int disliked = 0;
                int na = 0;

                // Count number of nearest neighbors
                for (int j = 0; j < _K; j++)
                {
                    if (votes[j].Vote == EVote.Liked)
                        liked++;
                    else if (votes[j].Vote == EVote.Disliked)
                        disliked++;
                    else
                        na++;
                }

                // 3) Classify based on K-nearest neighbors
                if ((liked >= disliked) && (liked >= na))
                {
                    d.Vote = EVote.Liked;
                    d.MatchPercent = Math.Round(liked / (double)_K * 100); // Calculate match percent
                    if (d.Fitness != double.MaxValue) // At the start, fitness defaults to max value. This implies selection has not begun
                        d.Fitness /= d.MatchPercent; // Fitness boost for predicted liked designs based on match
                    _likedPercent++;
                }
                else if ((disliked >= liked) && (disliked >= na))
                {
                    d.Vote = EVote.Disliked;
                    d.MatchPercent = Math.Round(disliked / (double)_K * 100); // Calculate match percent
                }
            }
        }

        // Called when user manually selects design for crossover
        public void AddSelected(int id)
        {
            if (_selectedDesigns.Contains(_gEngine.Population[id]))
                _selectedDesigns.Remove(_gEngine.Population[id]);
            else
                _selectedDesigns.Add(_gEngine.Population[id]);
        }

        // Called when user "Likes" a design
        public void LikeSelected(int id)
        {
            _gEngine.Population[id].Vote = EVote.Liked;
            _likedList.Add(_gEngine.Population[id]);
        }

        // Called when user "Dislikes" a design
        public void DislikeSelected(int id)
        {
            _gEngine.Population[id].Vote = EVote.Disliked;
            _dislikedList.Add(_gEngine.Population[id]);
        }

        #endregion

        private void UpdateLabels()
        {
            lblPopFit.Text = $"Population fitness: {(Math.Round(_gEngine.PopulationFitness, 2))}";
            lblGeneration.Text = $"Generation {_gEngine.Generation}";
            lblPopVotes.Text = $"Population popularity: {(Math.Round(_likedPercent, 2))}%";
        }

        private void UpdateRunButtonText()
        {
            btnRunFitness.Text = _doWork ? "Stop this madness!" : "Run until termination";

            // Disable control buttons while algorithm is running
            grbElements.Enabled = !_doWork;
            btnOpenTermCond.Enabled = !_doWork;
            btnReset.Enabled = !_doWork;
            btnSave.Enabled = !_doWork;
            btnExport.Enabled = !_doWork;
            btnClear.Enabled = !_doWork;
            grbSelection.Enabled = !_doWork;
            grbCrossover.Enabled = !_doWork;
            chkRandom.Enabled = !_doWork;
        }

        // Clean-up, save voting lists
        private void ElementViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveVotesData();

            // Shut down the browser
            Cef.Shutdown();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(Element);
        }

        // Update and reload the preview page
        private void ReloadDemoPage()
        {
            ComputeKNearestNeighbor();
            SortPopulationBasedOnVotes();
            this.Invoke(new UpdateLabelsDelegate(UpdateLabels));
            HTMLHelper.CreatePreviewHtmlFileWithClassification(_rowC, _colC, Element, _gEngine.Population);
            HTMLHelper.WriteTempCSSClassFile(_gEngine.Population);

            if (_browser.IsBrowserInitialized)
                _browser.Reload();
        }

        private void rbtElitist_Click(object sender, EventArgs e)
        {
            _selFunc = ESelectionFunction.Elitist;
        }

        private void rbtRoulette_Click(object sender, EventArgs e)
        {
            _selFunc = ESelectionFunction.RouletteWheel;
        }

        private void rbtButton_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(EDesignElement.Button);
        }

        private void rbtInput_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(EDesignElement.Input);
        }

        private void rbtParagraph_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(EDesignElement.Text);
        }

        private void rbtTable_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(EDesignElement.Table);
        }

        private void rbtBorder_Click(object sender, EventArgs e)
        {
            LoadNewDesignElement(EDesignElement.Border);
        }

        private void LoadNewDesignElement(EDesignElement element)
        {
            _hasUserSelected = false; // Reset user interaction variable
            Element = element;
            ECrossoverType t = _gEngine.CrossoverType;
            _gEngine = new GeneticEngine(_rowC * _colC, Element, chkRandom.Checked);
            _gEngine.CrossoverType = t;

            _selectedDesigns.Clear();

            TerminationLimit = _tForm.Value;

            UpdateLabels();
            ReloadDemoPage();
        }

        /// <summary>
        /// Calls to GeneticEngine to create a new initial population based
        /// on a known design
        /// </summary>
        /// <param name="basis"></param>
        public void LoadNewPopulationBasedOnParse(ChromosomeBase basis)
        {
            ECrossoverType t = _gEngine.CrossoverType;

            _gEngine = new GeneticEngine(_rowC * _colC, Element, chkRandom.Checked, basis);
            _gEngine.CrossoverType = t;

            _selectedDesigns.Clear();

            TerminationLimit = _tForm.Value;

            this.Invoke(new UpdateLabelsDelegate(UpdateLabels));
            ReloadDemoPage();
        }

        private void SortPopulationBasedOnVotes()
        {
            // If the user hasn't decided for a design, use votes
            // to sort the population and pick the most liked design
            if (!_hasUserSelected)
            {
                foreach (var p in _gEngine.Population)
                {
                    p.Fitness = p.Vote == EVote.Liked ? 1 / p.MatchPercent : p.MatchPercent;
                }

                _gEngine.Population.Sort();
            }
        }

        private void btnRunFitness_Click(object sender, EventArgs e)
        {
            _doWork = !_doWork;

            SortPopulationBasedOnVotes();

            if (_doWork)
            {
                // Make background thread
                Thread tAuto = new Thread(() =>
                {
                    Stopwatch st = new Stopwatch();
                    st.Start();
                    bool condition = true;

                    // Run selection and iterate until termination condition is met or the user cancels
                    while (condition && _doWork)
                    {
                        _gEngine.CalculatePopulationFitness(0);
                        _gEngine.MakeNextGeneration(_selFunc);
                        this.Invoke(new UpdateLabelsDelegate(UpdateLabels));

                        switch (TerminationCondition)
                        {
                            case ETermCondition.FitnessBased:
                                condition = _gEngine.PopulationFitness > TerminationLimit;
                                break;
                            case ETermCondition.GenerationsBased:
                                condition = _gEngine.Generation < TerminationLimit;
                                break;
                            case ETermCondition.VotesBased:
                                condition = IsPopulationPopular();
                                break;
                        }
                    }

                    ReloadDemoPage();
                    _doWork = false; // Work is done

                    st.Stop();
                    TimeSpan ts = st.Elapsed;
                    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                    Debug.WriteLine("RunTime " + elapsedTime);

                    this.Invoke(new UpdateRunButtonTextDelegate(UpdateRunButtonText));

                    if (TerminationCondition == ETermCondition.GenerationsBased)
                        TerminationLimit += _tForm.Value;

                });
                tAuto.IsBackground = true;
                tAuto.Start(); // Run the thread
            }

            UpdateRunButtonText();
        }

        // Helper function to calculate % of population
        // which is predicted to be liked based on K-NN
        private bool IsPopulationPopular()
        {
            ComputeKNearestNeighbor();

            return _likedPercent < TerminationLimit;
        }

        private void btnOpenTermCond_Click(object sender, EventArgs e)
        {
            _tForm.ShowDialog();
        }

        private void chkMultiSelect_CheckedChanged(object sender, EventArgs e)
        {
            _jsCom.MultiSelect = chkMultiSelect.Checked;
            btnCrossover.Enabled = chkMultiSelect.Checked;
            grbSelection.Enabled = !chkMultiSelect.Checked;
            grbCrossover.Enabled = !chkMultiSelect.Checked;
            rbtArithmetic.Checked = true;
            _gEngine.CrossoverType = ECrossoverType.Arithmetic;
        }

        private void btnCrossover_Click(object sender, EventArgs e)
        {
            if (_selectedDesigns.Count < 2)
                MessageBox.Show("Please select 2 or more designs for crossover");
            else
            {
                _gEngine.MakeNextGeneration(_selectedDesigns);
                ReloadDemoPage();
                UpdateLabels();
                _selectedDesigns.Clear();
            }
        }

        private void rbtStandardGA_Click(object sender, EventArgs e)
        {
            RWGAHelper.UseRWGA = false;
        }

        private void rbtRWGA_Click(object sender, EventArgs e)
        {
            RWGAHelper.UseRWGA = true;
        }

        private void rbtSingleP_Click(object sender, EventArgs e)
        {
            _gEngine.CrossoverType = ECrossoverType.SinglePoint;
        }

        private void rbtMultiP_Click(object sender, EventArgs e)
        {
            _gEngine.CrossoverType = ECrossoverType.MultiPoint;
        }

        private void rbtArithmetic_Click(object sender, EventArgs e)
        {
            _gEngine.CrossoverType = ECrossoverType.Arithmetic;
        }

        // Exports selected design to CSS-file
        private void btnExport_Click(object sender, EventArgs e)
        {
            if (_finalDesigns.Count == 0)
                MessageBox.Show("Export queue is empty!");
            else
            {
                HTMLHelper.WriteFinalCSSClassFile(_finalDesigns);
                MessageBox.Show("Exported CSS-file as \"demo_styles.css\"");
            }
        }

        // Adds fittest element to export queue
        private void btnSave_Click(object sender, EventArgs e)
        {
            _finalDesigns.Add(_gEngine.Population[0]);
            MessageBox.Show("Added " + _gEngine.Population[0].Element + " to export queue");
        }

        // Clears the element export queue
        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Clear export queue?", "Clear export queue", MessageBoxButtons.YesNo) == DialogResult.Yes)
                _finalDesigns.Clear();
        }

        private void loadCSSfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ParsedPreview(this).Show();
        }

        private void tmiAlpha_Click(object sender, EventArgs e)
        {
            ToggleToolStripChecks(sender as ToolStripMenuItem);
        }

        private void tmiWhite_Click(object sender, EventArgs e)
        {
            ToggleToolStripChecks(sender as ToolStripMenuItem);
        }

        private void tmiBlack_Click(object sender, EventArgs e)
        {
            ToggleToolStripChecks(sender as ToolStripMenuItem);
        }

        private void _browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            ChangeBGColor();
        }

        private void ToggleToolStripChecks(ToolStripMenuItem tsp)
        {
            foreach (ToolStripMenuItem tmi in changeBackgroundToolStripMenuItem.DropDownItems)
            {
                tmi.Checked = tsp == tmi;
            }

            ChangeBGColor();
        }

        private void ChangeBGColor()
        {
            if (tmiAlpha.Checked)
                _browser.ExecuteScriptAsync($"changeBGColor({2})");
            else if (tmiWhite.Checked)
                _browser.ExecuteScriptAsync($"changeBGColor({1})");
            else
                _browser.ExecuteScriptAsync($"changeBGColor({0})");
        }

        // To be called when preview mode is entered,
        // to apply temporary design(s) to preview page
        private void UpdateDesignStyles()
        {
            _browser.ExecuteScriptAsync("updatePreviewStyles()");
        }

        private void btnSetText_Click(object sender, EventArgs e)
        {
            _ctForm.ShowDialog();

            ReloadDemoPage();
        }

        private void setInitalConditionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _iForm.ShowDialog();
        }

        // Called from InitialConditionsForm to apply user submitted values
        public void SetInitialBundle(InitialBundle bundle)
        {
            _bundle = bundle;

            Element = EDesignElement.Button;

            rbtButton.Checked = true; // > Only buttons are supported for now

            ECrossoverType t = _gEngine.CrossoverType;

            _gEngine = new GeneticEngine(_rowC * _colC, bundle);
            _gEngine.CrossoverType = t;

            _selectedDesigns.Clear();

            TerminationLimit = _tForm.Value;

            this.Invoke(new UpdateLabelsDelegate(UpdateLabels));
            ReloadDemoPage();
        }

        private void lockParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rbtPropLock.Checked = true;
            new LockParametersForm(this).Show();
            _gEngine.CrossoverType = ECrossoverType.PropertyLock;
        }

        public ChromosomeBase GetFittest()
        {
            return _gEngine.Population[0];
        }

        private void openDevConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _browser.ShowDevTools();
        }


        private void offToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToggleHoverState();
        }

        private void onToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToggleHoverState();
        }

        private void ToggleHoverState()
        {
            _showHoverState = !_showHoverState;
            offToolStripMenuItem.Checked = !_showHoverState;
            onToolStripMenuItem.Checked = _showHoverState;

            _browser.ExecuteScriptAsync($"toggleHoverState()");
        }

        // Switches between population preview and static page preview modes
        private void enterPreviewModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _inPreviewMode = !_inPreviewMode;
            enterPreviewModeToolStripMenuItem.Text = _inPreviewMode ? "Leave preview mode" : "Enter preview mode";

            grbElements.Enabled = !_inPreviewMode;
            grbAlgoProp.Enabled = !_inPreviewMode;

            string page = _inPreviewMode ? "static_preview.html" : "preview.html";

            string applicationDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            string index = Path.Combine(applicationDirectory, $"HTML\\{page}");

            _browser.Load(index);
        }

        // Allows the user to delete vote data files from GUI
        private void resetVoteDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Erase votes data?", "WARNING", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (MessageBox.Show("REALLY erase all votes data? For realsies?", "WARNING", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    _likedList = new List<ChromosomeBase>();
                    _dislikedList = new List<ChromosomeBase>();

                    SaveVotesData();

                    MessageBox.Show("Erased votes data");

                    foreach (var c in _gEngine.Population)
                    {
                        c.Vote = EVote.Unrated;
                        c.MatchPercent = 0;
                    }

                    ReloadDemoPage();
                }
            }
        }

        // Saves the current lists of votes to files
        private void SaveVotesData()
        {
            // Based on https://msdn.microsoft.com/en-us/library/ms973893.aspx 20/4-17

            IFormatter fm = new BinaryFormatter();

            Directory.CreateDirectory(_votesFolder);

            Stream likedStream = new FileStream($"{_votesFolder}\\{_likedFile}", FileMode.Create, FileAccess.Write, FileShare.None);
            Stream dislikedStream = new FileStream($"{_votesFolder}\\{_dislikedFile}", FileMode.Create, FileAccess.Write, FileShare.None);

            fm.Serialize(likedStream, _likedList);
            fm.Serialize(dislikedStream, _dislikedList);
            likedStream.Close();
            dislikedStream.Close();
        }
    }
}
