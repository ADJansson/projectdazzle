﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDazzle
{
    /// <summary>
    /// Simple interface for forms supporting user interaction though web browser.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public interface IClickable
    {
        void AddSelected(int id);
        void SelectFittest(int id);
        void LikeSelected(int id);
        void DislikeSelected(int id);
    }
}
