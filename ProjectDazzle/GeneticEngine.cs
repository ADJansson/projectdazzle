﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace ProjectDazzle
{
    public enum ESelectionFunction
    {
        RouletteWheel,
        Elitist,
    } // To measure the performance of different selection functions

    /// <summary>
    /// GeneticEngine.cs - Population helper class. Performs selection and iterates the generation counter, along
    /// with the readability test.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public class GeneticEngine
    {
        private int _currentGeneration = 0;
        private ChromosomeBase _fittest;
        private int _populationSize = 0;
        private Random _random;
        private double _percentageToReplace = 10; // 10% of the least fit are replaced
        private int _trimAmount = 0;
        private EDesignElement _element;

        public virtual List<ChromosomeBase> Population
        {
            get; private set;
        } = new List<ChromosomeBase>();

        /// <summary>
        /// The fitness sum of the population
        /// </summary>
        public virtual double PopulationFitness
        {
            get
            {
                return CalculatePopulationFitnessSum();
            }
        }

        public int Generation
        {
            get
            {
                return _currentGeneration;
            }
        }

        public ECrossoverType CrossoverType
        {
            get; set;
        } = ECrossoverType.SinglePoint;

        public GeneticEngine()
        {

        }

        public GeneticEngine(int populationSize, EDesignElement element, bool useRandomSeed, ChromosomeBase basis = null)
        {
            _populationSize = populationSize;
            _element = element;
            _random = new Random(Guid.NewGuid().GetHashCode());

            // Create a random or seed-based population of provided size
            for (int i = 0, s = 0; i < _populationSize; i++, s++)
            {
                ChromosomeBase p = null;
                switch (_element)
                {
                    case EDesignElement.Button:
                        p = useRandomSeed ? new CSSButton() : new CSSButton(s);
                        break;
                    case EDesignElement.Input:
                        p = useRandomSeed ? new CSSInput() : new CSSInput(s);
                        break;
                    case EDesignElement.Table:
                        p = useRandomSeed ? new CSSTable() : new CSSTable(s);
                        break;
                    case EDesignElement.Text:
                        p = useRandomSeed ? new CSSText() : new CSSText(s);
                        break;
                    case EDesignElement.Border:
                        p = useRandomSeed ? new CSSBorder() : new CSSBorder(s);
                        break;
                }

                if (basis != null)
                {
                    p.MultiPointCrossover(basis); // Do some crossovers to approximate provided basis design
                    Population.Add(p);
                }
                else
                {
                    if (TestReadable(p)) // Test readability and add to population if pass
                        Population.Add(p);
                    else
                        i--;
                }
            }

            // If a base design is provided, compute population fitness with respect to this and sort
            if (basis != null)
            {
                CalculatePopulationFitness(basis, false, false);
                Population.Sort();
            }
        }

        // Create population based on provided initial data
        public GeneticEngine(int populationSize, InitialBundle bundle)
        {
            _populationSize = populationSize;
            _element = EDesignElement.Button;
            _random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < _populationSize; i++)
            {
                Population.Add(new CSSButton(bundle));
            }
        }

        /// <summary>
        /// Calculates the population fitness using the chromosome at the selected index as target.
        /// useEqualWeights handles problems with custom weights set by propertylocks.
        /// If all but one weight is 1, and the user selects a design with a property with weight = 0,
        /// the algorithm cannot distinguish between the selected design, and any other. Therefore, using equal
        /// weights, the program can differentiate between the target design and any other when sorting the
        /// list based on fitness.
        /// </summary>
        /// <param name="index">Index of target chromosome in population list</param>
        /// <param name="useEqualWeights">If true, uses weights = 1</param>
        /// <param name="testReadability">If true, adds extra penalty if text is unreadable</param>
        public void CalculatePopulationFitness(int index, bool useEqualWeights = false, bool testReadability = true)
        {
            CalculatePopulationFitness(Population[index], useEqualWeights, testReadability);
        }

        public void CalculatePopulationFitness(ChromosomeBase fittest, bool useEqualWeights = false, bool testReadability = true)
        {
            _fittest = fittest;

            // Assume the user knows what he is doing, and ignore readability test if the selected design
            // itself fails the test
            testReadability = TestReadable(_fittest);

            foreach (var c in Population)
            {
                c.Fitness = Math.Sqrt(useEqualWeights ? _fittest - c : _fittest / c);
                if (testReadability)
                    c.Fitness += TestReadable(c) ? 0 : 100;
            }
        }

        /// <summary>
        /// Using https://www.w3.org/TR/AERT#color-contrast's (6/4-17) formula,
        /// readability is calculated and validated. If the text is not 
        /// readable, the chromosome gets a fitness penalty
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool TestReadable(ChromosomeBase c)
        {
            Color bgColor = c.BGColor;
            Color gradColor = c.Gradient;
            Color textColor = c.ShadowColor;

            double brightness = (textColor.R * 299 + textColor.G * 587 + textColor.B) / 1000.0;

            double difference = (Math.Max(bgColor.R, textColor.R) - Math.Min(bgColor.R, textColor.R))
                + (Math.Max(bgColor.G, textColor.G) - Math.Min(bgColor.G, textColor.G))
                + (Math.Max(bgColor.B, textColor.B) - Math.Min(bgColor.B, textColor.B));

            double gradDifference = 200;

            if (c.UseGradient)
                gradDifference = (Math.Max(gradColor.R, textColor.R) - Math.Min(gradColor.R, textColor.R))
                + (Math.Max(gradColor.G, textColor.G) - Math.Min(gradColor.G, textColor.G))
                + (Math.Max(gradColor.B, textColor.B) - Math.Min(gradColor.B, textColor.B));


            bool ad = bgColor.A >= 128 && textColor.A >= 128; // Extra check for alpha values

            return brightness >= 125 && difference >= 500 && gradDifference >= 200 && ad;
        }

        // Performs crossover on list of manually selected designs
        public void MakeNextGeneration(List<ChromosomeBase> selected)
        {
            for (int i = 1; i < selected.Count; i++)
            {
                selected[0].Crossover(selected[i], CrossoverType);
            }

            Population[0] = selected[0];
            _fittest = Population[0];
            CalculatePopulationFitness(_fittest);
            Population.Sort();

            _currentGeneration++;
        }

        // Performs crossover using selected selection function
        public void MakeNextGeneration(ESelectionFunction selFunc = ESelectionFunction.Elitist)
        {
            if (_fittest == null)
            {
                Debug.WriteLine("Fittest individual not set!");
                return;
            }

            // Sort in decreasing fitness order
            Population.Sort();

            _trimAmount = (int)Math.Ceiling(_populationSize / _percentageToReplace); // Replace the 10% least fit

            // Make the next generation using selected selection scheme
            switch (selFunc)
            {
                case ESelectionFunction.Elitist:
                    UseElitistSelection();
                    break;
                case ESelectionFunction.RouletteWheel:
                    UseRouletteSelection();
                    break;
            }

            CalculatePopulationFitness(_fittest);
            _currentGeneration++;
        }

        public void UseElitistSelection()
        {
            int topHalf = _populationSize / 2;

            // Elitist selection

            for (int i = 1; i <= _trimAmount; i++)
            {
                // Crossover between the 10% least fit and 50% most fit similar to Genitor
                ChromosomeBase mate = Population[_random.Next(0, topHalf)];
                Population[_populationSize - i].Crossover(mate, CrossoverType);
            }
        }

        public void UseRouletteSelection()
        {
            double? mx = Population.Last()?.Fitness;
            double maxFit = 0;

            if (mx is null)
                return;
            else
                maxFit = (double)mx;

            List<double> probList = new List<double>(); // List to hold probabilities

            foreach (var ch in Population)
                probList.Add(maxFit - ch.Fitness);

            double probSum = probList.Sum(); // Total probability

            double rt = _random.NextDouble();

            for (int i = 1; i <= _trimAmount; i++)
            {
                var mate1 = RouletteHelper(_random.NextDouble(), probSum, probList);
                var mate2 = RouletteHelper(_random.NextDouble(), probSum, probList);

                ChromosomeBase child = null;

                switch (_element)
                {
                    case EDesignElement.Button:
                        child = new CSSButton();
                        break;
                    case EDesignElement.Input:
                        child = new CSSInput();
                        break;
                    case EDesignElement.Table:
                        child = new CSSTable();
                        break;
                    case EDesignElement.Text:
                        child = new CSSText();
                        break;
                    case EDesignElement.Border:
                        child = new CSSBorder();
                        break;
                }

                child?.Crossover(mate2, CrossoverType);

                Population[_populationSize - i] = child;
            }

        }

        // Helper to select a parent based on probability
        public ChromosomeBase RouletteHelper(double prob, double probSum, List<double> probList)
        {
            for (int i = 0; i < Population.Count; i++)
            {
                if (prob >= (probList[i] / probSum))
                    return Population[i];
            }

            return Population.Last();
        }

        private double CalculatePopulationFitnessSum()
        {
            double sum = 0;

            foreach (var c in Population)
                sum += c.Fitness;

            return sum;
        }
    }
}
