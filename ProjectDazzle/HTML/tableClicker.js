// Sends a signal to the C#-class with the id of the selected design
function setSelected(id) {

    window.external.setSelected(id);

    if (!window.external.isMultiSelectEnabled()) {
    
        var ys = document.getElementsByClassName("selected");
    
        for (i = 0; i < ys.length; i++) {
    
            ys[i].classList.remove("selected"); // Deselect (reset styles) for parsed preview
        }
    }

    var selected = document.getElementById("td" + id);

    // Using classList to toggle appearance
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/classList 4/4-17
    selected.classList.toggle("selected");
}

function likeSelected(id){
	
	console.log("liked " + id);
	window.external.likeSelected(id);
	updateFeedbackText(id, "Liked");
}

function dislikeSelected(id){
	
	console.log("disliked " + id);
	window.external.dislikeSelected(id);
	updateFeedbackText(id, "Disliked");
}

function updateFeedbackText(id, text){
	var d = document.getElementById("feedback" + id);
	d.innerHTML = text;
	d.classList.add("visible", "feedback-container");
}

// Changes the background of the preview page
function changeBGColor(c) {

    if (c === 0)
        document.body.className = "black";
    else if (c === 1)
        document.body.className = "white";
    else
        document.body.className = "alpha";
}

// Toggles hover state on all elements
function toggleHoverState() {

    var x = document.getElementById("previewTable").rows.length;
    var y = document.getElementById("previewTable").rows[0].cells.length;

    var z = x * y; // Number of elements in the table

    for (i = 0; i < z; i++) {

        var c = document.getElementById("temp" + i);
        c.classList.toggle("temp" + i + "-hoverState");
    }
}
