﻿namespace ProjectDazzle
{
    partial class TerminationConditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TerminationConditionForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblGenerationComp = new System.Windows.Forms.Label();
            this.nudValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtGenerations = new System.Windows.Forms.RadioButton();
            this.rbtFitness = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.rbtVotes = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudValue)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtVotes);
            this.groupBox1.Controls.Add(this.lblGenerationComp);
            this.groupBox1.Controls.Add(this.nudValue);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbtGenerations);
            this.groupBox1.Controls.Add(this.rbtFitness);
            this.groupBox1.Controls.Add(this.btnOk);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // lblGenerationComp
            // 
            resources.ApplyResources(this.lblGenerationComp, "lblGenerationComp");
            this.lblGenerationComp.Name = "lblGenerationComp";
            // 
            // nudValue
            // 
            resources.ApplyResources(this.nudValue, "nudValue");
            this.nudValue.Name = "nudValue";
            this.nudValue.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudValue_KeyUp);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // rbtGenerations
            // 
            resources.ApplyResources(this.rbtGenerations, "rbtGenerations");
            this.rbtGenerations.Name = "rbtGenerations";
            this.rbtGenerations.TabStop = true;
            this.rbtGenerations.UseVisualStyleBackColor = true;
            // 
            // rbtFitness
            // 
            resources.ApplyResources(this.rbtFitness, "rbtFitness");
            this.rbtFitness.Name = "rbtFitness";
            this.rbtFitness.TabStop = true;
            this.rbtFitness.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rbtVotes
            // 
            resources.ApplyResources(this.rbtVotes, "rbtVotes");
            this.rbtVotes.Name = "rbtVotes";
            this.rbtVotes.TabStop = true;
            this.rbtVotes.UseVisualStyleBackColor = true;
            this.rbtVotes.CheckedChanged += new System.EventHandler(this.rbtVotes_CheckedChanged);
            // 
            // TerminationConditionForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TerminationConditionForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rbtGenerations;
        private System.Windows.Forms.RadioButton rbtFitness;
        private System.Windows.Forms.NumericUpDown nudValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblGenerationComp;
        private System.Windows.Forms.RadioButton rbtVotes;
    }
}