﻿using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using System.Collections.Generic;

namespace ProjectDazzle
{
    /// <summary>
    /// ParsedPreview.cs - Similar to the ElementView, contains an embedded web browser to preview parsed CSS
    /// data in a separate window. Uses the ICliclable interface to allow for JavaScript interaction.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class ParsedPreview : Form, IClickable
    {
        public ChromiumWebBrowser _browser;
        private JavaScriptCommunicator _jsCom;
        private ElementViewer _parent;
        private List<ChromosomeBase> _parsedList;

        private const string _fileName = "parsed_preview";// Parsed data is written to this file

        public ParsedPreview(ElementViewer parent)
        {
            InitializeComponent();

            _parent = parent;

            _jsCom = new JavaScriptCommunicator(this);

            _parsedList = CSSParser.ParseCSSFiles();
            HTMLHelper.CreateParsedPreviewHtmlFile(_parsedList.Count);
            HTMLHelper.WriteParsedElementCSS(_parsedList);

            string applicationDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            string index = Path.Combine(applicationDirectory, $"HTML\\{_fileName}.html");

            CefSettings settings = new CefSettings();
            if (!Cef.IsInitialized)
                Cef.Initialize(settings);
            _browser = new ChromiumWebBrowser(index);

            Controls.Add(_browser);
            _browser.Dock = DockStyle.Fill;

            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            _browser.BrowserSettings = browserSettings;

            _browser.RegisterJsObject("external", _jsCom);
        }

        // Using the select-function in JavaScript to select a design
        public void SelectFittest(int id)
        {
            _parent.LoadNewPopulationBasedOnParse(_parsedList[id]);
        }

        public void AddSelected(int id)
        {

        }

        public void LikeSelected(int id)
        {

        }

        public void DislikeSelected(int id)
        {

        }
    }
}
