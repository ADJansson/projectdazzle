﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDazzle
{
    /// <summary>
    /// CustomTextForm.cs - GUI for entering custom text on preview elements
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class CustomTextForm : Form
    {
        private ElementViewer _parent;

        public CustomTextForm(ElementViewer parent)
        {
            InitializeComponent();

            _parent = parent;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveAndClose();
        }

        private void txtCustom_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SaveAndClose();
            }
        }

        private void SaveAndClose()
        {
            HTMLHelper.CustomHeading = txtCustom.Text;
            if (txtParagraph.Text.Length > 0)
                HTMLHelper.CustomText = txtParagraph.Text; // An empty string here would be SCHTOOPID!

            Close();
        }

        // Load and display the saved text from parent
        private void CustomTextForm_Load(object sender, EventArgs e)
        {
            txtParagraph.Visible = _parent.Element == EDesignElement.Text;
            txtParagraph.Enabled = _parent.Element == EDesignElement.Text;
        }
    }
}
