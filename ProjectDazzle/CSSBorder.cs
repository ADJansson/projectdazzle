﻿using System;

namespace ProjectDazzle
{
    /// <summary>
    /// CSSBorder.cs - CSS Border box/container for other elements on a web page. Extends the base chromosome.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [Serializable]
    public class CSSBorder : ChromosomeBase
    {
        public bool BoxShadow
        {
            get
            {
                return _uniqueGenes.GetValue(0) == 1;
            }
        }

        public CSSBorder() : base(EDesignElement.Border)
        {
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool);

            MakeWeights();
        }

        public CSSBorder(int seed) : base(EDesignElement.Border, seed)
        {
            _uniqueGenes.AddValue(_random.Next(0, 2), TLBool); // If the box has a shadow

            MakeWeights();
        }

        public CSSBorder(CSSBorder copy) : base(copy)
        {
        }

        public override ChromosomeBase Clone()
        {
            return new CSSBorder(this);
        }
    }
}
