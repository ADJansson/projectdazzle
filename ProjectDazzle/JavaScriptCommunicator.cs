﻿using System.Diagnostics;

namespace ProjectDazzle
{
    /// <summary>
    /// Helper class used to communicate between JavaScript in browser and C#-code.
    /// http://notions.okuda.ca/2009/06/11/calling-javascript-in-a-webbrowser-control-from-c/ 3/3-17
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class JavaScriptCommunicator
    {
        private int _selectedID = 0;
        private IClickable _receiver;

        public bool MultiSelect
        {
            get; set;
        } = false;

        public JavaScriptCommunicator(IClickable receiver)
        {
            _receiver = receiver;
        }

        // Functions to be called from JavaScript
        // hence the non-C# camel case convention
        public void setSelected(int id)
        {
            _selectedID = id;

            if (MultiSelect)
            {
                _receiver.AddSelected(id);
            }
            else
            {
                _receiver.SelectFittest(id);
            }
        }

        public void likeSelected(int id)
        {
            _receiver.LikeSelected(id);
        }

        public void dislikeSelected(int id)
        {
            _receiver.DislikeSelected(id);
        }

        public bool isMultiSelectEnabled()
        {
            return MultiSelect;
        }

        public void setMultiSelectEnabled(bool enabled)
        {
            MultiSelect = enabled;
        }
    }
}
