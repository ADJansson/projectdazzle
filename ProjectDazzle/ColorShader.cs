﻿using System;
using System.Drawing;

namespace ProjectDazzle
{
    /// <summary>
    /// ColorShader.cs - Helper class to calculate shade and tint based on provided color.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public static class ColorShader
    {
        private static double _shadeFactor = 0.8;
        private static double _tintFactor = 0.3;

        // RGB Shade/tint formulae from 
        // http://stackoverflow.com/questions/6615002/given-an-rgb-value-how-do-i-create-a-tint-or-shade (20/3-17)

        public static Color Shade(Color color)
        {
            return Color.FromArgb(
                color.A,
                (int)Math.Ceiling(color.R * _shadeFactor),
                (int)Math.Ceiling(color.G * _shadeFactor),
                (int)Math.Ceiling(color.B * _shadeFactor));
        }

        public static Color Tint(Color color)
        {
            return Color.FromArgb(color.A, Tint(color.R), Tint(color.G), Tint(color.B));

            int Tint(int x)
            {
                return (int)Math.Ceiling(x + (_tintFactor * (255 - x)));
            }
        }

        // If color is light, return shade
        // If color is dark, return tint
        public static Color MakeShadeTint(Color color)
        {
            return Average(color) > 128 ? Shade(color) : Tint(color);

            double Average(Color c)
            {
                return (c.R + c.G + c.B) / 3.0;
            }
        }
    }
}
