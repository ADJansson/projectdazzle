﻿namespace ProjectDazzle
{
    partial class LockParametersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LockParametersForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.chkBorderStyles = new System.Windows.Forms.CheckBox();
            this.chkBorderWidths = new System.Windows.Forms.CheckBox();
            this.chkBorderColors = new System.Windows.Forms.CheckBox();
            this.chkHoverColor = new System.Windows.Forms.CheckBox();
            this.chkPaddings = new System.Windows.Forms.CheckBox();
            this.chkTextColor = new System.Windows.Forms.CheckBox();
            this.chkTextSize = new System.Windows.Forms.CheckBox();
            this.chkShape = new System.Windows.Forms.CheckBox();
            this.chkColor = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.btnApply);
            this.groupBox1.Controls.Add(this.chkBorderStyles);
            this.groupBox1.Controls.Add(this.chkBorderWidths);
            this.groupBox1.Controls.Add(this.chkBorderColors);
            this.groupBox1.Controls.Add(this.chkHoverColor);
            this.groupBox1.Controls.Add(this.chkPaddings);
            this.groupBox1.Controls.Add(this.chkTextColor);
            this.groupBox1.Controls.Add(this.chkTextSize);
            this.groupBox1.Controls.Add(this.chkShape);
            this.groupBox1.Controls.Add(this.chkColor);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // btnApply
            // 
            resources.ApplyResources(this.btnApply, "btnApply");
            this.btnApply.Name = "btnApply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // chkBorderStyles
            // 
            resources.ApplyResources(this.chkBorderStyles, "chkBorderStyles");
            this.chkBorderStyles.Name = "chkBorderStyles";
            this.chkBorderStyles.UseVisualStyleBackColor = true;
            // 
            // chkBorderWidths
            // 
            resources.ApplyResources(this.chkBorderWidths, "chkBorderWidths");
            this.chkBorderWidths.Name = "chkBorderWidths";
            this.chkBorderWidths.UseVisualStyleBackColor = true;
            // 
            // chkBorderColors
            // 
            resources.ApplyResources(this.chkBorderColors, "chkBorderColors");
            this.chkBorderColors.Name = "chkBorderColors";
            this.chkBorderColors.UseVisualStyleBackColor = true;
            // 
            // chkHoverColor
            // 
            resources.ApplyResources(this.chkHoverColor, "chkHoverColor");
            this.chkHoverColor.Name = "chkHoverColor";
            this.chkHoverColor.UseVisualStyleBackColor = true;
            // 
            // chkPaddings
            // 
            resources.ApplyResources(this.chkPaddings, "chkPaddings");
            this.chkPaddings.Name = "chkPaddings";
            this.chkPaddings.UseVisualStyleBackColor = true;
            // 
            // chkTextColor
            // 
            resources.ApplyResources(this.chkTextColor, "chkTextColor");
            this.chkTextColor.Name = "chkTextColor";
            this.chkTextColor.UseVisualStyleBackColor = true;
            // 
            // chkTextSize
            // 
            resources.ApplyResources(this.chkTextSize, "chkTextSize");
            this.chkTextSize.Name = "chkTextSize";
            this.chkTextSize.UseVisualStyleBackColor = true;
            // 
            // chkShape
            // 
            resources.ApplyResources(this.chkShape, "chkShape");
            this.chkShape.Name = "chkShape";
            this.chkShape.UseVisualStyleBackColor = true;
            // 
            // chkColor
            // 
            resources.ApplyResources(this.chkColor, "chkColor");
            this.chkColor.Name = "chkColor";
            this.chkColor.UseVisualStyleBackColor = true;
            // 
            // LockParametersForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "LockParametersForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkBorderStyles;
        private System.Windows.Forms.CheckBox chkBorderWidths;
        private System.Windows.Forms.CheckBox chkBorderColors;
        private System.Windows.Forms.CheckBox chkHoverColor;
        private System.Windows.Forms.CheckBox chkPaddings;
        private System.Windows.Forms.CheckBox chkTextColor;
        private System.Windows.Forms.CheckBox chkTextSize;
        private System.Windows.Forms.CheckBox chkShape;
        private System.Windows.Forms.CheckBox chkColor;
        private System.Windows.Forms.Button btnApply;
    }
}