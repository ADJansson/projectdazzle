﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDazzle
{
    public enum ETermCondition
    {
        FitnessBased,
        GenerationsBased,
        VotesBased,
    }

    /// <summary>
    /// TerminationConditionForm.cs - Lets the user specify the termination condition of the genetic algorithm.
    /// Andreas Dyrøy Jansson 2017
    /// </summary>
    public partial class TerminationConditionForm : Form
    {
        private ElementViewer _parent;

        public int Value
        {
            get; private set;
        }

        public TerminationConditionForm(ElementViewer parent)
        {
            InitializeComponent();

            _parent = parent;
            Value = _parent.TerminationLimit;

            switch (_parent.TerminationCondition)
            {
                case ETermCondition.FitnessBased:
                    rbtFitness.Checked = true;
                    break;
                case ETermCondition.GenerationsBased:
                    rbtGenerations.Checked = true;
                    break;
                case ETermCondition.VotesBased:
                    rbtVotes.Checked = true;
                    break;
            }

            nudValue.Maximum = Int32.MaxValue;
            nudValue.Value = _parent.TerminationLimit;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SaveAndClose();
        }

        private void nudValue_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SaveAndClose();
            }
            else if (rbtGenerations.Checked)
            {
                int currentGen = _parent.GEngine.Generation;

                if (nudValue.Value <= currentGen)
                {
                    lblGenerationComp.Text = $"(Current generation + {nudValue.Value} = {currentGen + nudValue.Value})"; 
                }
                else
                    lblGenerationComp.Text = "";
            }
            else
                lblGenerationComp.Text = "";
        }

        private void SaveAndClose()
        {
            Value = (int)nudValue.Value;

            if (rbtFitness.Checked)
                _parent.TerminationCondition = ETermCondition.FitnessBased;
            else if (rbtGenerations.Checked)
                _parent.TerminationCondition = ETermCondition.GenerationsBased;
            else if (rbtVotes.Checked)
                _parent.TerminationCondition = ETermCondition.VotesBased;

            _parent.TerminationLimit = Value + (rbtGenerations.Checked ? _parent.GEngine.Generation : 0);

            Close();
        }

        private void rbtVotes_CheckedChanged(object sender, EventArgs e)
        {
            nudValue.Maximum = rbtVotes.Checked ? 100 : Int32.MaxValue;
        }
    }
}
