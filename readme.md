# About the source code

The project consists of two major parts: DazzleTest, which is a unit test project used to test functions
during the development, and ProjectDazzle, which contains the source code for the GA implementation.

The most important files are ChromosomeBase.cs, ElementViewer.cs, GeneticEngine.cs and HTMLHelper.cs
These files contain the majority of the code behind the GA implementation discussed in the report.
Other files are mostly helper files, extending the features of the GUI and doing various specialized
tasks.

Chromosome-to-CSS-mapping is done in HTMLHelper.cs and CSSConstants.cs.

The "HTML"-directory contains embedded browser related files, including the JavaScript code allowing
for selection and voting of designs ("tableClicker.js").

To compile the project using Visual Studio, please select "x86" as build type, as the CefSharp library
does not support "AnyCPU". The data files necessary for the program to run are set to be copied to
the output-directory during build.

![161](https://source.uit.no/aja073/ProjectDazzle/uploads/fd0e9a087926e2226664cfb339516ef2/161.gif)